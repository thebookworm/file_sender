Name:           simplesamlphp
Version:        1.8.2
Release:        1%{?dist}
Summary:        Authentication and federation application supporting several protocols

Group:          Applications/Internet
License:        LGPL
URL:            http://simplesamlphp.org
Source0:        http://simplesamlphp.googlecode.com/files/%{name}-%{version}.tar.gz
Source1:	%{name}.htaccess
Patch0: 	simplesamlphp-1.8.1-vendor.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

Requires: httpd, openssl, zlib
Requires: php >= 5.2.0, php-cli, php-xml
Requires: php-mcrypt

%description
Authentication and federation application supporting several protocols
simpleSAMLphp, with its support for several authentication mechanisms
and federation protocols, can be used for local authentication, as a
service provider or identity provider. It currently supports the
following protocols:
* SAML 2.0 as a Service or Identity Provider.
* Shiboleth 1.3 as a Service or Identity Provider.
* A-Select as a Service or Identity Provider
* CAS for remote authentication
* OpenID as a Provider or Consumer
* WS-Federation as a Service Provider
Further authentication providers can be added in form of modules. Other
features are protocol bridging, eduGAIN compatibility, attribute mapping
and attribute release consent.


%prep
%setup -q
%patch0 -p1

%build

%install
rm -rf %{buildroot}
%{__mkdir} -p %{buildroot}%{_datadir}/%{name}
%{__mkdir} -p %{buildroot}%{_sysconfdir}/%{name}
%{__mkdir} -p %{buildroot}%{_sysconfdir}/httpd/conf.d
%{__mkdir} -p %{buildroot}%{_localstatedir}/lib/%{name}/data
%{__mkdir} -p %{buildroot}%{_localstatedir}/lib/%{name}/tmp
%{__mkdir} -p %{buildroot}%{_localstatedir}/log/%{name}

%{__cp} -ad ./* %{buildroot}%{_datadir}/%{name}
%{__cp} -ad ./config/* %{buildroot}%{_sysconfdir}/%{name}
%{__cp} -p %{SOURCE1} %{buildroot}%{_sysconfdir}/httpd/conf.d/%{name}.conf

%{__rm} -f %{buildroot}%{_datadir}/%{name}/COPYING
%{__rm} -rf %{buildroot}%{_datadir}/%{name}/docs

%{__rm} -rf %{buildroot}%{_datadir}/%{name}/config
%{__rm} -rf %{buildroot}%{_datadir}/%{name}/data

ln -s ../../..%{_sysconfdir}/%{name} %{buildroot}%{_datadir}/%{name}/config
ln -s ../../..%{_localstatedir}/lib/%{name}/data %{buildroot}%{_datadir}/%{name}/data

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc docs/*
%{_datadir}/%{name}/
%dir %{_sysconfdir}/%{name}/
%config(noreplace) %{_sysconfdir}/%{name}/*
%config(noreplace) %{_sysconfdir}/httpd/conf.d/%{name}.conf
%dir %{_localstatedir}/lib/%{name}/
%dir %attr(0750,apache,apache) %{_localstatedir}/lib/%{name}/data
%dir %attr(0750,apache,apache) %{_localstatedir}/lib/%{name}/tmp
%dir %attr(0750,apache,apache) %{_localstatedir}/log/%{name}



%changelog
* Wed Jan 11 2012 Xander Jansen <Xander.Jansen@surfnet.nl> 1.8.2-1
- Update to 1.8.2 (security fix release)

* Thu Oct 27 2011 Xander Jansen <Xander.Jansen@surfnet.nl> 1.8.1-1
- Update to 1.8.1 (security fix release)

* Sat Jun 25 2011 Xander Jansen <Xander.Jansen@surfnet.nl> 1.8.0-2
- Added additional dependencies

* Sun May 1 2011 Xander Jansen <Xander.Jansen@surfnet.nl> 1.8.0-1
- Update to 1.8.0

* Sat Jan 29 2011 Xander Jansen <Xander.Jansen@surfnet.nl> 1.6.3-1
- Update to 1.6.3

* Thu Sep 2 2010 Gijs Molenaar <gijs@pythonic.nl> 1.6.2-1
- first release
