#!/bin/sh
#
# this script is ran after installation of debian package in chroot
#

#configure database
/etc/init.d/postgresql-8.3 restart
echo "CREATE DATABASE filesender; \q" | su postgres -c psql
su postgres -c psql filesender < /usr/share/filesender/scripts/filesender_db.sql


#configure webserver
cat > /etc/apache2/conf.d/filesender << "EOF"
Alias /simplesaml/ /usr/share/simplesamlphp/www/
<Directory "/usr/share/simplesamlphp/www">
    AllowOverride None
    Order deny,allow
    Allow from all
</Directory>

Alias /filesender/ /usr/share/filesender/www/
<Directory "/usr/share/filesender/">
    AllowOverride None
    Order deny,allow
    Allow from all
</Directory>
EOF

sed -i 's/80/6666/g' /etc/apache2/sites-enabled/000-default
sed -i 's/80/6666/g' /etc/apache2/ports.conf

#restart webserver
/etc/init.d/apache2 restart


#upload test or something
echo "TODO: upload file or something"

#stop webserver
/etc/init.d/apache2 stop
/etc/init.d/postgresql* stop

#remove files
rm  /etc/apache2/conf.d/filesender
