#!/bin/bash

set -e


# Which version to use for nightly build
VERSION="1.5.0.`date +%Y%m%d%H%M`"

# Where is the filesender repository
SVNREPO="http://subversion.assembla.com/svn/file_sender/filesender/branches/filesender-1.5/"

# debian distribution (oldstable, stable, testing, unstable or experimental)
DIST="unstable"

# Name of the Flex .swf binary (filesender for < 1.5, filesenderup for 1.5)
SWFNAME="filesenderup"


## you probably don't want to change this

# temporary simplesamlphp repo checkout
TMPREPO="/tmp/simplesamlphprepo-${DIST}"

# where is the Adobe Flex SDK
FLEXSDK="/opt/flexsdk/"

# what debian mirror to use
DEBMIRROR="http://ftp.nluug.nl/ftp/os/Linux/distr/debian/"

# Where to put build tarball
TGZREPO="/var/www/nightly/"

# Where is our own debian repository
DEBREPO="/var/www/debian/"

# where to build rpm stuff
RPMBUILD="${HOME}/rpmbuild/"

# where to put resulting RPM's
RPMREPO="/var/www/rpm/${DIST}/"


HERE="$(cd $(dirname $0); pwd)/"
. ${HERE}functions.sh


update_repo
cleanup_repo
build_flash
cleanup_build
prepare_build
build_debian
build_tarball
build_rpm
publish_tarball
publish_debian
publish_rpm
update_simplesaml_latest
sign
end
