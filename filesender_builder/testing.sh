#!/bin/bash

set -e

# Which version and branch to use for stable build
VERSION="1.1"
BRANCH="1.1"

# Where is the filesender repository
SVNREPO="http://subversion.assembla.com/svn/file_sender/filesender/branches/filesender-${BRANCH}"

# Where to put build tarball
TGZREPO="/var/www/releases/"

# distribution bundle (oldstable, stable, testing, unstable or experimental)
DIST="testing"

# Name of the Flex .swf binary (filesender for < 1.5, filesenderup for 1.5)
SWFNAME="filesender"


## probably you don't want to change these

# temporary simplesamlphp repo checkout
TMPREPO="/tmp/simplesamlphprepo-${DIST}"

# where is the Adobe Flex SDK
FLEXSDK="/opt/flexsdk/"

# what debian mirror to use to mirror simplesaml
DEBMIRROR="http://ftp.nluug.nl/ftp/os/Linux/distr/debian/"

# Where is our own debian repository
DEBREPO="/var/www/debian/"

# where to build rpm stuff
RPMBUILD="${HOME}/rpmbuild/"

# where to put resulting RPM's
RPMREPO="/var/www/rpm/${DIST}/"



HERE="$(cd $(dirname $0); pwd)/"
. ${HERE}functions.sh

update_repo
cleanup_repo
build_flash
cleanup_build
prepare_build
build_debian
build_tarball
build_rpm

message "Building complete, do you want to publish? [y/N]"
read a
if [[ $a != "y" ]]; then
    message "not publishing, bye"
    exit 0
fi

publish_tarball
publish_debian
publish_rpm
update_simplesaml_latest
sign
end
