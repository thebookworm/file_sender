
BUILDDIR="${HERE}build/"
CHECKOUT="${BUILDDIR}svn/"
RELEASE="filesender-${VERSION}"
SDIST="${BUILDDIR}packaging/${RELEASE}/"


message() {
    echo "*** BUILDER: $1"
}


error_message() {
    echo "*** BUILDER: ERROR: $1" >&2
}


fail_check() {
    if [ ! $? -eq 0 ]; then
        error_message "$1"
        exit 1
    fi
}


run() {
    message "running $1"
    $1
    fail_check "running $1 failed"
}


update_repo() {
    message "update or checkout filesender repo"
    pushd ${BUILDDIR}
    rm -rf svn
    run "svn co ${SVNREPO} svn"
    popd
}


cleanup_repo() {
    message "remove unversioned files"
    REMOVE=`svn status --no-ignore ${CHECKOUT} | grep '^\?' | sed 's/^\?      //'`
    run "rm -rvf ${REMOVE}"
}


build_flash() {
    message "build the flash thing"
    pushd ${CHECKOUT}flex/src
    run "${FLEXSDK}bin/mxmlc -theme=${FLEXSDK}frameworks/themes/Halo/halo.swc -static-link-runtime-shared-libraries=true -output ${CHECKOUT}www/swf/${SWFNAME}.swf ${SWFNAME}.mxml"
    popd
}

cleanup_build() {
    message "removing old packaging stuff"
    run "rm -rvf ${BUILDDIR}packaging/"
}


prepare_build() {
    message "creating packaging structure"
    run "mkdir ${BUILDDIR}packaging/"
    run "svn export ${CHECKOUT} ${SDIST}"
    run "cp ${CHECKOUT}www/swf/${SWFNAME}.swf ${SDIST}www/swf"
    run "rm -rf ${SDIST}flex"
    run "chmod 640 ${SDIST}config/config.php"
}

build_debian() {
    message "making debian package"
    pushd ${SDIST}
    run "cp -p config/config.php ${BUILDDIR}packaging/config.php"
    export DEBFULLNAME="Filesender Development"
    export DEBEMAIL="filesender-dev@filesender.org"
    if [ ${DIST} == "stable" ]
    then
        run "dch --distribution ${DIST}"
    else
        run "rm debian/changelog"
        run "dch --create --package filesender --newversion ${VERSION} --distribution ${DIST} \"nightly build ${VERSION}\""
    fi
    run "dpkg-buildpackage"
    run "mv ${BUILDDIR}packaging/config.php config/config.php"
    popd
}


build_tarball() {
    message "making source tarball"
    run "rm -rf ${SDIST}debian ${SDIST}flex ${SDIST}rpm"
    pushd ${BUILDDIR}packaging/
    run "fakeroot tar zcvf ${RELEASE}.tar.gz ${RELEASE}"
    run "fakeroot zip -r ${RELEASE}.zip ${RELEASE}"
    popd
}


build_rpm() {
    message "making RPM"
    cp ${BUILDDIR}packaging/${RELEASE}.tar.gz ${RPMBUILD}SOURCES
    cp ${CHECKOUT}rpm/filesender.spec ${RPMBUILD}SPECS
    cp ${CHECKOUT}rpm/filesender.htaccess ${RPMBUILD}SOURCES
    cp ${CHECKOUT}rpm/filesender.cron.daily ${RPMBUILD}SOURCES
    cp ${CHECKOUT}rpm/filesender-config.php ${RPMBUILD}SOURCES
    perl -pi -e "s/^Version:.*$/Version:\t\t${VERSION}/g" ${RPMBUILD}SPECS/filesender.spec
    rm -rf ${RPMBUILD}/RPMS/noarch/filesender-*.rpm
    echo "" | setsid rpmbuild -ba --sign ${RPMBUILD}/SPECS/filesender.spec
}


publish_tarball() {
    message "publishing tarball"
    run "cp ${BUILDDIR}packaging/${RELEASE}.tar.gz ${TGZREPO}."
    run "cp ${BUILDDIR}packaging/${RELEASE}.zip ${TGZREPO}."
    run "ln -sf ${TGZREPO}${RELEASE}.tar.gz ${TGZREPO}filesender-latest.tar.gz"
}


publish_debian() {
    message "put package in debian repo"
    run "reprepro -b ${DEBREPO} includedeb ${DIST} ${BUILDDIR}packaging/filesender_${VERSION}_all.deb"
}


update_simplesaml() {
    message "fetch latest simplesaml"
    if [ ! -x ${TMPREPO} ]; then
        mkdir ${TMPREPO}
    fi
    pushd ${TMPREPO}
    wget -m -np -nd ${DEBMIRROR}/pool/main/s/simplesamlphp/

    message "add latest simplesaml 1.6 to repo"
    reprepro -b ${DEBREPO} includedeb ${DIST} simplesamlphp_1.6*_all.deb
    reprepro -b ${DEBREPO} includedsc ${DIST} ./simplesamlphp_1.6*.dsc
    popd
}

update_simplesaml_latest() {
    message "fetch latest simplesaml"
    if [ ! -x ${TMPREPO} ]; then
        mkdir ${TMPREPO}
    fi
    pushd ${TMPREPO}
    wget -m -np -nd ${DEBMIRROR}/pool/main/s/simplesamlphp/

    message "add latest simplesaml 1.8 to repo"
    reprepro -b ${DEBREPO} includedeb ${DIST} simplesamlphp_1.8*_all.deb
    reprepro -b ${DEBREPO} includedsc ${DIST} ./simplesamlphp_1.8*.dsc
    popd
}


publish_rpm() {
    message "publising RPM"
    rm -rf ${RPMREPO}/filesender-*.rpm
    cp ${RPMBUILD}/RPMS/noarch/filesender-*.rpm ${RPMREPO}
    pushd ${RPMREPO}
    createrepo -o . .
    popd
}


sign() {
    message "signing tarball"
    pushd  ${TGZREPO}
    gpg --sign ${RELEASE}.tar.gz
    gpg --sign ${RELEASE}.zip
    sha1sum ${RELEASE}.tar.gz > ${RELEASE}.tar.gz.sha1
    sha1sum ${RELEASE}.zip > ${RELEASE}.zip.sha1
    md5sum ${RELEASE}.tar.gz > ${RELEASE}.tar.gz.md5
    md5sum ${RELEASE}.zip > ${RELEASE}.zip.md5
}

end() {
    message "building complete. bye."
    exit 0
}


