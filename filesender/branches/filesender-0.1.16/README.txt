Updated: March 11th, 2010

All you need to know to install and configure FileSender is available at:
https://www.assembla.com/wiki/show/file_sender/Documentation

FileSender homepage:
http://www.filesender.org

FileSender mailinglist (for support):
filesender-dev@filesender.org

To contact the author team:
jan.meijer@uninett.no
chris@ricoshae.com.au
please use the mailinglist as often as possible for support questions and feature requests)