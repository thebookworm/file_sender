<div id="box"><?php echo '<div id="pageheading">' . lang("_STATS") . '</div>'; ?>
    <?php
    $link = "index.php?";
    if (isset($_GET["freq"])) {
        $link .= "freq=" . $_GET["freq"] . "&amp;";
    }

    $frequencies = array();
    $graphs = array();

    if (isset($_GET["mode"]) && $_GET["mode"] == "keystats") {
        // Display the "key statistics" page.
        echo lang("_STATS_INFO_KEY") . excludedDomains();
        echo '<form action="index.php" method="get">
				  <input type="hidden" name="mode" value="keystats" />
				  <label for="year" class="mandatory">Year: </label>
				  <select name="year" style="margin-left:5px;width:100px;">
					<option value="all">All</option>';

        // Get the years that there is data for and add them to a dropdown list.
        $years = $functions->getYears();
        foreach ($years as $year) {
            echo '<option value="' . $year . '">' . $year . '</option>';
        }

        echo '</select>
				  <input type="submit" style="margin-left:15px;width:150px;" value="' . lang('_SEND') . '" />
				  </form></div><div class="box">';

        $year = "";
        if (isset($_GET["year"])) {
            $year = $_GET["year"];
        }
        echo '<h3>' . lang("_STATS_KEY_TITLE");

        if (in_array($year, $years)) {
            // A (valid) year is selected, display data only for that year.
            echo $year . '</h3>';
            echo $functions->keyStats($_GET['year']);
        } else {
            // Display all the data.
            echo lang("_STATS_KEY_TITLE_ALL") . '</h3>';
            echo $functions->keyStats();
        }
        echo '<a href="' . $link . '">' . lang("_OVERVIEW_LINK") . '</a>';
    } else if (isset($_GET["type"])) {
        // If a specific graph type is selected, display all of its frequences on one page.
        $setting = $config['stats_access_level_' . $_GET['type']];
        $hasAccess = ($setting == "public" || ($setting == "user" && $isAuth) || ($setting == "admin" && $isAdmin));

        if ($hasAccess) {
            // Display a description box if enabled in config.
            $description = "";
            if ($config['stats_display_documentation']) {
                $description = lang("_STATS_INFO_" . strtoupper($_GET["type"]));
                $description .= excludedDomains(); // List any domains that are not included in the stats.
            }

            if ($config['stats_documentation_position'] == "top") {
                echo '<div id="graphinfo">' . $description . '</div></div><div class="box">';
            }

            // User has access to view graph.
            $frequencies = $config["stats_available_frequencies"];
            $graphs[] = $_GET["type"];


            // Display every available frequency for this graph.
            foreach ($frequencies as $frequency) {
                $title = lang("_GRAPH_" . strtoupper($graphs[0]) . "_TITLE") . " " . lang("_FREQUENCY_" . strtoupper($frequency) . "_SHORT");
                echo '<div id="' . $graphs[0] . '-' . $frequency . '" class="bigchart"></div>
						  <p>
							<a href="#" class="tablelink" onclick="openTable(\'' . $graphs[0] . '-table-' . $frequency . '\'); return false;">Table</a>
							| <a href="' . $link . '">' . lang("_OVERVIEW_LINK") . '</a>
						  </p>
						  <div id="' . $graphs[0] . '-table-' . $frequency . '" class="statstable" style="display:none;" title="' . $title . '"></div>';
            }

            if ($config['stats_documentation_position'] == "bottom") {
                echo '</div><div id="graphinfo" class="box">' . lang("_STATS_INFO_TITLE") . $description;
            }
        } else {
            // User does not have access.
            echo '<div id="box">' . lang("_ACCESS_ERROR") . '</div>';
        }
    } else {
        // No specific graph selected, display a "dashboard overview" instead.
        // Set up links to change the interval frequency of the displayed graph(s).
        $link = "index.php";

        $linksHtml = "";
        foreach ($config['stats_available_frequencies'] as $frequency) {
            $linksHtml .= '<a href="' . $link . '?freq=' . $frequency . '">' . lang('_FREQUENCY_' . strtoupper($frequency)) . '</a> | ';
        }

        $linksHtml = substr_replace($linksHtml, "", -2); // Remove the last "| " from the string.

        echo '<p>' . lang('_FREQUENCY') . ' ' . $linksHtml . '</p>';
        echo '<p>' . lang('_MODES') . '<a href="' . $link . '?mode=custom">' . lang('_CUSTOM_MODE') . '</a>';
        echo ' | <a href="' . $link . '?mode=keystats">' . lang('_KEYSTATS_MODE') . '</a></p>';

        $frequencies[] = isset($_GET["freq"]) ? $_GET["freq"] : $config["stats_default_frequency"];

        if (!in_array($frequencies[0], $config['stats_available_frequencies'])) {
            $frequencies[0] = 'invalid_frequency';
        }

        $graphs = array('updown', 'filesizes', 'vouchers', 'totalupdown', 'activefiles', 'activeusers', 'totalfiles', 'totalusers');
        $count = 0;

        foreach ($graphs as $graph) {
            $setting = $config['stats_access_level_' . $graph];
            if ($setting == "public" || ($setting == "user" && $isAuth) || ($setting == "admin" && $isAdmin)) {
                // User has access to view this graph.
                $class = ($count % 2 == 0) ? 'dashboard-left' : 'dashboard-right';
                echo '<div id="' . $graph . '-' . $frequencies[0] . '" class="' . $class . '">
							<a href="' . $link . '?type=' . $graph . '">
							  <span class="graphlink"></span>
							</a>
						  </div>';
                $count++;
            }
        }

        echo '<br class="clearboth" />';
    }

    $graphStrings = getGraphStrings($graphs);

    function excludedDomains()
    {
        global $config;
        $excludedArr = $config['excluded_domains'];

        if (empty($excludedArr) || (count($excludedArr) == 1 && empty($excludedArr[0]))) {
            $domainString = lang("_ALL_DOMAINS_INCLUDED");
        } else {
            $domainString = lang("_EXCLUDED_DOMAINS") . '<ul id="excluded">';

            foreach ($config['excluded_domains'] as $domain) {
                $domainString .= '<li>' . $domain . '</li>';
            }
            $domainString .= '</ul>';
        }
        return $domainString;
    }

    function getGraphStrings($graphs)
    {
        $strings = array();

        foreach ($graphs as $graph) {
            $graphStrings = array();
            $graphName = strtoupper($graph);


            // Get title and axis labels.
            $graphStrings["title"] = lang("_GRAPH_" . $graphName . "_TITLE");
            $graphStrings["xaxis"] = lang("_GRAPH_" . $graphName . "_XAXIS_LABEL");
            $graphStrings["yaxis"] = lang("_GRAPH_" . $graphName . "_YAXIS_LABEL");

            // Get labels for the series legend (depending on how many series the graph has).
            $numSeries = 1;
            if ($graphName == "FILESIZES") $numSeries = 4;
            else if ($graphName == "UPDOWN" || $graphName == "VOUCHERS" || $graphName == "TOTALUPDOWN") $numSeries = 2;

            $graphStrings["series"] = array();
            for ($i = 0; $i < $numSeries; ++$i) {
                $graphStrings["series"][] = lang("_GRAPH_" . $graphName . "_SERIES_" . $i);
            }

            $strings[$graph] = $graphStrings;
        }

        return $strings;
    }


    ?>
</div>

<?php
// Get the strings for the available frequencies.
$freqStrings = array(
    "year" => lang("_FREQUENCY_YEAR_SHORT"),
    "month" => lang("_FREQUENCY_MONTH_SHORT"),
    "week" => lang("_FREQUENCY_WEEK_SHORT"),
    "day" => lang("_FREQUENCY_DAY_SHORT")
);
?>

<script type="text/javascript">
var freqStrings = <?php echo json_encode($freqStrings); ?>;

$(document).ready(function () {
    // Retrieve the chart/table JSON data.
    $.getJSON('data/data.json', function (data) {
        chartData = data["charts"];
        tableData = data["tables"];

        <?php
            foreach ($graphs as $graph) {
                foreach ($frequencies as $frequency) {
                    // Call the correct js functions, depending on which frequencies and types of graphs we currently want to display.
                    $chartFunction = $graph . "('" . $frequency . "');";
                    echo $chartFunction;
                    $chartDialog = '$("#' . $graph . '-table-' . $frequency . '").dialog({autoOpen: false,height: 400,width: 660,modal: true});';
                    echo $chartDialog;
                }
            }
        ?>
    });
});

function getDateFormat(frequency) {
    if (frequency == "year") {
        return "%Y";
    } else if (frequency == "month" || frequency == "week") {
        return "%b %Y";
    } else { // frequency == "day"
        return "%d %b '%y";
    }
}

function getTickInterval(frequency) {
    return '1 ' + (frequency == "week" ? "month" : frequency);
}

/*
 * Displays a line graph with two lines for uploads and downloads over time.
 */
function updown(frequency) {
    var data = chartData['updown'];
    var dates = <?php echo json_encode($functions->getGraphDates($frequencies)); ?>;
    var dateFormat = getDateFormat(frequency);
    var maxValue = findYAxisMax(data[0][frequency], data[1][frequency]) * 1.2;

    var start = $.datepicker.parseDate('yy-mm-dd', dates[frequency][0]);
    var end = $.datepicker.parseDate('yy-mm-dd', dates[frequency][1]);

    var chartTitle = '<?php echo lang("_GRAPH_UPDOWN_TITLE"); ?> ' + freqStrings[frequency];
    var xLabel = '<?php echo lang("_GRAPH_UPDOWN_XAXIS_LABEL"); ?>';
    var yLabel = '<?php echo lang("_GRAPH_UPDOWN_YAXIS_LABEL"); ?>';

    if (!hasEnoughData(data[0][frequency], start, end) && !hasEnoughData(data[1][frequency], start, end)) {
        drawEmptyGraph('updown', frequency, chartTitle, xLabel, yLabel, dates, dateFormat);
        missingData('#updown-' + frequency);
    } else {
        try {
            $.jqplot('updown-' + frequency, [data[0][frequency], data[1][frequency]], {
                title: chartTitle,
                seriesColors: ["#6666ff", "#ff8000"],
                gridPadding: {
                    right: 25,
                    left: 60
                },
                highlighter: {
                    show: true,
                    tooltipSeparator: ': ',
                    fadeTooltip: false,
                    sizeAdjust: 9
                },
                axesDefaults: {
                    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                    labelOptions: {
                        fontFamily: 'Verdana,Geneva,sans-serif',
                        fontSize: '10pt'
                    }
                },
                axes: {
                    xaxis: {
                        min: start,
                        tickInterval: getTickInterval(frequency),
                        renderer: $.jqplot.DateAxisRenderer,
                        tickOptions: {
                            formatString: dateFormat
                        },
                        label: xLabel
                    },
                    yaxis: {
                        tickOptions: {
                            formatString: "%d"
                        },
                        min: 0,
                        max: maxValue,
                        label: yLabel
                    }
                },
                seriesDefaults: {
                    lineWidth: 2,
                    markerOptions: {
                        show: false
                    }
                },
                series: [
                    {label: '<?php echo lang("_GRAPH_UPDOWN_UPLOADS_LABEL"); ?>'},
                    {label: '<?php echo lang("_GRAPH_UPDOWN_DOWNLOADS_LABEL"); ?>'}
                ],
                legend: {
                    show: true
                }
            });
        } catch (err) {
            drawEmptyGraph('updown', frequency, chartTitle, xLabel, yLabel, dates, dateFormat);
            noDataFound('#updown-' + frequency);
        }
    }

    if ($("#updown-table-" + frequency).length > 0) {
        // Create the HTML table.
        var headerArr = ['<?php echo lang("_GRAPH_UPDOWN_XAXIS_LABEL"); ?>',
            '<?php echo lang("_GRAPH_UPDOWN_UPLOADS_LABEL"); ?>',
            '<?php echo lang("_GRAPH_UPDOWN_DOWNLOADS_LABEL"); ?>'];
        createTable("#updown-table-" + frequency, headerArr, tableData['updown'][frequency], dates[frequency][0], frequency);
    }
}

/*
 * Displays a stacked line chart with a running total of uploaded files by date.
 */
function filesizes(frequency) {
    var data = chartData['filesizes'];
    var dates = <?php echo json_encode($functions->getGraphDates($frequencies)); ?>;
    var first = data[0][frequency];
    var second = data[1][frequency];
    var third = data[2][frequency];
    var fourth = data[3][frequency];
    var dateFormat = getDateFormat(frequency);

    var start = $.datepicker.parseDate('yy-mm-dd', dates[frequency][0]);
    var end = $.datepicker.parseDate('yy-mm-dd', dates[frequency][1]);

    var chartTitle = '<?php echo lang("_GRAPH_FILESIZES_TITLE"); ?> ' + freqStrings[frequency];
    var xLabel = '<?php echo lang("_GRAPH_FILESIZES_XAXIS_LABEL"); ?>';
    var yLabel = '<?php echo lang("_GRAPH_FILESIZES_YAXIS_LABEL"); ?>';

    if (!hasEnoughData(first, start, end)) {
        drawEmptyGraph('filesizes', frequency, chartTitle, xLabel, yLabel, dates, dateFormat);
        missingData('#filesizes-' + frequency);
    } else {
        try {
            $.jqplot('filesizes-' + frequency, [first, second, third, fourth], {
                title: chartTitle,
                seriesColors: ["#6666ff", "#ff8000", "#6eff70", "#bd693f"],
                gridPadding: {
                    right: 25,
                    left: 60
                },
                stackSeries: true,
                highlighter: {
                    show: true,
                    tooltipSeparator: ': ',
                    fadeTooltip: false,
                    sizeAdjust: 9
                },
                axesDefaults: {
                    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                    labelOptions: {
                        fontFamily: 'Verdana,Geneva,sans-serif',
                        fontSize: '10pt'
                    }
                },
                axes: {
                    xaxis: {
                        min: start,
                        tickInterval: getTickInterval(frequency),
                        renderer: $.jqplot.DateAxisRenderer,
                        tickOptions: {
                            formatString: dateFormat
                        },
                        pad: 1.0,
                        label: xLabel
                    },
                    yaxis: {
                        tickOptions: {
                            formatString: "%d"
                        },
                        min: 0,
                        label: yLabel
                    }
                },
                seriesDefaults: {
                    fill: true,
                    fillAndStroke: true,
                    lineWidth: 2,
                    markerOptions: {
                        show: false
                    }
                },
                series: [
                    {label: '<?php echo lang("_GRAPH_FILESIZES_FIRST_LABEL"); ?>'},
                    {label: '<?php echo lang("_GRAPH_FILESIZES_SECOND_LABEL"); ?>'},
                    {label: '<?php echo lang("_GRAPH_FILESIZES_THIRD_LABEL"); ?>'},
                    {label: '<?php echo lang("_GRAPH_FILESIZES_FOURTH_LABEL"); ?>'}
                ],
                legend: {
                    show: true,
                    location: 'nw'
                }
            });
        } catch (err) {
            drawEmptyGraph('filesizes', frequency, chartTitle, xLabel, yLabel, dates, dateFormat);
            noDataFound('#filesizes-' + frequency);
        }
    }

    if ($("#filesizes-table-" + frequency).length > 0) {
        // Create the HTML table.
        var headerArr = ['<?php echo lang("_GRAPH_FILESIZES_XAXIS_LABEL"); ?>',
            '<?php echo lang("_GRAPH_FILESIZES_FIRST_LABEL"); ?>',
            '<?php echo lang("_GRAPH_FILESIZES_SECOND_LABEL"); ?>',
            '<?php echo lang("_GRAPH_FILESIZES_THIRD_LABEL"); ?>',
            '<?php echo lang("_GRAPH_FILESIZES_FOURTH_LABEL"); ?>'];
        createTable("#filesizes-table-" + frequency, headerArr, tableData['filesizes'][frequency], dates[frequency][0], frequency);
    }
}

/*
 * Displays a line graph with two lines for numbers of sent/used invite vouchers over time.
 */
function vouchers(frequency) {
    var data = chartData['vouchers'];
    var dateFormat = getDateFormat(frequency);
    var dates = <?php echo json_encode($functions->getGraphDates($frequencies)); ?>;

    var start = $.datepicker.parseDate('yy-mm-dd', dates[frequency][0]);
    var end = $.datepicker.parseDate('yy-mm-dd', dates[frequency][1]);

    var chartTitle = '<?php echo lang("_GRAPH_VOUCHERS_TITLE"); ?> ' + freqStrings[frequency];
    var xLabel = '<?php echo lang("_GRAPH_VOUCHERS_XAXIS_LABEL"); ?>';
    var yLabel = '<?php echo lang("_GRAPH_VOUCHERS_YAXIS_LABEL"); ?>';

    if (!hasEnoughData(data[0][frequency], start, end) && !hasEnoughData(data[1][frequency], start, end)) {
        drawEmptyGraph('vouchers', frequency, chartTitle, xLabel, yLabel, dates, dateFormat);
        missingData('#vouchers-' + frequency);
    } else {
        try {
            $.jqplot('vouchers-' + frequency, [data[0][frequency], data[1][frequency]], {
                title: chartTitle,
                seriesColors: ["#6666ff", "#ff8000"],
                gridPadding: {
                    right: 25,
                    left: 60
                },
                highlighter: {
                    show: true,
                    tooltipSeparator: ': ',
                    fadeTooltip: false,
                    sizeAdjust: 9
                },
                axesDefaults: {
                    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                    labelOptions: {
                        fontFamily: 'Verdana,Geneva,sans-serif',
                        fontSize: '10pt'
                    }
                },
                axes: {
                    xaxis: {
                        min: start,
                        tickInterval: getTickInterval(frequency),
                        renderer: $.jqplot.DateAxisRenderer,
                        tickOptions: {
                            formatString: dateFormat
                        },
                        label: xLabel
                    },
                    yaxis: {
                        renderer: $.jqplot.LogAxisRenderer,
                        tickOptions: {
                            formatString: '%d'
                        },
                        label: yLabel
                    }
                },
                series: [
                    {label: '<?php echo lang("_GRAPH_VOUCHERS_SENT_LABEL"); ?>', lineWidth: 2, markerOptions: {show: false}},
                    {label: '<?php echo lang("_GRAPH_VOUCHERS_USED_LABEL"); ?>', lineWidth: 2, markerOptions: {show: false}}
                ],
                legend: {
                    show: true
                }
            });
        } catch (err) {
            drawEmptyGraph('vouchers', frequency, chartTitle, xLabel, yLabel, dates, dateFormat);
            noDataFound('#vouchers-' + frequency);
        }
    }

    if ($("#vouchers-table-" + frequency).length > 0) {
        // Create the HTML table.
        var headerArr = ['<?php echo lang("_GRAPH_VOUCHERS_XAXIS_LABEL"); ?>',
            '<?php echo lang("_GRAPH_VOUCHERS_SENT_LABEL"); ?>',
            '<?php echo lang("_GRAPH_VOUCHERS_USED_LABEL"); ?>'];

        createTable("#vouchers-table-" + frequency, headerArr, tableData['vouchers'][frequency], dates[frequency][0], frequency);
    }
}

/*
 * Line chart with two lines for running totals of uploads and downloads (in gigabytes) over time.
 */
function totalupdown(frequency) {
    var data = chartData['totalupdown'];
    var dates = <?php echo json_encode($functions->getGraphDates($frequencies)); ?>;
    var dateFormat = getDateFormat(frequency);
    var maxValue = findYAxisMax(data[0][frequency], data[1][frequency]) * 1.2;

    var start = $.datepicker.parseDate('yy-mm-dd', dates[frequency][0]);
    var end = $.datepicker.parseDate('yy-mm-dd', dates[frequency][1]);

    var chartTitle = '<?php echo lang("_GRAPH_TOTALUPDOWN_TITLE"); ?> ' + freqStrings[frequency];
    var xLabel = '<?php echo lang("_GRAPH_TOTALUPDOWN_XAXIS_LABEL"); ?>';
    var yLabel = '<?php echo lang("_GRAPH_TOTALUPDOWN_YAXIS_LABEL"); ?>';

    if (!hasEnoughData(data[0][frequency], start, end) && !hasEnoughData(data[1][frequency], start, end)) {
        drawEmptyGraph('totalupdown', frequency, chartTitle, xLabel, yLabel, dates, dateFormat);
        missingData('#totalupdown-' + frequency);
    } else {
        try {
            $.jqplot('totalupdown-' + frequency, [data[0][frequency], data[1][frequency]], {
                title: chartTitle,
                seriesColors: ["#6666ff", "#ff8000"],
                gridPadding: {
                    right: 25,
                    left: 60
                },
                highlighter: {
                    show: true,
                    tooltipSeparator: ': ',
                    fadeTooltip: false,
                    sizeAdjust: 9
                },
                axesDefaults: {
                    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                    labelOptions: {
                        fontFamily: 'Verdana,Geneva,sans-serif',
                        fontSize: '10pt'
                    }
                },
                axes: {
                    xaxis: {
                        min: start,
                        tickInterval: getTickInterval(frequency),
                        renderer: $.jqplot.DateAxisRenderer,
                        tickOptions: {
                            formatString: dateFormat
                        },
                        label: xLabel
                    },
                    yaxis: {
                        tickOptions: {
                            formatString: "%d"
                        },
                        min: 0,
                        max: maxValue,
                        label: yLabel
                    }
                },
                seriesDefaults: {
                    lineWidth: 2,
                    markerOptions: {
                        show: false
                    }
                },
                series: [
                    {label: '<?php echo lang("_GRAPH_TOTALUPDOWN_UPLOADS_LABEL"); ?>'},
                    {label: '<?php echo lang("_GRAPH_TOTALUPDOWN_DOWNLOADS_LABEL"); ?>'}
                ],
                legend: {
                    show: true
                }
            });
        } catch (err) {
            drawEmptyGraph('totalupdown', frequency, chartTitle, xLabel, yLabel, dates, dateFormat);
            noDataFound('#totalupdown-' + frequency);
        }
    }

    if ($("#totalupdown-table-" + frequency).length > 0) {
        // Create the HTML table.
        var headerArr = ['<?php echo lang("_GRAPH_TOTALUPDOWN_XAXIS_LABEL"); ?>',
            '<?php echo lang("_GRAPH_TOTALUPDOWN_UPLOADS_LABEL"); ?>',
            '<?php echo lang("_GRAPH_TOTALUPDOWN_DOWNLOADS_LABEL"); ?>'];

        createTable("#totalupdown-table-" + frequency, headerArr, tableData['totalupdown'][frequency], dates[frequency][0], frequency);
    }
}

function activefiles(frequency) {
    var data = chartData['activefiles'];
    var dates = <?php echo json_encode($functions->getGraphDates($frequencies)); ?>;
    var dateFormat = getDateFormat(frequency);
    var maxValue = findYAxisMax(data[0][frequency]) * 1.2;

    var start = $.datepicker.parseDate('yy-mm-dd', dates[frequency][0]);
    var end = $.datepicker.parseDate('yy-mm-dd', dates[frequency][1]);

    var chartTitle = '<?php echo lang("_GRAPH_ACTIVEFILES_TITLE"); ?> ' + freqStrings[frequency];
    var xLabel = '<?php echo lang("_GRAPH_ACTIVEFILES_XAXIS_LABEL"); ?>';
    var yLabel = '<?php echo lang("_GRAPH_ACTIVEFILES_YAXIS_LABEL"); ?>';

    if (!hasEnoughData(data[0][frequency], start, end)) {
        drawEmptyGraph('activefiles', frequency, chartTitle, xLabel, yLabel, dates, dateFormat);
        missingData('#activefiles-' + frequency);
    } else {
        try {
            $.jqplot('activefiles-' + frequency, [data[0][frequency]], {
                title: chartTitle,
                seriesColors: ["#6666ff", "#ff8000"],
                gridPadding: {
                    right: 25,
                    left: 60
                },
                highlighter: {
                    show: true,
                    tooltipSeparator: ': ',
                    fadeTooltip: false,
                    sizeAdjust: 9
                },
                axesDefaults: {
                    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                    labelOptions: {
                        fontFamily: 'Verdana,Geneva,sans-serif',
                        fontSize: '10pt'
                    }
                },
                axes: {
                    xaxis: {
                        min: start,
                        tickInterval: getTickInterval(frequency),
                        renderer: $.jqplot.DateAxisRenderer,
                        tickOptions: {
                            formatString: dateFormat
                        },
                        label: xLabel
                    },
                    yaxis: {
                        min: 0,
                        max: maxValue,
                        tickOptions: {
                            formatString: "%d"
                        },
                        label: yLabel
                    }
                },
                seriesDefaults: {
                    lineWidth: 2,
                    markerOptions: {
                        show: false
                    }
                }
            });
        } catch (err) {
            drawEmptyGraph('activefiles', frequency, chartTitle, xLabel, yLabel, dates, dateFormat);
            noDataFound('#activefiles-' + frequency);
        }
    }

    if ($("#activefiles-table-" + frequency).length > 0) {
        // Create the HTML table.
        var headerArr = ['<?php echo lang("_GRAPH_ACTIVEFILES_XAXIS_LABEL"); ?>',
            '<?php echo lang("_GRAPH_ACTIVEFILES_SERIES_LABEL"); ?>'];
        createTable("#activefiles-table-" + frequency, headerArr, tableData['activefiles'][frequency], dates[frequency][0], frequency);
    }
}

function activeusers(frequency) {
    var data = chartData['activeusers'];
    var dates = <?php echo json_encode($functions->getGraphDates($frequencies)); ?>;
    var dateFormat = getDateFormat(frequency);
    var maxValue = findYAxisMax(data[0][frequency]) * 1.2;

    var start = $.datepicker.parseDate('yy-mm-dd', dates[frequency][0]);
    var end = $.datepicker.parseDate('yy-mm-dd', dates[frequency][1]);

    var chartTitle = '<?php echo lang("_GRAPH_ACTIVEUSERS_TITLE"); ?> ' + freqStrings[frequency];
    var xLabel = '<?php echo lang("_GRAPH_ACTIVEUSERS_XAXIS_LABEL"); ?>';
    var yLabel = '<?php echo lang("_GRAPH_ACTIVEUSERS_YAXIS_LABEL"); ?>';

    if (!hasEnoughData(data[0][frequency], start, end)) {
        drawEmptyGraph('activeusers', frequency, chartTitle, xLabel, yLabel, dates, dateFormat);
        missingData('#activeusers-' + frequency);
    } else {
        try {
            $.jqplot('activeusers-' + frequency, [data[0][frequency]], {
                title: chartTitle,
                seriesColors: ["#6666ff", "#ff8000"],
                gridPadding: {
                    right: 25,
                    left: 60
                },
                highlighter: {
                    show: true,
                    tooltipSeparator: ': ',
                    fadeTooltip: false,
                    sizeAdjust: 9
                },
                axesDefaults: {
                    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                    labelOptions: {
                        fontFamily: 'Verdana,Geneva,sans-serif',
                        fontSize: '10pt'
                    }
                },
                axes: {
                    xaxis: {
                        min: start,
                        tickInterval: getTickInterval(frequency),
                        renderer: $.jqplot.DateAxisRenderer,
                        tickOptions: {
                            formatString: dateFormat
                        },
                        label: xLabel
                    },
                    yaxis: {
                        min: 0,
                        max: maxValue,
                        tickOptions: {
                            formatString: "%d"
                        },
                        label: yLabel
                    }
                },
                seriesDefaults: {
                    lineWidth: 2,
                    markerOptions: {
                        show: false
                    }
                }
            });
        } catch (err) {
            drawEmptyGraph('activeusers', frequency, chartTitle, xLabel, yLabel, dates, dateFormat);
            noDataFound('#activeusers-' + frequency);
        }
    }

    if ($("#activeusers-table-" + frequency).length > 0) {
        // Create the HTML table.
        var headerArr = ['<?php echo lang("_GRAPH_ACTIVEUSERS_XAXIS_LABEL"); ?>',
            '<?php echo lang("_GRAPH_ACTIVEUSERS_SERIES_LABEL"); ?>'];
        createTable("#activeusers-table-" + frequency, headerArr, tableData['activeusers'][frequency], dates[frequency][0], frequency);
    }
}

function totalfiles(frequency) {
    var data = chartData['totalfiles'];
    var dates = <?php echo json_encode($functions->getGraphDates($frequencies)); ?>;
    var dateFormat = getDateFormat(frequency);
    var maxValue = findYAxisMax(data[0][frequency]) * 1.2;

    var start = $.datepicker.parseDate('yy-mm-dd', dates[frequency][0]);
    var end = $.datepicker.parseDate('yy-mm-dd', dates[frequency][1]);

    var chartTitle = '<?php echo lang("_GRAPH_TOTALFILES_TITLE"); ?> ' + freqStrings[frequency];
    var xLabel = '<?php echo lang("_GRAPH_TOTALFILES_XAXIS_LABEL"); ?>';
    var yLabel = '<?php echo lang("_GRAPH_TOTALFILES_YAXIS_LABEL"); ?>';

    if (!hasEnoughData(data[0][frequency], start, end)) {
        drawEmptyGraph('totalfiles', frequency, chartTitle, xLabel, yLabel, dates, dateFormat);
        missingData('#totalfiles-' + frequency);
    } else {
        try {
            $.jqplot('totalfiles-' + frequency, [data[0][frequency]], {
                title: chartTitle,
                seriesColors: ["#6666ff", "#ff8000"],
                gridPadding: {
                    right: 25,
                    left: 60
                },
                highlighter: {
                    show: true,
                    tooltipSeparator: ': ',
                    fadeTooltip: false,
                    sizeAdjust: 9
                },
                axesDefaults: {
                    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                    labelOptions: {
                        fontFamily: 'Verdana,Geneva,sans-serif',
                        fontSize: '10pt'
                    }
                },
                axes: {
                    xaxis: {
                        min: start,
                        tickInterval: getTickInterval(frequency),
                        renderer: $.jqplot.DateAxisRenderer,
                        tickOptions: {
                            formatString: dateFormat
                        },
                        label: xLabel
                    },
                    yaxis: {
                        min: 0,
                        max: maxValue,
                        tickOptions: {
                            formatString: "%d"
                        },
                        label: yLabel
                    }
                },
                seriesDefaults: {
                    lineWidth: 2,
                    markerOptions: {
                        show: false
                    }
                }
            });
        } catch (err) {
            drawEmptyGraph('totalfiles', frequency, chartTitle, xLabel, yLabel, dates, dateFormat);
            noDataFound('#totalfiles-' + frequency);
        }
    }

    if ($("#totalfiles-table-" + frequency).length > 0) {
        // Create the HTML table.
        var headerArr = ['<?php echo lang("_GRAPH_TOTALFILES_XAXIS_LABEL"); ?>',
            '<?php echo lang("_GRAPH_TOTALFILES_SERIES_LABEL"); ?>'];
        createTable("#totalfiles-table-" + frequency, headerArr, tableData['totalfiles'][frequency], dates[frequency][0], frequency);
    }
}

function totalusers(frequency) {
    var data = chartData['totalusers'];
    var dates = <?php echo json_encode($functions->getGraphDates($frequencies)); ?>;
    var dateFormat = getDateFormat(frequency);
    var maxValue = findYAxisMax(data[0][frequency]) * 1.2;

    var start = $.datepicker.parseDate('yy-mm-dd', dates[frequency][0]);
    var end = $.datepicker.parseDate('yy-mm-dd', dates[frequency][1]);

    var chartTitle = '<?php echo lang("_GRAPH_TOTALUSERS_TITLE"); ?> ' + freqStrings[frequency];
    var xLabel = '<?php echo lang("_GRAPH_TOTALUSERS_XAXIS_LABEL"); ?>';
    var yLabel = '<?php echo lang("_GRAPH_TOTALUSERS_YAXIS_LABEL"); ?>';

    if (!hasEnoughData(data[0][frequency], start, end)) {
        drawEmptyGraph('totalusers', frequency, chartTitle, xLabel, yLabel, dates, dateFormat);
        missingData('#totalusers-' + frequency);
    } else {
        try {
            $.jqplot('totalusers-' + frequency, [data[0][frequency]], {
                title: chartTitle,
                seriesColors: ["#6666ff", "#ff8000"],
                gridPadding: {
                    right: 25,
                    left: 60
                },
                highlighter: {
                    show: true,
                    tooltipSeparator: ': ',
                    fadeTooltip: false,
                    sizeAdjust: 9
                },
                axesDefaults: {
                    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                    labelOptions: {
                        fontFamily: 'Verdana,Geneva,sans-serif',
                        fontSize: '10pt'
                    }
                },
                axes: {
                    xaxis: {
                        min: start,
                        tickInterval: getTickInterval(frequency),
                        renderer: $.jqplot.DateAxisRenderer,
                        tickOptions: {
                            formatString: dateFormat
                        },
                        label: xLabel
                    },
                    yaxis: {
                        min: 0,
                        max: maxValue,
                        tickOptions: {
                            formatString: "%d"
                        },
                        label: yLabel
                    }
                },
                seriesDefaults: {
                    lineWidth: 2,
                    markerOptions: {
                        show: false
                    }
                }
            });
        } catch (err) {
            drawEmptyGraph('totalusers', frequency, chartTitle, xLabel, yLabel, dates, dateFormat);
            noDataFound('#totalusers-' + frequency);
        }
    }

    if ($("#totalusers-table-" + frequency).length > 0) {
        // Create the HTML table.
        var headerArr = ['<?php echo lang("_GRAPH_TOTALUSERS_XAXIS_LABEL"); ?>',
            '<?php echo lang("_GRAPH_TOTALUSERS_SERIES_LABEL"); ?>'];
        createTable("#totalusers-table-" + frequency, headerArr, tableData['totalusers'][frequency], dates[frequency][0], frequency);
    }
}

/*
 * Finds the highest y axis value from a number of data arrays. Used to determine the axis scaling.
 */
function findYAxisMax(/* data arrays */) {
    var max = 0;

    for (var i = 0; i < arguments.length; i++) {
        $.each(arguments[i], function () {
            if (Number(this[1]) > max) max = Number(this[1]);
        });
    }

    return max;
}

/*
 * Checks that a data array has at least 2 data points between the given start and end dates.
 */
function hasEnoughData(dataArray, startDate, endDate) {
    var count = 0;

    $.each(dataArray, function () {
        var currentDate = $.datepicker.parseDate('yy-mm-dd', this[0]);
        if (currentDate >= startDate && currentDate <= endDate) {
            count++;
            if (count >= 2) return true;
        }
    });

    return (count >= 2);
}

/*
 * Draws an empty graph with no series in it, to indicate that an error occurred.
 */
function drawEmptyGraph(divName, frequency, chartTitle, xLabel, yLabel, dates, dateFormat) {
    $.jqplot(divName + '-' + frequency, [
        [null]
    ], {
        title: chartTitle,
        gridPadding: {
            right: 25,
            left: 60
        },
        axesDefaults: {
            labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
            labelOptions: {
                fontFamily: 'Verdana,Geneva,sans-serif',
                fontSize: '10pt'
            }
        },
        axes: {
            xaxis: {
                min: dates[frequency][0],
                max: dates[frequency][1],
                renderer: $.jqplot.DateAxisRenderer,
                tickOptions: {
                    formatString: dateFormat
                },
                label: xLabel
            },
            yaxis: {
                label: yLabel
            }
        }
    });
}

/*
 * Displays an error indicating to the user that there are not enough data points available to plot a chart.
 */
function missingData(chartDiv) {
    $(chartDiv).find('.jqplot-title').append(' <span class="chart_error">(<?php echo lang("_TOO_FEW_DATAPOINTS_ERROR"); ?>)</span>');
}

/*
 * Displays an error indicating to the user that no graphable data was found.
 */
function noDataFound(chartDiv) {
    $(chartDiv).find('.jqplot-title').append(' <span class="chart_error">(<?php echo lang("_NO_DATA_ERROR"); ?>)</span>');
}

/*
 * Creates a HTML table out of a data array, and places it in the assigned div. Requires an array containing table header titles,
 * and a two dimensional array containing body data. The startDate argument should be a string like "2011-01-20 00:00:00".
 */
function createTable(divId, headerArray, bodyArray, startDate, frequency) {
    var tableBody = '<table><tr class="headerrow">';

    $.each(headerArray, function () {
        tableBody += '<th>' + this + '</th>';
    });

    tableBody += '</tr>';

    // Get the first date that we are interested in displaying in the table.
    var start = $.datepicker.parseDate('yy-mm-dd', startDate.split(" ")[0]);

    $.datepicker.setDefaults({
        monthNames: <?php echo lang("_DP_monthNames"); ?>
    });

    var formatString = "";
    if (frequency == "year") {
        formatString = "yy";
    } else if (frequency == "month") {
        formatString = "MM yy";
    } else if (frequency == "week") {
        formatString = "week";
    } else { // frequency == "day"
        formatString = "dd MM yy";
    }

    $.each(bodyArray, function () {
        // ASSUMPTION: the first column in the array is a date string in the format "2011-01-20 00:00:00".
        var date = $.datepicker.parseDate('yy-mm-dd', this[0] = this[0].split(" ")[0]);

        if (date >= start) {
            var weekNum = 0;
            if (formatString == "week") {
                weekNum = $.datepicker.iso8601Week(date);
                this[0] = $.datepicker.formatDate('<?php echo lang("_DP_weekHeader"); ?> ' + weekNum + ', yy', date);
            } else {
                this[0] = $.datepicker.formatDate(formatString, date);
            }
            var tableRow = "";

            // Create the row and add to the table.
            $.each(this, function (key, value) {
                tableRow += '<td>' + value + '</td>';
            })
            tableBody += '<tr class="altcolor">' + tableRow + '</tr>';
        }
    })

    // Add the table to its assigned div.
    tableBody += '</table>';
    $(divId).append(tableBody);
}

function openTable(tableDiv) {
    $("#" + tableDiv).dialog("open");
}
</script>