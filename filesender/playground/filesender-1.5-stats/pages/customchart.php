<div class="box"><?php echo '<div id="pageheading">'.lang("_STATS").'</div>'; ?>
	<?php echo lang("_STATS_INFO_CUSTOM"); ?>
	<a href="index.php"><?php echo lang("_OVERVIEW_LINK"); ?></a>
</div>

<?php 
	$frequencies = $config["stats_available_frequencies"];
?> 

<div class="box"><form id="customchartform" action="#">
	<table width="100%" border="0">
		<tbody>
			<tr> 
				<td width="130px" class="mandatory"><label for="chart"><?php echo lang("_CHART_TYPE"); ?></label></td>
				<td>
					<select name="chart" id="chart">
						<option value="updown"><?php echo lang("_GRAPH_UPDOWN_TITLE"); ?></option>
						<option value="filesizes"><?php echo lang("_GRAPH_FILESIZES_TITLE"); ?></option>
						<option value="vouchers"><?php echo lang("_GRAPH_VOUCHERS_TITLE"); ?></option>
						<option value="totalupdown"><?php echo lang("_GRAPH_TOTALUPDOWN_TITLE"); ?></option>
						<option value="activefiles"><?php echo lang("_GRAPH_ACTIVEFILES_TITLE"); ?></option>
						<option value="activeusers"><?php echo lang("_GRAPH_ACTIVEUSERS_TITLE"); ?></option>
						<option value="totalfiles"><?php echo lang("_GRAPH_TOTALFILES_TITLE"); ?></option>
						<option value="totalusers"><?php echo lang("_GRAPH_TOTALUSERS_TITLE"); ?></option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="mandatory"><label for="frequency"><?php echo lang("_FREQUENCY"); ?></label></td>
				<td>
					<select name="frequency" id="frequency">
						<?php
							foreach ($frequencies as $frequency) {
								echo '<option value="' . $frequency . '">' . lang('_FREQUENCY_' . strtoupper($frequency)) . '</option>';
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td class="mandatory"><label for="startdate">Start date:</label></td>
				<td><input class="datepicker" id="startdate" name="startdate" title="<?php echo lang('_DP_dateFormat'); ?>" /></td>
			</tr>
			<tr>
				<td class="mandatory"><label for="enddate">End date:</label></td>
				<td><input class="datepicker" id="enddate" name="enddate" title="<?php echo lang('_DP_dateFormat'); ?>" /></td>
			</tr>
			<tr>
				<td align="right" valign="middle"></td>
				<td><input type="submit" id="customchartsubmit" value="<?php echo lang('_SEND'); ?>" /></td>
			</tr>
		</tbody>
	</table>
</form></div>
<div class="bigchart box" id="customchart"></div>
<div id="dialog-invalid-dates" title="" style="display:none">
	<?php echo lang("_INVALID_DATE_ERROR"); ?>
</div>
<?php
	$arr = array();
	
	
?>
<script type="text/javascript">
	var datepickerDateFormat = '<?php echo lang('_DP_dateFormat'); ?>';
	var frequency = "<?php echo $config['stats_default_frequency']; ?>";

	$(document).ready(function() {
		// Load the data and initialise settings.
		$("#customchart").hide();
		$.getJSON('data/data.json', function(data) {
			chartData = data["charts"];
			tableData = data["tables"];
			setMinimumDate();
			initSettings();
			updateSettings();
			
			$("#customchartform").submit(function() {
				if (datesAreValid()) {
					// Create chart for the first time.
					$("#customchart").show();
					updateSettings();
					customChart();
				} else {
					$("#dialog-invalid-dates").dialog("open");
				}
				return false; // This is to cancel the form submit so it doesn't reload the page.
			});
		});
		
		$("#dialog-invalid-dates").dialog({ autoOpen: false, height: 200, width: 350, modal: true, title: '<?php echo lang("_INVALID_DATE_ERROR_TITLE"); ?>',		
		buttons: {
			'<?php echo lang("_OK"); ?>': function() {
				$( this ).dialog( "close" );
				}
			}
		})
		
		$("#frequency").val(frequency);
		
		$(".datepicker").attr('readonly' , 'true');
		$(".datepicker").datepicker({maxDate: new Date(), altFormat: "d-m-yy" });
		$(".datepicker").datepicker("option", "dateFormat", "<?php echo lang("_DP_dateFormat"); ?>" );
		$("#enddate").datepicker("setDate", new Date());
			
		// set datepicker language
		$.datepicker.setDefaults({
		closeText: '<?php echo lang("_DP_closeText"); ?>',
		prevText: '<?php echo lang("_DP_prevText"); ?>',
		nextText: '<?php echo lang("_DP_nextText"); ?>',
		currentText: "<?php echo lang("_DP_currentText"); ?>",
		monthNames: <?php echo lang("_DP_monthNames"); ?>,
		monthNamesShort: <?php echo lang("_DP_monthNamesShort"); ?>,
		dayNames: <?php echo lang("_DP_dayNames"); ?>,
		dayNamesShort: <?php echo lang("_DP_dayNamesShort"); ?>,
		dayNamesMin: <?php echo lang("_DP_dayNamesMin"); ?>,
		weekHeader: '<?php echo lang("_DP_weekHeader"); ?>',
		dateFormat: '<?php echo lang("_DP_dateFormat"); ?>',
		firstDay: <?php echo lang("_DP_firstDay"); ?>,
		isRTL: <?php echo lang("_DP_isRTL"); ?>,
		showMonthAfterYear: <?php echo lang("_DP_showMonthAfterYear"); ?>,
		yearSuffix: '<?php echo lang("_DP_yearSuffix"); ?>'});
		
		// Make the minimum datepicker date change if the frequency or chart selection changes.
		$("#frequency").change(function() {
			setMinimumDate();
		});
		
		$("#chart").change(function() {
			setMinimumDate();
		});
	});
	
	function setMinimumDate() {
		// Figure out the earliest available date in the data set and restrict datepicker to that.
		var minimumDate = new Date(chartData[$("#chart").val()][0][$("#frequency").val()][0][0]);
		$(".datepicker").datepicker('option', 'minDate', minimumDate);
		$("#startdate").datepicker('setDate', minimumDate);
	}
	
	function datesAreValid() {
		try {
			var startDate = $.datepicker.parseDate("<?php echo lang("_DP_dateFormat"); ?>", $("#startdate").val());
			var endDate = $.datepicker.parseDate("<?php echo lang("_DP_dateFormat"); ?>", $("#enddate").val());
			
			if (startDate <= endDate && endDate <= new Date()) {
				// Set the validated dates as values in the fields.
				$("#startdate").val($.datepicker.formatDate("<?php echo lang("_DP_dateFormat"); ?>", startDate));
				$("#enddate").val($.datepicker.formatDate("<?php echo lang("_DP_dateFormat"); ?>", endDate));
				return true;
			}
		} catch (err) {
			return false;
		}
		return false;
	}

	function initSettings() {
		// Sets up the initial settings array which is used for drawing the graphs.
		settings = {
			seriesColors: ["#6666ff", "#ff8000", "#6eff70", "#bd693f"],
			gridPadding: {
				right: 25,
				left: 60
			},
			highlighter: {
				show: true,
				tooltipSeparator: ': ',
				fadeTooltip: false,
				sizeAdjust: 9
			},
			axesDefaults: {
				labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
				labelOptions: {
					fontFamily: 'Verdana,Geneva,sans-serif',
					fontSize: '10pt'
				}
			},
			axes: {
				xaxis: {
					renderer: $.jqplot.DateAxisRenderer,
					tickOptions: {
						formatString: "%d %b '%y"
					},
				},
				yaxis: { 
					tickOptions: {
						formatString: "%d"
					},
				}
			},
			seriesDefaults: {
				lineWidth: 2,
				markerOptions: {
					show: false
				}
			},
			legend: {
				show: true
			}
		};
	}
	
	/*
	 * Displays an error indicating to the user that no graphable data was found.
	 */
	function noDataFound(graphDiv) {
		$(graphDiv).html('<?php echo lang("_NO_DATA_ERROR"); ?>');
	}
	
	function getDateFormat(frequency) {
		if (frequency == "year") {
			return "%Y";
		} else if (frequency == "month" || frequency == "week") {
			return "%b %Y";
		} else { // frequency == "day"
			return "%d %b '%y";
		}
	}
	
	function updateSettings() {
		// Set the settings that are different for each individual graph type when the form changes.
		var type = $("#chart").val();
		var freq = $("#frequency").val();
		
		settings['axes']['xaxis']['min'] = $.datepicker.parseDate('<?php echo lang("_DP_dateFormat"); ?>', $("#startdate").val());
		settings['axes']['xaxis']['max'] = $.datepicker.parseDate('<?php echo lang("_DP_dateFormat"); ?>', $("#enddate").val());
		settings['axes']['xaxis']['tickOptions']['formatString'] = getDateFormat(freq);
		settings['axes']['yaxis']['min'] = 0;
		settings['axes']['yaxis']['max'] = getHighestValue() * 1.2;
		settings['axes']['yaxis']['renderer'] = $.jqplot.LinearAxisRenderer;
		settings['stackSeries'] = false;
		settings['seriesDefaults']['fill'] = false;
		settings['seriesDefaults']['fillAndStroke'] = false;
		settings['legend']['show'] = true;
			
		if (type == "updown") {
			settings['title'] = '<?php echo lang("_GRAPH_UPDOWN_TITLE"); ?>';
			settings['series'] = [
				{label:'<?php echo lang("_GRAPH_UPDOWN_UPLOADS_LABEL"); ?>'},
				{label:'<?php echo lang("_GRAPH_UPDOWN_DOWNLOADS_LABEL"); ?>'}
			];
			settings['axes']['xaxis']['label'] = '<?php echo lang("_GRAPH_UPDOWN_XAXIS_LABEL"); ?>';
			settings['axes']['yaxis']['label'] = '<?php echo lang("_GRAPH_UPDOWN_YAXIS_LABEL"); ?>';
		} else if (type == "filesizes") {
			settings['title'] = '<?php echo lang("_GRAPH_FILESIZES_TITLE"); ?>';
			settings['stackSeries'] = true;
			settings['axes']['yaxis']['max'] = null;
			settings['series'] = [
				{label:'<?php echo lang("_GRAPH_FILESIZES_FIRST_LABEL"); ?>'},
				{label:'<?php echo lang("_GRAPH_FILESIZES_SECOND_LABEL"); ?>'},
				{label:'<?php echo lang("_GRAPH_FILESIZES_THIRD_LABEL"); ?>'},
				{label:'<?php echo lang("_GRAPH_FILESIZES_FOURTH_LABEL"); ?>'}
			];
			settings['axes']['xaxis']['label'] = '<?php echo lang("_GRAPH_FILESIZES_XAXIS_LABEL"); ?>';
			settings['axes']['yaxis']['label'] = '<?php echo lang("_GRAPH_FILESIZES_YAXIS_LABEL"); ?>';
			settings['seriesDefaults']['fill'] = true;
			settings['seriesDefaults']['fillAndStroke'] = true;
		} else if (type == "vouchers") {
			settings['title'] = '<?php echo lang("_GRAPH_VOUCHERS_TITLE"); ?>';
			settings['series'] = [
				{label:'<?php echo lang("_GRAPH_VOUCHERS_SENT_LABEL"); ?>'},
				{label:'<?php echo lang("_GRAPH_VOUCHERS_USED_LABEL"); ?>'}
			];
			settings['axes']['xaxis']['label'] = '<?php echo lang("_GRAPH_UPDOWN_XAXIS_LABEL"); ?>';
			settings['axes']['yaxis']['label'] = '<?php echo lang("_GRAPH_UPDOWN_YAXIS_LABEL"); ?>';
		} else if (type == "totalupdown") {
			settings['title'] = '<?php echo lang("_GRAPH_TOTALUPDOWN_TITLE"); ?>';
			settings['series'] = [
				{label:'<?php echo lang("_GRAPH_TOTALUPDOWN_UPLOADS_LABEL"); ?>'},
				{label:'<?php echo lang("_GRAPH_TOTALUPDOWN_DOWNLOADS_LABEL"); ?>'}
			];
			settings['axes']['xaxis']['label'] = '<?php echo lang("_GRAPH_TOTALUPDOWN_XAXIS_LABEL"); ?>';
			settings['axes']['yaxis']['label'] = '<?php echo lang("_GRAPH_TOTALUPDOWN_YAXIS_LABEL"); ?>';
		} else if (type == "activefiles") {
			settings['title'] = '<?php echo lang("_GRAPH_ACTIVEFILES_TITLE"); ?>';
			settings['series'] = [{label:'<?php echo lang("_GRAPH_ACTIVEFILES_SERIES_LABEL"); ?>'}];
			settings['axes']['xaxis']['label'] = '<?php echo lang("_GRAPH_ACTIVEFILES_XAXIS_LABEL"); ?>';
			settings['axes']['yaxis']['label'] = '<?php echo lang("_GRAPH_ACTIVEFILES_YAXIS_LABEL"); ?>';
			settings['legend']['show'] = false;
		} else if (type == "activeusers") {
			settings['title'] = '<?php echo lang("_GRAPH_ACTIVEUSERS_TITLE"); ?>';
			settings['series'] = [{label:'<?php echo lang("_GRAPH_ACTIVEUSERS_SERIES_LABEL"); ?>'}];
			settings['axes']['xaxis']['label'] = '<?php echo lang("_GRAPH_ACTIVEUSERS_XAXIS_LABEL"); ?>';
			settings['axes']['yaxis']['label'] = '<?php echo lang("_GRAPH_ACTIVEUSERS_YAXIS_LABEL"); ?>';
			settings['legend']['show'] = false;
		} else if (type == "totalfiles") {
			settings['title'] = '<?php echo lang("_GRAPH_TOTALFILES_TITLE"); ?>';
			settings['series'] = [{label:'<?php echo lang("_GRAPH_TOTALFILES_SERIES_LABEL"); ?>'}];
			settings['axes']['xaxis']['label'] = '<?php echo lang("_GRAPH_TOTALFILES_XAXIS_LABEL"); ?>';
			settings['axes']['yaxis']['label'] = '<?php echo lang("_GRAPH_TOTALFILES_YAXIS_LABEL"); ?>';
			settings['legend']['show'] = false;
		} else if (type == "totalusers") {
			settings['title'] = '<?php echo lang("_GRAPH_TOTALUSERS_TITLE"); ?>';
			settings['series'] = [{label:'<?php echo lang("_GRAPH_TOTALUSERS_SERIES_LABEL"); ?>'}];
			settings['axes']['xaxis']['label'] = '<?php echo lang("_GRAPH_TOTALUSERS_XAXIS_LABEL"); ?>';
			settings['axes']['yaxis']['label'] = '<?php echo lang("_GRAPH_TOTALUSERS_YAXIS_LABEL"); ?>';
			settings['legend']['show'] = false;
		} 
	}
	
	function getHighestValue() {
		// Work out the highest value in the currently displayed data series, to set max limit on y-axis.
		var arr = chartData[$("#chart").val()];
		var highestValue = 0;
		var startDate = $.datepicker.parseDate('<?php echo lang("_DP_dateFormat"); ?>', $("#startdate").val());
		var endDate = $.datepicker.parseDate('<?php echo lang("_DP_dateFormat"); ?>', $("#enddate").val());
		
		
		for (var series = 0; series < arr.length; series++) { // For each series in the data
			var currentSeries = arr[series][$("#frequency").val()];
			for (var dataPoint = 0; dataPoint < currentSeries.length; dataPoint++) { // For each data point in the series.
				var dpDate = new Date(currentSeries[dataPoint][0]);
				
				// If the point falls within the current date range, check the value.
				if (dpDate >= startDate && dpDate <= endDate && currentSeries[dataPoint][1] > highestValue) {
					highestValue = currentSeries[dataPoint][1];
				} else if (dpDate > endDate) {
					break;
				}
			}
		}
		return highestValue;
	}
	
	function customChart() {
		// Draw a chart based on the current settings.
		$('#customchart').empty();
		data = chartData[$("#chart").val()];
		
		series = new Array();
		
		for (i = 0; i < data.length; i++) {
			series.push(data[i][$("#frequency").val()]);
		}
	
		try {
			graph = $.jqplot('customchart', series, settings);
		} catch(err) {
			noDataFound("#customchart");
		}
	}
</script>