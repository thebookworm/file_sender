<?php

/*
 * FileSender www.filesender.org
 * 
 * Copyright (c) 2009-2012, AARNet, Belnet, HEAnet, SURFnet, UNINETT
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * *	Redistributions of source code must retain the above copyright
 * 	notice, this list of conditions and the following disclaimer.
 * *	Redistributions in binary form must reproduce the above copyright
 * 	notice, this list of conditions and the following disclaimer in the
 * 	documentation and/or other materials provided with the distribution.
 * *	Neither the name of AARNet, Belnet, HEAnet, SURFnet and UNINETT nor the
 * 	names of its contributors may be used to endorse or promote products
 * 	derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// ---------------------------------------
// Format bytes into readbable text format
// ---------------------------------------
function formatBytes($bytes, $precision = 2) {

    if($bytes >  0) 
    {
        $units = array(' Bytes', ' kB', ' MB', ' GB', ' TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        $bytes /= pow(1024, $pow);

        return round($bytes, $precision) . '' . $units[$pow];
    }
    return 0;
} 

// ---------------------------------------
// Create Unique ID for vouchers
// ---------------------------------------
function getGUID() {

    return sprintf(
        '%08x-%04x-%04x-%02x%02x-%012x',
        mt_rand(),
        mt_rand(0, 65535),
        bindec(substr_replace(sprintf('%016b', mt_rand(0, 65535)), '0100', 11, 4)),
        bindec(substr_replace(sprintf('%08b', mt_rand(0, 255)), '01', 5, 2)),
        mt_rand(0, 255),
        mt_rand()
    );
}

// ---------------------------------------
// Replace illegal chars with _ character in supplied filenames
// ---------------------------------------

function sanitizeFilename($filename){

    if (!empty($filename)) {
        $filename = preg_replace("/^\./", "_", $filename); //return preg_replace("/[^A-Za-z0-9_\-\. ]/", "_", $filename);
        return $filename;
    } else {
        //trigger_error("invalid empty filename", E_USER_ERROR);
        return "";
    }
}

// ---------------------------------------
// Error if fileUid doesn't look sane
// ---------------------------------------

function ensureSaneFileUid($fileuid){

    global $config;

    if (preg_match($config['voucherRegEx'], $fileuid) and strLen($fileuid) == $config['voucherUIDLength']) {
        return $fileuid;
    } else {
        trigger_error("invalid file uid $fileuid", E_USER_ERROR);
    }
}

class Functions {

    private static $instance = NULL;
    private $saveLog;
    private $db;
    private $sendmail;
    private $authsaml;
    private $authvoucher;

    // the following fields are returned without fileUID to stop unauthorised users accessing the fileUID
    public $returnFields = " fileid, fileexpirydate, fileto , filesubject, fileactivitydate, filemessage, filefrom, filesize, fileoriginalname, filestatus, fileip4address, fileip6address, filesendersname, filereceiversname, filevouchertype, fileauthuseruid, fileauthuseremail, filecreateddate, fileauthurl, fileuid, filevoucheruid ";	

    public function __construct() {

        $this->saveLog = Log::getInstance();
        $this->db = DB::getInstance();
	    $this->sendmail = Mail::getInstance();
        $this->authsaml = AuthSaml::getInstance();
        $this->authvoucher = AuthVoucher::getInstance();
    }

    public static function getInstance() {
        // Check for both equality and type		
        if(self::$instance === NULL) {
            self::$instance = new self();
        }
        return self::$instance;
    } 
	
    //--------------------------------------- CHECKED
    // Return Basic Database Statistics e.g. Up xx Gb (files xx) | Down xx Gb (files xx)
	// ---------------------------------------
    public function getStats() {

        global $config;

        $statString = "| UP: ";

        $statement =   $this->db->fquery("SELECT * FROM logs WHERE logtype='Uploaded'");
		$statement->execute();
		$count = $statement->rowCount();

        $statString = $statString.$count." files ";

        $statement = $this->db->fquery("SELECT SUM(logfilesize) as total_uploaded FROM logs WHERE logtype='Uploaded'");
		$statement->execute();
		$totalResult = $statement->fetch(PDO::FETCH_NUM);
		$totalResult = $totalResult[0];
        $statString = $statString."(".round($totalResult/1024/1024/1024)."GB) |" ;
		$stmnt = NULL;
		
      	$statement = $this->db->fquery("SELECT * FROM logs WHERE logtype='Download'");
      	$statement->execute();
		$count = $statement->rowCount();
        $statString = $statString." DOWN: ".$count." files ";
		
       	$statement =  $this->db->fquery("SELECT SUM(logfilesize) FROM logs WHERE logtype='Download'");
      	$statement->execute();
		$totalResult = $statement->fetch(PDO::FETCH_NUM);
		$totalResult = $totalResult[0];
        $statString = $statString."(".round($totalResult/1024/1024/1024)."GB) |";
		$stmnt = NULL;
		
        return $statString;

    }

    //--------------------------------------- CHECKED
    // Return Basic Database Statistics e.g. Up xx Gb (files xx) | Down xx Gb (files xx)
    // ---------------------------------------

    public function keyStats($year = NULL)
    {
        $excludedDomains = $this->excludedDomains();

        $yearString = "";

        if (isset($year)) {
            $yearString = "AND extract(year from statdate) = " . $year . " ";
        }

        // Retrieve numbers relating to file sizes from DB.
        $query = "SELECT max(statfilesize), sum(statfilesize), avg(statfilesize) FROM stats WHERE statlogtype='Uploaded' " . $excludedDomains . $yearString;
        $statement = $this->db->fquery($query);
        $statement->execute();
        $sizes = $statement->fetch(PDO::FETCH_NUM);

        $statement = $this->db->fquery("SELECT * FROM stats WHERE statlogtype='Uploaded' " . $excludedDomains . $yearString);
        $statement->execute();
        $totalUploads = $statement->rowCount();

        $twoGB = 2 * 1024 * 1024 * 1024;
        $statement = $this->db->fquery("SELECT * FROM stats WHERE statlogtype='Uploaded' AND statfilesize >= " . $twoGB . " " . $excludedDomains . $yearString);
        $statement->execute();
        $numLargerFiles = $statement->rowCount();
        $largerPercentage = round($numLargerFiles / $totalUploads * 100, 2);

        // Print file sizes numbers.
        $statString = "<p><b>" . lang("_STATS_KEY_SIZES") . "</b></p>
						<ul>
							<li>" . lang("_STATS_KEY_SIZES_BIGGEST") . round($sizes[0] / 1024 / 1024 / 1024, 2) . " GB</li>
							<li>" . lang("_STATS_KEY_SIZES_TOTAL") . round($sizes[1] / 1024 / 1024 / 1024, 2) . " GB</li>
							<li>" . lang("_STATS_KEY_SIZES_AVERAGE") . round($sizes[2] / 1024 / 1024, 2) . " MB</li>
							<li>" . lang("_STATS_KEY_SIZES_BIGGER") . $numLargerFiles . " (" . $largerPercentage . "%)</li>
							<li>" . lang("_STATS_KEY_SIZES_SMALLER") . ($totalUploads - $numLargerFiles) . " (" . (100 - $largerPercentage) . "%)</li>
						</ul>";


        // Get numbers related to uploads.
        $statement = $this->db->fquery("SELECT min(statdate) FROM stats WHERE statlogtype='Uploaded' " . $excludedDomains . $yearString);
        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_NUM);
        $firstUpload = strtotime($result[0]);
        $daysSinceFirstUpload = floor((time() - $firstUpload) / (60 * 60 * 24));

        $statement = $this->db->fquery("SELECT DISTINCT statfromhashed FROM stats WHERE statlogtype='Uploaded' " . $excludedDomains . $yearString);
        $statement->execute();
        $uniqueUploaders = $statement->rowCount();

        // Print upload numbers.
        $statString .= "<p><b>" . lang("_STATS_KEY_UPLOADS") . "</b></p>
						<ul>
							<li>" . lang("_STATS_KEY_UPLOADS_NUMBER") . $totalUploads . "</li>
							<li>" . lang("_STATS_KEY_UPLOADS_DATE") . date(lang("datedisplayformat"), $firstUpload) . "</li>
							<li>" . lang("_STATS_KEY_UPLOADS_DAILY") . round($totalUploads / $daysSinceFirstUpload, 1) . "</li>
							<li>" . lang("_STATS_KEY_UPLOADS_UNIQUE") . $uniqueUploaders . "</li>
							<li>" . lang("_STATS_KEY_UPLOADS_PER_USER") . round($totalUploads / $uniqueUploaders, 1) . "</li>
						</ul>";


        // Retrieve download numbers.
        $statement = $this->db->fquery("SELECT * FROM stats WHERE statlogtype='Download' " . $excludedDomains . $yearString);
        $statement->execute();
        $totalDownloads = $statement->rowCount();

        $statement = $this->db->fquery("SELECT min(statdate) FROM stats WHERE statlogtype='Download' " . $excludedDomains . $yearString);
        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_NUM);
        $firstDownload = strtotime($result[0]);
        $daysSinceFirstDownload = floor((time() - $firstDownload) / (60 * 60 * 24));

        $statement = $this->db->fquery("SELECT DISTINCT statfromhashed FROM stats WHERE statlogtype='Download' " . $excludedDomains . $yearString);
        $statement->execute();
        $uniqueDownloaders = $statement->rowCount();

        // Print download numbers.
        $statString .= "<p><b>" . lang("_STATS_KEY_DOWNLOADS") . "</b></p>
						<ul>
							<li>" . lang("_STATS_KEY_DOWNLOADS_NUMBER") . $totalDownloads . "</li>
							<li>" . lang("_STATS_KEY_DOWNLOADS_DATE") . date(lang("datedisplayformat"), $firstDownload) . "</li>
							<li>" . lang("_STATS_KEY_DOWNLOADS_DAILY") . round($totalDownloads / $daysSinceFirstDownload, 1) . "</li>
							<li>" . lang("_STATS_KEY_DOWNLOADS_UNIQUE") . $uniqueDownloaders . "</li>
							<li>" . lang("_STATS_KEY_DOWNLOADS_PER_USER") . round($totalDownloads / $uniqueDownloaders, 1) . "</li>
						</ul>";

        return $statString;
    }

    // ---------------------------------------
    // Returns a string containing a few "key" usage statistics.
    // ---------------------------------------

    private function excludedDomains()
    {
        global $config;
        $domains = "";

        foreach ($config['excluded_domains'] as $domain) {
            $domains .= "AND statfromdomain != '" . $domain . "' ";
        }
        return $domains;
    }

    // ---------------------------------------
    // Returns a string of excluded domains that can be appended to SQL queries on the stats table.
    // ---------------------------------------

    public function getYears()
    {
        $query = "SELECT DISTINCT extract(year from statdate) as year FROM stats WHERE statlogtype='Uploaded' " . $this->excludedDomains() . " ORDER BY year DESC";

        $statement = $this->db->fquery($query);
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_COLUMN, 0);
    }

    // ---------------------------------------
    // Returns an array containing the years that there exist data for in the stats table (e.g. ['2010', '2011', '2012']).
    // ---------------------------------------

    public function upDownStats($frequencies, $logType = 'Uploaded')
    {
        $arr = array();
        $excluded = $this->excludedDomains();

        // Loop through the required frequencies and make a query for each of them.
        foreach ($frequencies as $frequency) {
            $query = "WITH dates AS (
                            SELECT date_trunc('$frequency', generate_series((SELECT min(statdate) FROM stats), now(), '1 $frequency')) AS stamp
                        ),
                        updown AS (
	                        SELECT date_trunc('$frequency', statdate) AS stamp, COUNT(*) AS rowcount
	                        FROM stats
	                        WHERE statlogtype = '$logType'
	                        $excluded
	                        GROUP BY stamp
                        )
                        SELECT dates.stamp::TIMESTAMP WITHOUT TIME ZONE, COALESCE(updown.rowcount, 0) AS rowcount
                        FROM dates
                        LEFT JOIN updown ON dates.stamp = updown.stamp
                        ORDER BY stamp ASC";

            // Execute query and add the resulting table to our return array.
            $statement = $this->db->fquery($query);
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_NUM);
            $stmnt = NULL;

            $this->removeCurrentPeriod($frequency, $result);

            $arr[$frequency] = $result;
        }
        return $arr;
    }

    // ---------------------------------------
    // Returns true if the lastDate argument is in the same time period (year, month, week or day) as the current date.
    // ---------------------------------------

    private function removeCurrentPeriod($frequency, &$result)
    {
        end($result);

        if (isset($result[key($result)][0]) && $this->isCurrentPeriodIncomplete($frequency, $result[key($result)][0])) {
            array_pop($result);
        }
    }

    // ---------------------------------------
    // Removes the last data point from an array if the time period in which that data point falls is still incomplete.
    // ---------------------------------------

    private function isCurrentPeriodIncomplete($frequency, $lastDate)
    {
        $today = new DateTime();
        $lastDate = new DateTime($lastDate);

        switch ($frequency) {
            case "year":
                return (int)$today->format("Y") == (int)$lastDate->format("Y");
            case "month":
                return (int)$today->format("m") == (int)$lastDate->format("m");
            case "week":
                return (int)$today->format("W") == (int)$lastDate->format("W");
            case "day":
                return (int)$today->format("d") == (int)$lastDate->format("d");
            default:
                return false;
        }
    }

    // ---------------------------------------
    // Gets the data for the "uploads and downloads" graph from DB.
    // ---------------------------------------

    public function vouchersStats($frequencies, $dates, $usedVouchers = false) {
        $arr = array();

        //$type = "";
        if ($usedVouchers) $type = "statlogtype = 'Uploaded' AND statauthuseruid = '' ";
        else $type = "statlogtype = 'Voucher Sent'";

        // Loop through the required frequencies and make a query for each of them.
        foreach ($frequencies as $frequency) {
            // Select our columns: date (truncated to the current frequency) and the number of sent or used vouchers.
            $query = "WITH dates AS ("
                    ."    SELECT date_trunc('$frequency', generate_series((SELECT min(statdate) FROM stats), now(), '1 $frequency')) AS stamp"
                    ."), "
                    ."vouchers AS ("
                    ."   SELECT date_trunc('$frequency', statdate) AS stamp, COUNT(*) AS rowcount "
                    ."   FROM stats "
                    ."   WHERE $type "
                    ."   GROUP BY stamp"
                    .") "
                    ."SELECT dates.stamp::TIMESTAMP WITHOUT TIME ZONE, COALESCE(vouchers.rowcount, 0) AS rowcount "
                    ."FROM dates "
                    ."LEFT JOIN vouchers ON dates.stamp = vouchers.stamp "
                    ."ORDER BY stamp ASC";

            // Execute query and add the resulting table to our return array.
            $statement = $this->db->fquery($query);
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_NUM);
            $stmnt = NULL;
            $this->removeCurrentPeriod($frequency, $result);

            $arr[$frequency] = $result;
        }
        return $arr;
    }

    // ---------------------------------------
    // Gets the data for the "invite vouchers" graph from DB.
    // ---------------------------------------

    public function fileSizesStats($frequencies, $dates, $minSize = NULL, $maxSize = NULL)
    {
        $arr = array();

        // Loop through the required frequencies and make a query for each of them.
        foreach ($frequencies as $frequency) {
            // Truncate dates to correct precision.
            $query = "WITH x AS (
						SELECT date_trunc('" . $frequency . "', statdate) AS " . $frequency . "
						FROM stats
						WHERE statlogtype = 'Uploaded' ";

            // Add optional restraints for size and date ranges.
            if ($minSize != NULL) {
                $query .= "AND statfilesize > " . $minSize . " ";
            }

            if ($maxSize != NULL) {
                $query .= "AND statfilesize <= " . $maxSize . " ";
            }

            // Be sure to exclude any domains that we don't want in our stats.
            $query .= $this->excludedDomains();

            // Get the first and last dates from the data series and count # of uploads per date.
            $query .= "),y AS (
							SELECT min(" . $frequency . ") AS start, max(" . $frequency . ") AS stop
							FROM x
						) SELECT DISTINCT
							z." . $frequency . "
							,count(x." . $frequency . ") OVER (ORDER BY z." . $frequency . ") AS uploads
						FROM (";

            // Ensure that also dates with 0 uploads are included in the table.
            $query .= "SELECT generate_series('" . $this->getStartPoint($frequency, $dates[$frequency][0], $dates[$frequency][1]) . "', now(), '1 " . $frequency . "')::timestamp without time zone
						AS " . $frequency . " FROM y
						) z LEFT   JOIN x USING (" . $frequency . ")
						ORDER  BY 1";

            // Execute query and add the resulting table to our return array.
            $statement = $this->db->fquery($query);
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_NUM);
            $stmnt = NULL;

            $this->removeCurrentPeriod($frequency, $result);

            $arr[$frequency] = $result;
        }
        return $arr;
    }

    // ---------------------------------------
    // Gets the data for the "file sizes" graph from DB.
    // ---------------------------------------

    function getStartPoint($precision = "month", $startDate = NULL, $endDate = NULL)
    {
        $query = "SELECT min(date_trunc('" . $precision . "', statdate)) FROM stats WHERE statlogtype='Uploaded'";

        $statement = $this->db->fquery($query);
        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_NUM);
        $stmnt = NULL;
        return $result[0];
    }

    // ---------------------------------------
    // Gets the data for the "total uploads and downloads in gigabytes" graph from DB.
    // ---------------------------------------

    public function totalUpDownStats($frequencies, $dates, $logType = 'Uploaded')
    {
        $arr = array();

        // Loop through the required frequencies and make a query for each of them.
        foreach ($frequencies as $frequency) {
            // Set up the basic query. We want two columns: date (truncated to frequency) and a running total of the filesizes up to that date.
            $query = "SELECT DISTINCT date_trunc('" . $frequency . "', statdate) AS " . $frequency . ",
					  CAST (sum(statfilesize) OVER (ORDER BY date_trunc('" . $frequency . "', statdate)) as bigint) / (1024 * 1024 * 1024) AS total 
					  FROM stats 
					  WHERE statlogtype = '" . $logType . "' ";

            // Be sure to exclude any domains that we don't want in our stats.
            $query .= $this->excludedDomains();

            $query .= "ORDER BY " . $frequency;

            // Execute query and add the resulting table to our return array.
            $statement = $this->db->fquery($query);
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_NUM);
            $stmnt = NULL;

            $this->removeCurrentPeriod($frequency, $result);

            $arr[$frequency] = $result;
        }
        return $arr;
    }

    // ---------------------------------------
    // Gets the data for the "total files" graph from DB.
    // ---------------------------------------

    public function totalFilesStats($frequencies, $dates)
    {
        $arr = array();

        // Loop through the required frequencies and make a query for each of them.
        foreach ($frequencies as $frequency) {
            // Set up the basic query. We want two columns: date (truncated to frequency) and total users up to that date.
            $query = "SELECT DISTINCT date_trunc('" . $frequency . "', statdate) AS " . $frequency . ",
					  CAST (count(*) OVER (ORDER BY date_trunc('" . $frequency . "', statdate)) as bigint) AS total 
					  FROM stats 
					  WHERE statlogtype = 'Uploaded' ";

            // Be sure to exclude any domains that we don't want in our stats.
            $query .= $this->excludedDomains();

            $query .= "ORDER BY " . $frequency;

            // Execute query and add the resulting table to our return array.
            $statement = $this->db->fquery($query);
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_NUM);
            $stmnt = NULL;

            $this->removeCurrentPeriod($frequency, $result);

            $arr[$frequency] = $result;
        }
        return $arr;
    }

    // ---------------------------------------
    // Gets the data for the "total files" graph from DB.
    // ---------------------------------------

    public function totalUsersStats($frequencies, $dates)
    {
        $arr = array();

        // Loop through the required frequencies and make a query for each of them.
        foreach ($frequencies as $frequency) {
            // Begin building the SQL query. The sub query in the FROM clause filters out the unique users from our available data.
            // We then add up the number of unique users up to a specific date (the second line).
            $query = "SELECT date_trunc('" . $frequency . "', statdate) AS " . $frequency . ",
					  CAST (sum(count(statfromhashed)) OVER (ORDER BY date_trunc('" . $frequency . "', statdate)) AS bigint) AS total
					  FROM (
						SELECT DISTINCT ON (statfromhashed) statdate, statfromhashed 
						FROM stats 
						WHERE statlogtype = 'Uploaded' ";

            // Be sure to exclude any domains that we don't want in our stats.
            $query .= $this->excludedDomains();

            // Finish the query.
            $query .= "ORDER BY statfromhashed, statdate) AS users_uniq GROUP BY " . $frequency;

            // Execute query and add the resulting table to our return array.
            $statement = $this->db->fquery($query);
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_NUM);
            $stmnt = NULL;

            $this->removeCurrentPeriod($frequency, $result);

            $arr[$frequency] = $result;
        }
        return $arr;
    }

    // ---------------------------------------
    // Gets the data for the "active files" graph from DB.
    // ---------------------------------------

    public function activeFilesStats($frequencies)
    {
        global $config;
        $arr = array();

        $excluded = "";

        foreach ($config['excluded_domains'] as $domain) {
            $excluded .= "AND (filefrom !~ '" . $domain . "') ";
        }

        foreach ($frequencies as $frequency) {
            $query = "WITH dates AS (
                            SELECT date_trunc('$frequency', generate_series((SELECT min(statdate) FROM stats), now(), '1 $frequency')) AS stamp
                        ),
                        active_files AS (
                            SELECT date_trunc('$frequency', date) AS stamp, count(1) AS rowcount
                            FROM files, (SELECT generate_series((SELECT min(statdate) FROM stats), now(), '1 $frequency') AS date) dates
                            WHERE date >= filecreateddate
                            AND date <= fileexpirydate
                            $excluded
                            GROUP BY date
                        )
                        SELECT dates.stamp::TIMESTAMP WITHOUT TIME ZONE, COALESCE(active_files.rowcount, 0) AS rowcount
                        FROM dates
                        LEFT JOIN active_files ON dates.stamp = active_files.stamp
                        ORDER BY stamp ASC";

            // Execute query and add the resulting table to our return array.
            $statement = $this->db->fquery($query);
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_NUM);
            $stmnt = NULL;

            $this->removeCurrentPeriod($frequency, $result);

            $arr[$frequency] = $result;
        }
        return $arr;
    }

    // ---------------------------------------
    // Gets the data for the "active users" graph from DB.
    // ---------------------------------------

    public function activeUsersStats($frequencies)
    {
        global $config;
        $arr = array();

        $excluded = "";

        foreach ($config['excluded_domains'] as $domain) {
            $excluded .= "AND (filefrom !~ '" . $domain . "') ";
        }

        foreach ($frequencies as $frequency) {
            $query = "WITH dates AS (
                            SELECT date_trunc('$frequency', generate_series((SELECT min(statdate) FROM stats), now(), '1 $frequency')) AS stamp
                        ),
                        active_users AS (
                            SELECT date_trunc('$frequency', date) AS stamp, count(DISTINCT filefrom) AS usercount
                            FROM files, (SELECT generate_series((SELECT min(statdate) FROM stats), now(), '1 $frequency') AS date) dates
                            WHERE date >= filecreateddate
                            AND date <= fileexpirydate
                            $excluded
                            GROUP BY date
                        )
                        SELECT dates.stamp::TIMESTAMP WITHOUT TIME ZONE, COALESCE(active_users.usercount, 0) AS usercount
                        FROM dates
                        LEFT JOIN active_users ON dates.stamp = active_users.stamp
                        ORDER BY stamp ASC";

            // Execute query and add the resulting table to our return array.
            $statement = $this->db->fquery($query);
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_NUM);
            $stmnt = NULL;

            $this->removeCurrentPeriod($frequency, $result);

            $arr[$frequency] = $result;
        }
        return $arr;
    }

    // ---------------------------------------
    // Support for the various stats functions. Returns the correct start and end dates for each of the possible frequencies.
    // ---------------------------------------

    public function getGraphDates($frequencies)
    {
        global $config;
        $dates = array();

        foreach ($frequencies as $frequency) {
            // Work out from config how far back in time we need to go for the currently selected frequency.
            $offset = $config['stats_offset_' . $frequency];

            // Get today's date and use it to work out the correct start dates.
            $today = getdate();
            $startYear = $today["year"] - $offset["year"];
            $startMonth = $today["mon"] - $offset["mon"];
            $startDay = $today["mday"] - $offset["mday"];

            // Wrap start day around to previous month if negative.
            if ($startDay <= 0) {
                $startMonth -= 1;
                $daysInMonth = cal_days_in_month(CAL_GREGORIAN, $startMonth, $startYear);
                $startDay = $daysInMonth + $startDay;
            }

            // Wrap start month around to previous year if negative.
            if ($startMonth <= 0) {
                $startYear -= 1;
                $startMonth += 12;
            }

            if ($frequency == "year" || $frequency == "month") {
                $startDay = 1;
            }

            if ($frequency == "year") {
                $startMonth = 1;
            }


            // Finally, set the dates to be used in queries.
            $start = $startYear . "-" . $startMonth . "-" . $startDay;
            $end = $today["year"] . "-" . $today["mon"] . "-" . $today["mday"];

            $dates[$frequency] = array($start, $end);
        }
        return $dates;
    }

    // ---------------------------------------
    // Support function for the file size stats, gets the first date in a series.
    // ---------------------------------------

    public function getVouchers()
    {

       global $config;

        if( $this->authsaml->isAuth()) {
            $authAttributes = $this->authsaml->sAuth();
        } else {
            $authAttributes["saml_uid_attribute"] = "";
        }
		
		$pdo = $this->db->connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Set Errorhandling to Exception
		$statement = $pdo->prepare("SELECT ".$this->returnFields." FROM files WHERE (fileauthuseruid = :fileauthuseruid) AND filestatus = 'Voucher'  ORDER BY fileactivitydate DESC");
		$statement->bindParam(':fileauthuseruid', $authAttributes["saml_uid_attribute"]);
		try 
		{ 	
			$statement->execute(); 
		}
		catch(PDOException $e)
		{ 
			logEntry($e->getMessage(),"E_ERROR");	
			displayError(lang("_ERROR_CONTACT_ADMIN"),$e->getMessage());
		}   
		$result = $statement->fetchAll();
		$pdo = NULL;
		$returnArray = array();
        foreach($result as $row)
        {
            array_push($returnArray, $row);
        }
        return json_encode($returnArray);
    }

    //--------------------------------------- CHECKED
    // Get Files for a specified user based on saml_uid_attribute
	// ---------------------------------------
    public function getUserFiles() {

        global $config;

        if( $this->authsaml->isAuth()) {
            $authAttributes = $this->authsaml->sAuth();
        } else {
            $authAttributes["saml_uid_attribute"] = "nonvalue";
        }
		$pdo = $this->db->connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Set Errorhandling to Exception
		$statement = $pdo->prepare("SELECT ".$this->returnFields." FROM files WHERE (fileauthuseruid = :fileauthuseruid) AND filestatus = 'Available'  ORDER BY filecreateddate DESC");
		$statement->bindParam(':fileauthuseruid', $authAttributes["saml_uid_attribute"]);
		try 
		{ 	
			$statement->execute(); 
		}
		catch(PDOException $e)
		{ 
			logEntry($e->getMessage(),"E_ERROR");	
			displayError(lang("_ERROR_CONTACT_ADMIN"),$e->getMessage()); 
		}   
		$result = $statement->fetchAll();
		$pdo = NULL;
		$returnArray = array();
		foreach($result as $row )
		{
			array_push($returnArray, $row);
		}
		return json_encode($returnArray);
    }

    //--------------------------------------- CHECKED
    // Return logs if users is admin
    // current email authenticated as per config["admin"]
	// ---------------------------------------
    public function adminLogs($type) {

        // check if this user has admin access before returning data
		global $page;
		global $total_pages;
		$pagination = "";
		$maxitems_perpage = 20;
		$page = 1;
		
		$statement = $this->db->fquery("SELECT count(logtype)  FROM logs WHERE logtype = '$type'");
		$statement->execute();
		$total = $statement->fetch(PDO::FETCH_NUM);
		$total = $total[0];

		$total_pages[$type] = ceil($total/$maxitems_perpage);
		
		if(isset($_REQUEST["page"]) && is_numeric($_REQUEST["page"])) // protect SQLinjection by confirming  $_REQUEST["page"] is an integer only
		{
       	$page = intval($_REQUEST["page"]); 
  		if (0 == $page){
  		$page = 1;
  		}  
  		$start = $maxitems_perpage * ($page - 1);
  		$max = $maxitems_perpage;
		$pagination = "LIMIT ".$maxitems_perpage." OFFSET ".$start;
		} else {
		$pagination = "LIMIT ".$maxitems_perpage." OFFSET 0";
		}
        if($this->authsaml->authIsAdmin()) { 
		$pdo = $this->db->connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Set Errorhandling to Exception
		$statement = $pdo->prepare('SELECT logtype, logfrom , logto, logdate, logfilesize, logfilename, logmessage FROM logs WHERE logtype = :logtype ORDER BY logdate DESC '.$pagination);
		$statement->bindParam(':logtype', $type);
		try 
		{ 	
			$statement->execute(); 
		}
		catch(PDOException $e)
		{ 
			logEntry($e->getMessage(),"E_ERROR");	
			displayError(lang("_ERROR_CONTACT_ADMIN"),$e->getMessage()); 
		}   
		$result = $statement->fetchAll();
		$pdo = NULL;
      	$returnArray = array();
	        foreach($result as $row) 
            {
                array_push($returnArray, $row);
            }
            return $returnArray;
        }
    }

    //---------------------------------------CHECKED
    // Return Files if users is admin
    // current email authenticated as per config["admin"]
	// ---------------------------------------
    public function adminFiles($type) {

		global $page;
		global $total_pages;
		$pagination = "";
		$maxitems_perpage = 10;
		$page = 1;
		
		$statement = $this->db->fquery("SELECT count(fileid) FROM files WHERE filestatus = '$type'");
		$statement->execute();
		$total = $statement->fetch(PDO::FETCH_NUM);
		$total = $total[0];
		
		$total_pages[$type] = ceil($total/$maxitems_perpage);
		
		if(isset($_REQUEST["page"]) && is_numeric($_REQUEST["page"])) // protect SQLinjection by confirming  $_REQUEST["page"] is an integer only
		{
			$page = intval($_REQUEST["page"]); 
  			if (0 == $page){
  			$page = 1;
  			}  
  			$start = $maxitems_perpage * ($page - 1);
  			$max = $maxitems_perpage;
			$pagination = "LIMIT ".$maxitems_perpage." OFFSET ".$start;
		} else {
			$pagination = "LIMIT ".$maxitems_perpage." OFFSET 0";
		}
		
		// check if this user has admin access before returning data
		if($this->authsaml->authIsAdmin()) { 
		$pdo = $this->db->connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Set Errorhandling to Exception
		$statement = $pdo->prepare('SELECT '.$this->returnFields.' FROM files WHERE filestatus = :filestatus ORDER BY fileactivitydate DESC '. $pagination);
		$statement->bindParam(':filestatus', $type);
		try 
		{ 	
			$statement->execute(); 
		}
		catch(PDOException $e)
		{ 
			logEntry($e->getMessage(),"E_ERROR");	
			displayError(lang("_ERROR_CONTACT_ADMIN"),$e->getMessage());
		}   
		$result = $statement->fetchAll();
		$pdo = NULL;
		$returnArray = array();
		foreach($result as $row)
		{
			array_push($returnArray, $row);
		}
		return $returnArray;
		}
	}

	// check if this upload already has a data entry
	public function checkPending($dataitem) {
		
		$pdo = $this->db->connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Set Errorhandling to Exception
		$statement = $pdo->prepare("SELECT * FROM files where fileoriginalname = :fileoriginalname AND filesize = :filesize AND fileuid = :fileuid AND filestatus = 'Pending'");
		$statement->bindParam(':fileoriginalname', $dataitem["fileoriginalname"]);
		$statement->bindParam(':filesize', $dataitem["filesize"]);
		$statement->bindParam(':fileuid', $dataitem["fileuid"]);
		try 
		{ 	
			$statement->execute(); 
		}
		catch(PDOException $e)
		{ 
			logEntry($e->getMessage(),"E_ERROR");	
			displayError(lang("_ERROR_CONTACT_ADMIN"),$e->getMessage());
		}   
		$result = $statement->fetchAll();
		if($result)
		{
			return $result[0];
		} else {
			return "";
		}
		$pdo = NULL;
		
	}	
	
    //--------------------------------------- CHECKED
    // Return file information based on filervoucheruid
	// ---------------------------------------
    // 
    public function getFile($dataitem) {

		$vid = $dataitem['filevoucheruid'];
 
		$pdo = $this->db->connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Set Errorhandling to Exception
		$statement = $pdo->prepare('SELECT * FROM files where filevoucheruid = :filevoucheruid');
		$statement->bindParam(':filevoucheruid', $vid);
		try 
		{ 	
			$statement->execute(); 
		}
		catch(PDOException $e)
		{ 
			logEntry($e->getMessage(),"E_ERROR");	
			displayError(lang("_ERROR_CONTACT_ADMIN"),$e->getMessage()); 
		}   
		$result = $statement->fetchAll();
		$pdo = NULL;
		$returnArray = array();
		foreach($result as $row)
		{
			array_push($returnArray, $row);
		}
		return json_encode($returnArray);
	}

    //--------------------------------------- CHECKED NOTE
	// Note: Function Name Duplicated in AuthVoucher.php but using $_Request["vid"]
	// Note: Remove AuthVoucher.php getVocuher function and replace with similar function in Functions class
    // Return voucher information based on fileid
	// ---------------------------------------
    // 
    public function getVoucher($vid) {

       	$pdo = $this->db->connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Set Errorhandling to Exception
		$statement = $pdo->prepare('SELECT * FROM files where fileid = :fileid');
		$statement->bindParam(':fileid', $vid);
		try 
		{ 	
			$statement->execute(); 
		}
		catch(PDOException $e)
		{ 
			logEntry($e->getMessage(),"E_ERROR");	
			displayError(lang("_ERROR_CONTACT_ADMIN"),$e->getMessage());
		}   
		$result = $statement->fetchAll();
		$pdo = NULL;
  		$returnArray = array();
        foreach($result as $row)
		{
            array_push($returnArray, $row);
        }
		return $returnArray;
		}
	
	//--------------------------------------- CHECKED
	// Return voucher information based on filervoucheruid
	// ---------------------------------------
	// 
	public function getVoucherData($vid) {

		$pdo = $this->db->connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Set Errorhandling to Exception
		$statement = $pdo->prepare('SELECT * FROM files where filevoucheruid = :filevoucheruid');
		$statement->bindParam(':filevoucheruid', $vid);
		try { 	
			$statement->execute(); 
			}
		catch(PDOException $e)
		{ 
			logEntry($e->getMessage(),"E_ERROR");	
			displayError(lang("_ERROR_CONTACT_ADMIN"),$e->getMessage()); 
		}   
		$result = $statement->fetchAll();
		$pdo = NULL;
  		$returnArray = array();
        foreach($result as $row)
		{
            array_push($returnArray, $row);
        }
        return $returnArray[0];
    }
	
	//--------------------------------------- CHECKED
	// insert a voucher
	// ---------------------------------------

	public function insertVoucher($to,$expiry){
	
		// must be authenticated
		if( $this->authsaml->isAuth()) {
			
        global $config;
        $dbCheck = DB_Input_Checks::getInstance();
		$authAttributes = $this->authsaml->sAuth();
		
		$pdo = $this->db->connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Set Errorhandling to Exception
		
		$statement = $pdo->prepare('INSERT INTO files (
			fileexpirydate,
			fileto,
			filesubject,
			fileactivitydate,
			filevoucheruid,
			filemessage,
			filefrom,
			filesize,
			fileoriginalname,
			filestatus,
			fileip4address,
			fileip6address,
			filesendersname,
			filereceiversname,
			filevouchertype,
			fileuid,
			fileauthuseruid,
			fileauthuseremail,
			filecreateddate

            ) VALUES
            ( 
			:fileexpirydate,
			:fileto,
			:filesubject,
			:fileactivitydate,
			:filevoucheruid,
			:filemessage,
			:filefrom,
			:filesize,
			:fileoriginalname,
			:filestatus,
			:fileip4address,
			:fileip6address,
			:filesendersname,
			:filereceiversname,
			:filevouchertype,
			:fileuid,
			:fileauthuseruid,
			:fileauthuseremail,
			:filecreateddate)');
			
			$filevoucheruid = getGUID();
			$voucher = 'Voucher';
			$blank = '';
			$zero = 0;
			$fileexpiryParam = date($config['db_dateformat'], strtotime($expiry));
			$statement->bindParam(':fileexpirydate',$fileexpiryParam);
			$statement->bindParam(':fileto', $to);
			$statement->bindParam(':filesubject', $voucher);
			$fileactivitydateParam =  date($config['db_dateformat'], time());
			$statement->bindParam(':fileactivitydate',$fileactivitydateParam );	
			$statement->bindParam(':filevoucheruid', $filevoucheruid );
			$statement->bindParam(':filemessage', $blank);
			$statement->bindParam(':filefrom', $authAttributes["email"]);
			$statement->bindParam(':filesize', $zero);
			$statement->bindParam(':fileoriginalname', $blank);
			$statement->bindParam(':filestatus', $voucher);
			$fileip4addressParam = $dbCheck->checkIp($_SERVER['REMOTE_ADDR']);
			$statement->bindParam(':fileip4address',$fileip4addressParam );
			$fileip6addressParam = $dbCheck->checkIp6($_SERVER['REMOTE_ADDR']);
			$statement->bindParam(':fileip6address', $fileip6addressParam);
			$statement->bindParam(':filesendersname', $blank);
			$statement->bindParam(':filereceiversname', $blank);
			$statement->bindParam(':filevouchertype', $blank);
			$fileuidParam = getGUID();
			$statement->bindParam(':fileuid', $fileuidParam);
			$statement->bindParam(':fileauthuseruid', $authAttributes["saml_uid_attribute"]);
			$statement->bindParam(':fileauthuseremail', $authAttributes["email"]);
			$filecreateddateParam =  date($config['db_dateformat'], time());
			$statement->bindParam(':filecreateddate',$filecreateddateParam);
			try { 	
			$statement->execute(); 
			}
			catch(PDOException $e){ 
			logEntry($e->getMessage(),"E_ERROR");	
			displayError(lang("_ERROR_CONTACT_ADMIN"),$e->getMessage()); 
			}   
			$pdo = NULL;
			// get voucherdata to email
			$dataitem = $this->getVoucherData($filevoucheruid);
			$this->saveLog->saveLog($dataitem,"Voucher Sent","");
			return $this->sendmail->sendEmail($dataitem,$config['voucherissuedemailbody']);
			
		} else {
			
			return false;
		}
    }
	
	// --------------------------------------- CHECKED
	// ensure valid fileexpirydate
	// --------------------------------------- 
	public function ensureValidFileExpiryDate($data)
	{
		global $config;
		// check fileexpirydate exists and is valid
		if((strtotime($data) >= strtotime("+".$config['default_daysvalid']." day") ||  strtotime($data) <= strtotime("now")))
		{
			// reset fileexpiry date to max config date from server
			$data = date($config['db_dateformat'],strtotime("+".($config['default_daysvalid'])." day"));
		} 
		return date($config['db_dateformat'],strtotime($data));
	}
		
	// ---------------------------------------CHECKED
    // Validate $data and return data
	// ---------------------------------------
	public function validateFileData($data)
	{
		// client must provide following minimum data
		// fileto // filesize // filefrom // fileexpirydata // file voucher or authenticated uuid // filename
		// ensure they exists and are valid
		// return array of errors or 
		global $config;
		global $resultArray;
		
		$dbCheck = DB_Input_Checks::getInstance();
		$authsaml = AuthSaml::getInstance();
		$functions = Functions::getInstance();
		
		$errorArray = array();
		// test 
		//array_push($errorArray, "err_nodiskspace");
		//array_push($errorArray, "err_tomissing");
		// filesize missing
		if(!isset($data["filesize"])){ array_push($errorArray, "err_missingfilesize"); }
		// check space is available on disk before uploading
		if(isset($data["filesize"]) && disk_free_space($config['site_filestore']) - $data["filesize"] < 1) { array_push($errorArray, "err_nodiskspace");} 
		// expiry missing
		if(!isset($data["fileexpirydate"])){ array_push($errorArray,  "err_expmissing"); }
		// fileto missing
		if(!isset($data["fileto"])){ array_push($errorArray, "err_tomissing");}
		// filename missing
		if(!isset($data["fileoriginalname"])){ array_push($errorArray, "err_invalidfilename");}
		// filename has invalid extension - $config['ban_extension'] as array
		$ban_extension = explode(',', $config['ban_extension']);
		foreach ($ban_extension as $extension) {
			if(isset($data["fileoriginalname"]) && $extension == pathinfo($data["fileoriginalname"], PATHINFO_EXTENSION) ){ array_push($errorArray, "err_invalidextension");}
		}
		// filename blank
		if(isset($data["fileoriginalname"]) && $data["fileoriginalname"] === ""){ array_push($errorArray, "err_invalidfilename");}
		// filename contains invalid characters
		if(isset($data["fileoriginalname"]) && preg_match('=^[^\\\\/:;\*\?\"<>|]+(\.[^\\\\/:;\*\?\"<>|]+)*$=',$data["fileoriginalname"]) === 0){ array_push($errorArray, "err_invalidfilename");}
		
		// expiry out of range
		if(strtotime($data["fileexpirydate"]) > strtotime("+".$config['default_daysvalid']." day") ||  strtotime($data["fileexpirydate"]) < strtotime("now"))
		{
			// Don't generate error, expiry date will be fixed later on with:
			// $data["fileexpirydate"] = $this->ensureValidFileExpiryDate($data["fileexpirydate"]);
			// When generating an error use/uncomment the following code:
			// array_push($errorArray,"err_exoutofrange");
		}
		// Recipient email missing
		if(!isset($data["fileto"])){ array_push($errorArray,  "err_filetomissing"); 
		} else {
		$emailto = str_replace(",",";",$data["fileto"]);
		$emailArray = preg_split("/;/", $emailto);
		// validate number of emails
		if(count($emailArray) > $config['max_email_recipients'] ) {array_push($errorArray,  "err_toomanyemail");}
		// validate individual emails
		foreach ($emailArray as $Email) {
			if(!filter_var($Email,FILTER_VALIDATE_EMAIL)) {array_push($errorArray, "err_invalidemail");}
		}
		}
		// Sender email missing or not authuser or voucher sender
		if (!isset($data["filefrom"])){
			array_push($errorArray,  "err_filefrommissing");
		} else {
			// Check if sender address is valid
			if(!filter_var($data["filefrom"],FILTER_VALIDATE_EMAIL)) {array_push($errorArray, "err_invalidemail");}
			// check if filefrom matches voucher from or matches authenticated user
		if(isset($_SESSION['voucher']))
		{
			$tempData = $functions->getVoucherData($_SESSION['voucher']);
			//array_push($errorArray,  $data["filefrom"] .":". $tempData["filefrom"]);
			if($data["filefrom"] != $tempData["fileto"] ) {array_push($errorArray, "err_invalidemail");}
		}	else if( $authsaml->isAuth()) 
		{
			$authAttributes = $authsaml->sAuth();
			//array_push($errorArray,  $data["filefrom"] .":". $authAttributes["email"]);	
			if($data["filefrom"] != $authAttributes["email"]) {array_push($errorArray, "err_invalidemail");}
		}
			
		}
		// if errors - return them via json to client	
		if(count($errorArray) > 0 )
		{
		$resultArray["errors"] =  $errorArray;
		echo json_encode($resultArray);
		exit; // Stop further script execution
		}
			
		// no errors >> continue
		// ensure valid fields before commiting to database
		$data["fileexpirydate"] = $this->ensureValidFileExpiryDate($data["fileexpirydate"]);
		$data["filesubject"] = (isset($data["filesubject"])) ? $data["filesubject"] : "";
		$data["fileactivitydate"]= date($config['db_dateformat'], time());
		$data["filevoucheruid"] = (isset($data["filevoucheruid"])) ? $data["filevoucheruid"] : getGUID();
		$data["filemessage"] = (isset($data["filemessage"])) ? $data["filemessage"] : "";
        $data["filefrom"]=$data["filefrom"];
        $data["filesize"]=$data["filesize"];
        $data["fileoriginalname"]=  sanitizeFilename($data['fileoriginalname']);
        $data["filestatus"]="Pending";//isset($data['filestatus']) ? $data['filestatus'] : "Pending";
        $data["fileip4address"]= $dbCheck->checkIp($_SERVER['REMOTE_ADDR']);
        $data["fileip6address"]= $dbCheck->checkIp6($_SERVER['REMOTE_ADDR']);
		$data["filesendersname"]=isset($data['filesendersname']) ? $data['filesendersname'] : NULL;
		$data["filereceiversname"]=isset($data['filereceiversname']) ? $data['filereceiversname'] : NULL;
		$data["filevouchertype"]=isset($data['filevouchertype']) ? $data['filevouchertype'] : NULL;
        if($data["fileuid"] == "" ) {$data["fileuid"] = getGUID();};
        //$data["fileauthuseruid"]="null";
        //$data["fileauthuseremail"]="null";
        $data["filecreateddate"]= date($config['db_dateformat'], time()); 
		
		return $data;
	}
	
	// --------------------------------------- CHECKED
	// Insert new file  
	// ---------------------------------------
	public function insertFile($dataitem){

        global $config;

		// prepare PDO insert statement
		$pdo = $this->db->connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Set Errorhandling to Exception
		$statement = $pdo->prepare('INSERT INTO files (
			fileexpirydate,
			fileto,
			filesubject,
			fileactivitydate,
			filevoucheruid,
			filemessage,
			filefrom,
			filesize,
			fileoriginalname,
			filestatus,
			fileip4address,
			fileip6address,
			filesendersname,
			filereceiversname,
			filevouchertype,
			fileuid,
			fileauthuseruid,
			fileauthuseremail,
			filecreateddate
            ) VALUES
            ( 	:fileexpirydate,
			:fileto,
			:filesubject,
			:fileactivitydate,
			:filevoucheruid,
			:filemessage,
			:filefrom,
			:filesize,
			:fileoriginalname,
			:filestatus,
			:fileip4address,
			:fileip6address,
			:filesendersname,
			:filereceiversname,
			:filevouchertype,
			:fileuid,
			:fileauthuseruid,
			:fileauthuseremail,
			:filecreateddate)');	
				
			$statement->bindParam(':fileexpirydate', $dataitem['fileexpirydate']);
			$statement->bindParam(':fileto', $dataitem['fileto']);
			$statement->bindParam(':filesubject', $dataitem['filesubject']);
			$statement->bindParam(':fileactivitydate', $dataitem['fileactivitydate']);
			$statement->bindParam(':filevoucheruid', $dataitem['filevoucheruid']);
			$statement->bindParam(':filemessage', $dataitem['filemessage']);
			$statement->bindParam(':filefrom', $dataitem['filefrom']);
			$statement->bindParam(':filesize', $dataitem['filesize']);
			$statement->bindParam(':fileoriginalname', $dataitem['fileoriginalname']);
			$statement->bindParam(':filestatus', $dataitem['filestatus']);
			$statement->bindParam(':fileip4address', $dataitem['fileip4address']);
			$statement->bindParam(':fileip6address', $dataitem['fileip6address']);
			$statement->bindParam(':filesendersname', $dataitem['filesendersname']);
			$statement->bindParam(':filereceiversname', $dataitem['filereceiversname']);
			$statement->bindParam(':filevouchertype', $dataitem['filevouchertype']);
			$statement->bindParam(':fileuid', $dataitem['fileuid']);
			$statement->bindParam(':fileauthuseruid', $dataitem['fileauthuseruid']);
			$statement->bindParam(':fileauthuseremail', $dataitem['fileauthuseremail']);
			$statement->bindParam(':filecreateddate', $dataitem['filecreateddate']);
	
			try { 
				$statement->execute(); 
				}
			catch(PDOException $e){ 
				logEntry($e->getMessage(),"E_ERROR");	
				displayError(lang("_ERROR_CONTACT_ADMIN"),$e->getMessage());
				return false;
				}   

			if($dataitem['filestatus'] == "Voucher") {
				$this->saveLog->saveLog($dataitem,"Voucher Sent","");
				return $this->sendmail->sendEmail($dataitem,$config['voucherissuedemailbody']);
			} elseif ($dataitem['filestatus'] == "Available") {
				$this->saveLog->saveLog($dataitem,"Uploaded","");
				return $this->sendmail->sendEmail($dataitem,$config['fileuploadedemailbody']);
			}
			return true;
		}
	
	// --------------------------------------- CHECKED
	// Update file 
	// ---------------------------------------
	public function updateFile($dataitem){

        global $config;

		// prepare PDO insert statement
		$pdo = $this->db->connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Set Errorhandling to Exception
		$statement = $pdo->prepare('UPDATE files SET
			fileexpirydate = :fileexpirydate,
			fileto = :fileto,
			filesubject = :filesubject,
			fileactivitydate = :fileactivitydate,
			filemessage = :filemessage,
			filefrom = :filefrom,
			filesize = :filesize,
			fileoriginalname = :fileoriginalname,
			filestatus = :filestatus,
			fileip4address = :fileip4address,
			fileip6address = :fileip6address,
			filesendersname = :filesendersname,
			filereceiversname = :filereceiversname,
			filevouchertype = :filevouchertype,
			fileuid = :fileuid,
			fileauthuseruid = :fileauthuseruid,
			fileauthuseremail = :fileauthuseremail,
			filecreateddate = :filecreateddate
			WHERE filevoucheruid = :filevoucheruid');	
				
			$statement->bindParam(':fileexpirydate', $dataitem['fileexpirydate']);
			$statement->bindParam(':fileto', $dataitem['fileto']);
			$statement->bindParam(':filesubject', $dataitem['filesubject']);
			$statement->bindParam(':fileactivitydate', $dataitem['fileactivitydate']);
			$statement->bindParam(':filevoucheruid', $dataitem['filevoucheruid']);
			$statement->bindParam(':filemessage', $dataitem['filemessage']);
			$statement->bindParam(':filefrom', $dataitem['filefrom']);
			$statement->bindParam(':filesize', $dataitem['filesize']);
			$statement->bindParam(':fileoriginalname', $dataitem['fileoriginalname']);
			$statement->bindParam(':filestatus', $dataitem['filestatus']);
			$statement->bindParam(':fileip4address', $dataitem['fileip4address']);
			$statement->bindParam(':fileip6address', $dataitem['fileip6address']);
			$statement->bindParam(':filesendersname', $dataitem['filesendersname']);
			$statement->bindParam(':filereceiversname', $dataitem['filereceiversname']);
			$statement->bindParam(':filevouchertype', $dataitem['filevouchertype']);
			$statement->bindParam(':fileuid', $dataitem['fileuid']);
			$statement->bindParam(':fileauthuseruid', $dataitem['fileauthuseruid']);
			$statement->bindParam(':fileauthuseremail', $dataitem['fileauthuseremail']);
			$statement->bindParam(':filecreateddate', $dataitem['filecreateddate']);
	
			try { 
				$statement->execute(); 
				}
			catch(PDOException $e){ 
				logEntry($e->getMessage(),"E_ERROR");	
				displayError(lang("_ERROR_CONTACT_ADMIN"),$e->getMessage()); 
				return false;
				}   
			return true;
		}
    // --------------------------------------- CHECKED
    // Delete a voucher
    // ---------------------------------------
    public function deleteVoucher($fileid){

        global $config;

		if( $this->authsaml->isAuth()) { // check authentication SAML User
			
			$pdo = $this->db->connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Set Errorhandling to Exception
			$statement = $pdo->prepare("UPDATE files SET filestatus = 'Voucher Cancelled' WHERE fileid = :fileid");
			$statement->bindParam(':fileid', $fileid);
			
			try { $statement->execute();}
			catch(PDOException $e){ logEntry($e->getMessage(),"E_ERROR");	return false; }   
				
			$fileArray =  $this->getVoucher($fileid);
	
			if(count($fileArray) > 0) 
			{
				$this->sendmail->sendEmail($fileArray[0],$config['defaultvouchercancelled']);	
				$this->saveLog->saveLog($fileArray[0],"Voucher Cancelled","");
				return true;
			}
			return false;
		} else {
			return false;
		}	
	}
	
    // --------------------------------------- CHECKED
    // Close a voucher
    // ---------------------------------------
    public function closeVoucher($fileid){

        global $config;

		if( $this->authsaml->isAuth() || $this->authvoucher->aVoucher()) { // check authentication SAML User
			
			$pdo = $this->db->connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Set Errorhandling to Exception
			$statement = $pdo->prepare("UPDATE files SET filestatus = 'Closed' WHERE fileid = :fileid");
			$statement->bindParam(':fileid', $fileid);
			
			try { $statement->execute();}
			catch(PDOException $e){ logEntry($e->getMessage(),"E_ERROR");	return false; }   
				
			$fileArray =  $this->getVoucher($fileid);
	
			if(count($fileArray) > 0) 
			{
				$this->saveLog->saveLog($fileArray[0],"Voucher Cancelled","");
				return true;
			}
			return false;
		} else {
			return false;
		}	
    }
	
	 // --------------------------------------- CHECKED
    // Close a voucher
    // ---------------------------------------
    public function closeCompleteVoucher($filevoucheruid){

        global $config;

		if( $this->authsaml->isAuth() || $this->authvoucher->aVoucher()) { // check authentication SAML User
			
			$pdo = $this->db->connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Set Errorhandling to Exception
			$statement = $pdo->prepare("UPDATE files SET filestatus = 'Closed' WHERE filevoucheruid = :filevoucheruid");
			$statement->bindParam(':filevoucheruid', $filevoucheruid);
			
			try { $statement->execute();}
			catch(PDOException $e){ logEntry($e->getMessage(),"E_ERROR");	return false; }   
			
			logEntry("Voucher Closed: ".$filevoucheruid);	
			
			return true;
		
		} else {
			return false;
		}	
    }
	
    // --------------------------------------- CHECKED
    // Delete a file
    // ---------------------------------------
    public function deleteFile($fileid){

            global $config;

		if( $this->authsaml->isAuth()) { // check authentication SAML User
			
			$pdo = $this->db->connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Set Errorhandling to Exception
			$statement = $pdo->prepare("UPDATE files SET filestatus = 'Deleted' WHERE fileid = :fileid");
			$statement->bindParam(':fileid', $fileid);
			
			try { $statement->execute();}
			catch(PDOException $e){ logEntry($e->getMessage(),"E_ERROR");	return false; }   
				
			$fileArray =  $this->getVoucher($fileid);
	
			if(count($fileArray) > 0) 
			{
				$this->sendmail->sendEmail($fileArray[0],$config['defaultfilecancelled']);	
				$this->saveLog->saveLog($fileArray[0],"File Cancelled","");
				return true;
			}
			return false;
		} else {
			return false;
		}	
    }

    //--------------------------------------- CHECKED
    // Return filesize as integer from php
    // Function also handles windows servers
    // ---------------------------------------
    // 
    public function getFileSize($filename){

        global $config;

        if($filename == "" ) {
            return;
        } else {
            $file = $filename;//$config["site_filestore"].sanitizeFilename($filename);
            //We should turn this into a switch/case, exhaustive with a default case
            if (file_exists($file)) {
				if (PHP_OS == "Darwin") {
	                $size = trim(shell_exec("stat -f %z ". escapeshellarg($file)));
				}
                else if (!(strtoupper(substr(PHP_OS, 0, 3)) == 'WIN')) 
                {
                    $size = trim(shell_exec("stat -c%s ". escapeshellarg($file)));
                } 
				else { 
                    $fsobj = new COM("Scripting.FileSystemObject"); 
                    $f = $fsobj->GetFile($file); 
                    $size = $f->Size; 
                }
                return $size;
            } else { 
                return 0;
            } 
        }
    }

    //--------------------------------------- CHECKED
    // Get drive space
    // Returns JSON array
	// ---------------------------------------
    public function driveSpace() {

        global $config;

        $result["site_filestore_total"] = disk_total_space($config['site_filestore']);   			// use absolute locations result in bytes
        $result["site_temp_filestore_total"] = disk_total_space($config['site_temp_filestore']);   			// use absolute locations
        $result["site_filestore_free"] = disk_free_space($config['site_filestore']);   			// use absolute locations
        $result["site_temp_filestore_free"] = disk_free_space($config['site_temp_filestore']);   			// use absolute locations

        return $result;

    }
}
?>
