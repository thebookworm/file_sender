CREATE TABLE stats
(
  statid integer NOT NULL DEFAULT nextval('log_id_seq'::regclass),
  statfileuid character varying(60),
  statlogtype character varying(60),
  statfromdomain character varying(250),
  statfromhashed character varying(64),
  stattodomain text,
  statdate timestamp without time zone,
  statfilesize bigint,
  statsessionid character varying(60),
  statvoucheruid character varying(60),
  statauthuseruid character varying(500),
  CONSTRAINT stats_pkey PRIMARY KEY (statid)
);
