<?php

/*
 * FileSender www.filesender.org
 * 
 * Copyright (c) 2009-2012, AARNet, Belnet, HEAnet, SURFnet, UNINETT
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * *	Redistributions of source code must retain the above copyright
 * 	notice, this list of conditions and the following disclaimer.
 * *	Redistributions in binary form must reproduce the above copyright
 * 	notice, this list of conditions and the following disclaimer in the
 * 	documentation and/or other materials provided with the distribution.
 * *	Neither the name of AARNet, Belnet, HEAnet, SURFnet and UNINETT nor the
 * 	names of its contributors may be used to endorse or promote products
 * 	derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* ---------------------------------
 * en_AU Language File
 * Maintained by the FileSender Core Team
 * ---------------------------------
 * 
 */
// main menu items
$lang["_ADMIN"] = "Administration";
$lang["_NEW_UPLOAD"] = "Send File";
$lang["_VOUCHERS"] = "Guest Voucher";
$lang["_LOGON"] = "Logon";
$lang["_LOG_OFF"] = "Log Off";
$lang["_MY_FILES"] = "My Files";
$lang["_STATS"] = "Statistics";

// page titles
$lang["_HOME"] = "Home";
$lang["_ABOUT"] = "About";
$lang["_HELP"] = "Help";
$lang["_DELETE_VOUCHER"] = "Delete Voucher";
$lang["_UPLOAD_COMPLETE"] = "Your file has been uploaded and message sent.";
$lang["_UPLOAD_PROGRESS"] = "Upload progress";
$lang["_DOWNLOAD"] = "Download";
$lang["_CANCEL_UPLOAD"] = "Cancel Upload";

// admin
$lang["_PAGE"] = "Page";
$lang["_UP"] = "Up";
$lang["_DOWN"] = "Down";
$lang["_FILES"] = "Files";
$lang["_DRIVE"] = "Drive";
$lang["_TOTAL"] = "Total";
$lang["_USED"] = "Used";
$lang["_AVAILABLE"] = "Available";
$lang["_TEMP"] = "Temp"; // as in Temporary files

// Greetings
$lang["_WELCOME"] = "Welcome"; 
$lang["_WELCOMEGUEST"] = "Welcome Guest"; 

// admin tab names
$lang["_GENERAL"] = "General";
$lang["_UPLOADS"] = "Uploads";
$lang["_DOWNLOADS"] = "Downloads";
$lang["_ERRORS"] = "Errors";
$lang["_FILES_AVAILABLE"] = "Files Available";
$lang["_ACTIVE_VOUCHERS"] = "Active Vouchers";
$lang["_COMPLETE_LOG"] = "Complete Log";

// Form Fields
$lang["_TO"] = "To";
$lang["_FROM"] = "From";
$lang["_SIZE"] = "Size";
$lang["_CREATED"] = "Created";
$lang["_FILE_NAME"] = "File Name";
$lang["_SUBJECT"] = "Subject";
$lang["_EXPIRY"] = "Expiry";
$lang["_MESSAGE"] = "Message";
$lang["_TYPE"] = "Type";

$lang["_TERMS_OF_AGREEMENT"] = "Terms of Agreement";
$lang["_SHOW_TERMS"] = "Show Terms";
$lang["_SHOWHIDE"] = "Show/Hide";
$lang["_UPLOADING_WAIT"] = "Uploading file - please wait...";

// button labels
$lang["_UPLOAD"] = "Send a file";
$lang["_BROWSE"] = "Browse";
$lang["_CANCEL"] = "Cancel";
$lang["_OPEN"] = "Open";
$lang["_CLOSE"] = "Close";
$lang["_OK"] = "OK";
$lang["_SEND"] = "Send";
$lang["_DELETE"] = "Delete";
$lang["_YES"] = "Yes";
$lang["_NO"] = "No";

$lang["_ERROR_CONTACT_ADMIN"] = "There has been an error - please contact your administrator.";	
$lang["_INVALID_MISSING_EMAIL"] = "Invalid or missing email";	
$lang["_INVALID_EXPIRY_DATE"] = "Invalid expiry Date";	
$lang["_INVALID_FILE"] = "Invalid File";	
$lang["_INVALID_FILEVOUCHERID"] = "Invalid File or Voucher ID";	
$lang["_INVALID_FILESIZE_ZERO"] = "File size cannot be 0. Please select another file.";
$lang["_INVALID_FILE_EXT"] = "Invalid file extension.";
$lang["_INVALID_TOO_LARGE_1"] = "File size cannot be greater than";
$lang["_AUTH_ERROR"] = "Your are no longer authenticated. <br />Your session may have expired or there has been a server error. <br /><br />Please logon again and re-try.";	
$lang["_SELECT_ANOTHER_FILE"] = "Please select another file.";
$lang["_INVALID_VOUCHER"] = "This Voucher is no longer Valid. <br />Please contact the person that issued this voucher";
$lang["_SELECT_FILE"] = "Select your file";
$lang["_INVALID_FILE_NAME"] = "The name of the file you are uploading is invalid. Please rename your file and try again.";
$lang["_INVALID_SIZE_USEHTML5"] = "Please select another file or use a HTML5 enabled browser to upload larger files.";
$lang["_ACCEPTTOC"] = "I accept the terms and conditions of this service.";	
$lang["_AGREETOC"] = "You MUST agree to the terms and conditions.";
$lang["_FILE_TO_BE_RESENT"] = "File to be redistributed";
$lang["_ERROR_UPLOADING_FILE"] = "Error uploading your file";
$lang["_ERROR_SENDING_EMAIL"] = "There has been an error sending emails, please contact your administrator.";
$lang["_ERROR_INCORRECT_FILE_SIZE"] = "There has been a problem uploading your file. <br />The file size on the server does not match your original file. <br /><br />Please contact your Administrator.";
$lang["_MAXEMAILS"] = "The maximum number of email addresses allowed is ";
$lang["_INVALID_DATE_FORMAT"] = "The date format is invalid.";
$lang["_DISK_SPACE_ERROR"] = "There is not enough drive space on this service. Please contact the service administration or upload a smaller file.";
$lang["_ERROR_ATTRIBUTES"] = "Your Identity Provider is not providing the required attributes. Contact your Administrator";
$lang["_PERMISSION_DENIED"] = "You do not have permission to do this.";
// Logout page
$lang["_LOGOUT_COMPLETE"] = "Logout Complete";

// Statistics module
$lang["_OVERVIEW_LINK"] = "Back to statistics dashboard";
$lang["_MODES"] = "Modes: ";
$lang["_CUSTOM_MODE"] = "Custom";
$lang["_KEYSTATS_MODE"] = "Key stats";
$lang["_EXCLUDED_DOMAINS"] = '<p>These domains have been excluded from the statistics:</p>';
$lang["_ALL_DOMAINS_INCLUDED"] = '<p>All domains are currently included.</p>';
$lang["_STATS_MODULE_DISABLED"] = "The statistics module has been disabled for this FileSender installation.";
$lang["_ACCESS_ERROR"] = "<p>This page is disabled or you do not have access to view it.</p>";
$lang["_NO_DATA_ERROR"] = "no data available";
$lang["_TOO_FEW_DATAPOINTS_ERROR"] = "not enough data points";
$lang["_INVALID_DATE_ERROR_TITLE"] = "Date validation error";
$lang["_INVALID_DATE_ERROR"] = "<p>An error has occurred. Please check that the selected dates are valid.</p>";
$lang["_CHART_TYPE"] = "Chart type:";
$lang["_FREQUENCY"] = "Interval frequency:";
$lang["_FREQUENCY_YEAR"] = "Yearly";
$lang["_FREQUENCY_YEAR_SHORT"] = "(yearly)";
$lang["_FREQUENCY_MONTH"] = "Monthly";
$lang["_FREQUENCY_MONTH_SHORT"] = "(monthly)";
$lang["_FREQUENCY_WEEK"] = "Weekly";
$lang["_FREQUENCY_WEEK_SHORT"] = "(weekly)";
$lang["_FREQUENCY_DAY"] = "Daily";
$lang["_FREQUENCY_DAY_SHORT"] = "(daily)";
$lang["_GRAPH_UPDOWN_TITLE"] = "Uploads and downloads";
$lang["_GRAPH_UPDOWN_XAXIS_LABEL"] = "Date";
$lang["_GRAPH_UPDOWN_YAXIS_LABEL"] = "Number of files";
$lang["_GRAPH_UPDOWN_UPLOADS_LABEL"] = "Uploads";
$lang["_GRAPH_UPDOWN_DOWNLOADS_LABEL"] = "Downloads";
$lang["_GRAPH_FILESIZES_TITLE"] = "File sizes";
$lang["_GRAPH_FILESIZES_XAXIS_LABEL"] = "Date";
$lang["_GRAPH_FILESIZES_YAXIS_LABEL"] = "Uploaded files";
$lang["_GRAPH_FILESIZES_FIRST_LABEL"] = "0 MB - 20 MB";
$lang["_GRAPH_FILESIZES_SECOND_LABEL"] = "20 MB - 200 MB";
$lang["_GRAPH_FILESIZES_THIRD_LABEL"] = "200 MB - 2 GB";
$lang["_GRAPH_FILESIZES_FOURTH_LABEL"] = "2 GB and up";
$lang["_GRAPH_VOUCHERS_TITLE"] = "Invite vouchers";
$lang["_GRAPH_VOUCHERS_XAXIS_LABEL"] = "Date";
$lang["_GRAPH_VOUCHERS_YAXIS_LABEL"] = "Vouchers";
$lang["_GRAPH_VOUCHERS_SENT_LABEL"] = "Vouchers sent";
$lang["_GRAPH_VOUCHERS_USED_LABEL"] = "Vouchers used";
$lang["_GRAPH_TOTALUPDOWN_TITLE"] = "Total uploads and downloads in GB";
$lang["_GRAPH_TOTALUPDOWN_XAXIS_LABEL"] = "Date";
$lang["_GRAPH_TOTALUPDOWN_YAXIS_LABEL"] = "Gigabytes";
$lang["_GRAPH_TOTALUPDOWN_UPLOADS_LABEL"] = "Uploads";
$lang["_GRAPH_TOTALUPDOWN_DOWNLOADS_LABEL"] = "Downloads";
$lang["_GRAPH_ACTIVEFILES_TITLE"] = "Active files";
$lang["_GRAPH_ACTIVEFILES_XAXIS_LABEL"] = "Date";
$lang["_GRAPH_ACTIVEFILES_YAXIS_LABEL"] = "Files";
$lang["_GRAPH_ACTIVEFILES_SERIES_LABEL"] = "Active files";
$lang["_GRAPH_ACTIVEUSERS_TITLE"] = "Active users";
$lang["_GRAPH_ACTIVEUSERS_XAXIS_LABEL"] = "Date";
$lang["_GRAPH_ACTIVEUSERS_YAXIS_LABEL"] = "Users";
$lang["_GRAPH_ACTIVEUSERS_SERIES_LABEL"] = "Active users";
$lang["_GRAPH_TOTALFILES_TITLE"] = "Total uploaded files";
$lang["_GRAPH_TOTALFILES_XAXIS_LABEL"] = "Date";
$lang["_GRAPH_TOTALFILES_YAXIS_LABEL"] = "Files";
$lang["_GRAPH_TOTALFILES_SERIES_LABEL"] = "Total files";
$lang["_GRAPH_TOTALUSERS_TITLE"] = "Total unique uploaders";
$lang["_GRAPH_TOTALUSERS_XAXIS_LABEL"] = "Date";
$lang["_GRAPH_TOTALUSERS_YAXIS_LABEL"] = "Users";
$lang["_GRAPH_TOTALUSERS_SERIES_LABEL"] = "Total users";

$lang["_STATS_INFO_TITLE"] = '<h3>Description</h3>';
$lang["_STATS_INFO_UPDOWN"] = '<p>This line chart displays the number of uploads and downloads performed on this FileSender installation over time. It is not a running total, but displays statistics for each interval (day, week, month, year).<br /><br />
The amount of downloads will usually be higher than the amount of uploads.</p>';
$lang["_STATS_INFO_FILESIZES"] = '<p>This chart shows the distribution of file sizes of all uploaded files over time, as a running total. This is helpful for determining how "most people" use your FileSender installation. File size ranges are configurable in config.php.<br /><br />
Note that the ranges are defined as LOW &lt; x &le; HIGH. This means that files of size 0 are not included.</p>';
$lang["_STATS_INFO_VOUCHERS"] = '<p>This line chart displays guest voucher statistics on a logarithmic scale. "Vouchers sent" is the amount of guest vouchers that have been sent by authorised users. "Vouchers used" is the amount of those vouchers that have actually been used by the recipients.<br /><br/>
The number of sent vouchers will typically be equal to or higher than the number of used vouchers, but this may vary slightly because the vouchers are not necessarily used immediately.</p>';
$lang["_STATS_INFO_TOTALUPDOWN"] = '<p>This line chart shows a running total of uploads and downloads over time, in gigabytes.</p>';
$lang["_STATS_INFO_ACTIVEFILES"] = '<p>This line chart displays the number of active files on the server over time. An "active file" is defined as an uploaded file that has not yet been closed or deleted, i.e. it is available for download. Please note, the domain exclusion listed below is currently NOT active for this chart and will be activated at a later time.</p>';
$lang["_STATS_INFO_ACTIVEUSERS"] = '<p>This line chart displays the number of active users on the server over time. An "active user" is defined as a user that has uploaded a file which is active (that is, available for download) at the time. Please note, the domain exclusion listed below is currently NOT active for this chart and will be activated at a later time.</p>';
$lang["_STATS_INFO_TOTALFILES"] = '<p>This chart shows the total number of uploads on the server over time.</p>';
$lang["_STATS_INFO_TOTALUSERS"] = '<p>This chart shows the total number of users on the server over time. A user is defined as someone that has uploaded at least one file.</p>';
$lang["_STATS_INFO_CUSTOM"] = '<p>This page allows you to customise the graph display by manually selecting a date range and interval frequency.</p>';
$lang["_STATS_INFO_KEY"] = '<p>This page contains some key statistics for your FileSender installation. Use the drop-down list below if you want to restrict the displayed data to a specific year.</p>';

$lang["_STATS_KEY_TITLE"] = "Key statistics for ";
$lang["_STATS_KEY_TITLE_ALL"] = "all years";
$lang["_STATS_KEY_SIZES"] = "File sizes";
$lang["_STATS_KEY_SIZES_BIGGEST"] = "Biggest file: ";
$lang["_STATS_KEY_SIZES_TOTAL"] = "Total file size: ";
$lang["_STATS_KEY_SIZES_AVERAGE"] = "Average file size: ";
$lang["_STATS_KEY_SIZES_BIGGER"] = "Number of files 2GB or larger: ";
$lang["_STATS_KEY_SIZES_SMALLER"] = "Number of files smaller than 2GB: ";

$lang["_STATS_KEY_UPLOADS"] = "Uploads";
$lang["_STATS_KEY_UPLOADS_NUMBER"] = "Number of uploads: ";
$lang["_STATS_KEY_UPLOADS_DATE"] = "Date of first upload: ";
$lang["_STATS_KEY_UPLOADS_DAILY"] = "Average uploads per day: ";
$lang["_STATS_KEY_UPLOADS_UNIQUE"] = "Number of unique uploaders: ";
$lang["_STATS_KEY_UPLOADS_PER_USER"] = "Average number of files per uploader: ";

$lang["_STATS_KEY_DOWNLOADS"] = "Downloads";
$lang["_STATS_KEY_DOWNLOADS_NUMBER"] = "Number of downloads: ";
$lang["_STATS_KEY_DOWNLOADS_DATE"] = "Date of first download: ";
$lang["_STATS_KEY_DOWNLOADS_DAILY"] = "Average downloads per day: ";
$lang["_STATS_KEY_DOWNLOADS_UNIQUE"] = "Number of unique downloaders: ";
$lang["_STATS_KEY_DOWNLOADS_PER_USER"] = "Average number of files per downloader: ";

// vouchers
$lang["_SEND_NEW_VOUCHER"] = "A Voucher allows someone to send you a file.<br />
To create a voucher, enter an email address then select Send Voucher.<br />
An email will be sent to the recipient with a link to use the Voucher.";

// User interaction
$lang["_EMAIL_SEPARATOR_MSG"] = "Multiple email addresses separated by , or ;";
$lang["_NO_FILES"] = "There are currently no files available";
$lang["_NO_VOUCHERS"] = "There are currently no vouchers available";
$lang["_ARE_YOU_SURE"] = "Are you sure?";
$lang["_DELETE_FILE"] = "Delete File";
$lang["_EMAIL_SENT"] = "Message Sent";
$lang["_EXPIRY_DATE"] = "Expiry date";
$lang["_FILE_SIZE"] = "File Size";
$lang["_FILE_RESENT"] = "File Re-sent";	
$lang["_MESSAGE_RESENT"] = "Message Re-sent";			
$lang["_ME"] = "Me";
$lang["_SEND_VOUCHER"] = "Send Voucher";
$lang["_RE_SEND_EMAIL"] = "Re-send Email";
$lang["_NEW_RECIPIENT"] = "Add new recipient";
$lang["_SEND_VOUCHER_TO"] = "Send voucher to";
$lang["_START_DOWNLOAD"] = "Start Download";
$lang["_VOUCHER_SENT"] = "Voucher Sent";
$lang["_VOUCHER_DELETED"] = "Voucher Deleted";
$lang["_VOUCHER_CANCELLED"] = "This voucher has been cancelled.";
$lang["_VOUCHER_USED"] = "This voucher has already been used.";
$lang["_STARTED_DOWNLOADING"] = "Your file should start downloading.";
$lang["_FILE_DELETED"] = "This file has been deleted.";

// steps
$lang["_STEP1"] = "Enter delivery email address(es)";
$lang["_STEP2"] = "Set expiry date";
$lang["_STEP3"] = "Browse for a file";
$lang["_STEP4"] = "Click Send";
$lang["_HTML5Supported"] = "Uploads over 2GB supported!";
$lang["_HTML5NotSupported"] = "Uploads over 2GB not supported!";			

$lang["_OPTIONAL"] = "optional";

// confirmation
$lang["_CONFIRM_DELETE_FILE"] = "Are you sure you want to delete this File?";
$lang["_CONFIRM_DELETE_VOUCHER"] = "Are you sure you want to delete this voucher?";
$lang["_CONFIRM_RESEND_EMAIL"] = "Are you sure you want to re-send this email?";

// standard date display format
$lang['datedisplayformat'] = "d/m/Y"; // Format for displaying date/time, use PHP date() format string syntax 

// datepicker localization
$lang["_DP_closeText"] = 'Done'; // Done
$lang["_DP_prevText"] = 'Prev'; //Prev
$lang["_DP_nextText"] = 'Next'; // Next
$lang["_DP_currentText"] = 'Today'; // Today
$lang["_DP_monthNames"] = "['January','February','March','April','May','June','July','August','September','October','November','December']";
$lang["_DP_monthNamesShort"] = "['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun','Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']";
$lang["_DP_dayNames"] = "['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']";
$lang["_DP_dayNamesShort"] = "['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']";
$lang["_DP_dayNamesMin"] = "['Su','Mo','Tu','We','Th','Fr','Sa']";
$lang["_DP_weekHeader"] = 'Wk';
$lang["_DP_dateFormat"] = 'dd/mm/yy';
$lang["_DP_firstDay"] = '1';
$lang["_DP_isRTL"] = 'false';
$lang["_DP_showMonthAfterYear"] = 'false';
$lang["_DP_yearSuffix"] = '';

// Login Splash text
$lang["_SITE_SPLASHHEAD"] = "Welcome to ". htmlspecialchars($config['site_name']);
$lang["_SITE_SPLASHTEXT"] = htmlspecialchars($config['site_name']) ." is a secure way to share large files with anyone! Logon to upload your files or invite people to send you a file.";

// site help
$lang["_HELP_TEXT"] = '
<div>
<div align="left" style="padding:5px">
<h4>Login</h4> 
<ul>
    <li>You log in through one of the listed Identity Providers using your standard institutional account. If you do not see your institution in the list, or your login fails, please contact your local IT support</li>
</ul>

<h4>Uploads of <i>any size</i> with HTML5</h4>
<ul>
        <li>You\'ll be able to use this method if this sign is displayed: <img src="images/html5_installed.png" alt="green HTML5 tick" class="textmiddle" style="display:inline" /></li>
	<li>To get the <img src="images/html5_installed.png" alt="green HTML5 tick" class="textmiddle" style="display:inline" /> sign, simply use an up to date browser that supports HTML5, the latest version of the "language of the web".</li>
	<li>Up to date versions of Firefox and Chrome on Windows, Mac OS X and Linux are known to work.</li>
	<li>You can <b><i>resume</i></b> an interrupted or cancelled upload.  To resume an upload, simply send the exact same file again!  Make sure the file has the same name as before and <i>'. htmlspecialchars($config['site_name']) .'</i> will recognise it.  When your upload starts, you should notice the progress bar jump to where the upload was halted, and continue from there.<br /><br />
If you <b><i>modified the file</i></b> between the first and second attempt, please rename the file first.  This ensures a new, fresh upload is started and all your changes are properly transferred.</li>
</ul>

<h4>Downloads of any size</h4>
<ul>
        <li>Any modern browser will do just fine.  Don\'t worry about Adobe Flash or HTML5 - these only matter for uploads; nothing special is required for downloads</li>
</ul>

<h4>Uploads smaller than 2 Gigabytes (2GB) with Adobe Flash</h4>
<ul>
	<li>If you can watch YouTube videos this method should work for you</li>
	<li>You need a modern browser with version 10 (or higher) of the <a target="_blank" href="http://www.adobe.com/software/flash/about/">Adobe Flash</a> plugin.</li>
	<li>Using Adobe Flash you can upload file sizes of up to 2 Gigabytes (2GB).  <i>'. htmlspecialchars($config['site_name']) .'</i> will warn you should you try to upload a file that is too big for this method</li>
	<li>Resuming uploads is not supported with this method</li>
</ul>



<h4>Configured service constraints</h4>
<ul>
    <li><strong>
      Maximum recipient  addresses per email:</strong> Up to '. $config["max_email_recipients"].' email addresses separated by  a comma or semi-colon</li>
    <li><strong>Maximum number of files per  upload:</strong> one - to upload several files in one transaction, compress them into a  single archive first</li>
    <li><strong>Maximum file size per upload, with Adobe Flash only: </strong>'. formatBytes($config["max_flash_upload_size"]).' </li>
    <li><strong>Maximum file size per upload, with HTML5: </strong>'. formatBytes($config["max_html5_upload_size"]).'</li>
    <li><strong>Maximum file / voucher expiry days: </strong>'. $config["default_daysvalid"].' </li>
</ul>

<h4>Technical details</h4>
<ul>
	<li><i>'. htmlspecialchars($config['site_name']) .'</i> uses the <a href="http://www.filesender.org/" target="_blank">FileSender software</a>. FileSender indicates whether or not the HTML5 upload method is supported for a particular browser.  This depends mainly on the availability of advanced browser functionality, in particular the HTML5 FileAPI.  Please use the <a href="http://caniuse.com/fileapi" target="_blank">"When can I use..."</a> website to monitor implementation progress of the HTML5 FileAPI for all major browsers.  In particular support for <a href="http://caniuse.com/filereader" target="_blank">FileReader API</a> and <a href="http://caniuse.com/bloburls" target="_blank">Blob URLs</a> needs to be light green (=supported) for a browser to support uploads larger then 2GB.  Please note that although Opera 12 is listed to support the HTML5 FileAPI, it currently does not support all that is needed to support use of the HTML5 upload method in FileSender.</li>
</ul>

<p>For more information please visit <a href="http://www.filesender.org/" target="_blank">www.filesender.org</a></p>
</div>
</div>';

// site about
$lang["_ABOUT_TEXT"] = ' <div align="left" style="padding:5px">'. htmlspecialchars($config['site_name']) .' is an installation of FileSender (<a rel="nofollow" href="http://www.filesender.org/" target="_blank">www.filesender.org</a>), which is developed to the requirements of the higher education and research community.</div>';

// site AUP terms
$lang["_AUPTERMS"] = "AuP Terms and conditions...";

?>
