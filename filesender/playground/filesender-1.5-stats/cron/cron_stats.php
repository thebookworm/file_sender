<?php

/*
 * FileSender www.filesender.org
 * 
 * Copyright (c) 2009-2012, AARNet, Belnet, HEAnet, SURFnet, UNINETT
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * *	Redistributions of source code must retain the above copyright
 * 	notice, this list of conditions and the following disclaimer.
 * *	Redistributions in binary form must reproduce the above copyright
 * 	notice, this list of conditions and the following disclaimer in the
 * 	documentation and/or other materials provided with the distribution.
 * *	Neither the name of AARNet, Belnet, HEAnet, SURFnet and UNINETT nor the
 * 	names of its contributors may be used to endorse or promote products
 * 	derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
// required as this page is called from CRON not from a web browser
chdir(dirname(__FILE__));

// force all error reporting
if (defined('E_DEPRECATED')) {
	error_reporting(E_ALL & ~E_DEPRECATED);
} else {
	error_reporting(E_ALL);
}

$filesenderbase = dirname(dirname(__FILE__));

// include all required classes
require_once("$filesenderbase/config/config.php");

$CFG = config::getInstance();
$config = $CFG->loadConfig();
 
require_once("$filesenderbase/includes/ErrorHandler.php");
require_once("$filesenderbase/classes/AuthSaml.php");
require_once("$filesenderbase/classes/AuthVoucher.php");
require_once("$filesenderbase/classes/DB.php");
require_once("$filesenderbase/classes/Log.php");
require_once("$filesenderbase/classes/Mail.php");
require_once("$filesenderbase/classes/Functions.php");

// set cron variable to force
$cron = true;

// set time zone for this session
date_default_timezone_set($config['Default_TimeZone']);

// check if session already exists
if(session_id() == ""){
	// start new session and mark it as valid because the system is a trusted source
	session_start();
	$_SESSION['validSession'] = true;
} 

logprocess("CRON", "Starting statistics Cron tasks");
if (anonymiseLogs() && updateStatsData()) {
	// cron completed - log
	logProcess("CRON", "Cron Complete");
} else {
	// email admin - error in Cron
	logProcess("CRON", "Cron Error - check error log");
}
 
function logProcess($client,$message) {
	global $config;
	
	if($config["debug"])
	{
		$dateref = date("Ymd");
		$data = date("Y/m/d H:i:s");
		$myFile = $config['log_location'].$dateref."-".$client.".log.txt";
		$fh = fopen($myFile, 'a') or die("can't open file");
		// don't print errors on screen when there is no session.
		if(isset($_REQUEST['PHPSESSID'])){
			$sessionId = $_REQUEST['PHPSESSID'];
		} else {
			$sessionId = "none";
		}
		$stringData = $data.' [Session ID: '.$sessionId.'] '.$message."\n";
		fwrite($fh, $stringData);
		fclose($fh);
		closelog();
	}
}

function anonymiseLogs() {
	$db = DB::getInstance();
	
	// Find the highest ID in the stats table.
	$highestId = 0; 
	
	try {
		$query = $db->fquery("SELECT max(statid) FROM stats");
		$result = $query->fetch(PDO::FETCH_NUM);
		$stmnt = NULL; 
		$highestId = $result[0];
	} catch (Exception $e) {
		logProcess("CRON", "SQL Error Retrieving stats table row numbers " . $e->getMessage());
		return false;
	}
	
	if ($highestId == NULL) $highestId = 0;
	
	// Get any log items with higher IDs (these have not yet been anonymised).
	$statement = "";
	try {
		$statement = $db->fquery("SELECT logid, logfileuid, logtype, logfrom, logto, logdate, logfilesize, logsessionid, logvoucheruid, logauthuseruid FROM logs WHERE logid>" . $highestId);
	} catch (Exception $e) {
		logProcess("CRON", "SQL Error Polling logs table " . $e->getMessage());
		return false;
	}
	
	// For each log item found, anonymise and insert into stats table.
	$pdo = $db->connect();
	foreach ($statement as $row) {
		$fromHash = hash('sha256', $row["logfrom"]);
		$fromDomain = explode('@', $row["logfrom"]);
		$toDomain = explode('@', $row["logto"]);
		
		$statement = $pdo->prepare('INSERT INTO stats (
			statid,
			statfileuid,
			statlogtype,
			statfromdomain,
			statfromhashed,
			stattodomain,
			statdate,
			statfilesize,
			statsessionid,
			statvoucheruid,
			statauthuseruid
            ) VALUES ( 	
			:statid, 
			:statfileuid,
			:statlogtype,
			:statfromdomain,
			:statfromhashed,
			:stattodomain,
			:statdate,
			:statfilesize,
			:statsessionid,
			:statvoucheruid,
			:statauthuseruid)');	
				
		$statement->bindParam(':statid', $row['logid']);
		$statement->bindParam(':statfileuid', $row['logfileuid']);
		$statement->bindParam(':statlogtype', $row['logtype']);
		$statement->bindParam(':statfromdomain', $fromDomain[1]);
		$statement->bindParam(':statfromhashed', $fromHash);
		$statement->bindParam(':stattodomain', $toDomain[1]);
		$statement->bindParam(':statdate', $row['logdate']);
		$statement->bindParam(':statfilesize', $row['logfilesize']);
		$statement->bindParam(':statsessionid', $row['logsessionid']);
		$statement->bindParam(':statvoucheruid', $row['logvoucheruid']);
		$statement->bindParam(':statauthuseruid', $row['logauthuseruid']);
		
		try {
			$statement->execute();
		} catch (Exception $e) {
			logProcess("CRON", "SQL Error Inserting into stats table " . $e->getMessage());
			return false;
		}
	}
	logProcess("CRON", "Completed anonymisation process");
	return true;
}

// ---------------------------
// Retrieve all data for the standard charts from the DB and store as a json array which can be fetched from jQuery later.
// This is done in CRON because the 'stats' DB table is also updated from here, and because it is work intensive.
// ---------------------------
function updateStatsData() {
	global $config;
	$functions = Functions::getInstance();

	// Get the possible frequencies and computed start/end dates for those frequencies.
	$frequencies = $config['stats_available_frequencies'];
	
	$dates = $functions->getGraphDates($frequencies);
	
	$megaByte = 1024 * 1024; 
	$gigaByte = $megaByte * 1024;
	
	// Get data from the DB.
	$chartData = array();
	
	$chartData['updown'] = 	array($functions->upDownStats($frequencies, 'Uploaded'),
								  $functions->upDownStats($frequencies, 'Download'));
	
	$chartData['filesizes'] =	array($functions->fileSizesStats($frequencies, $dates, 0, (20 * $megaByte)),
									  $functions->fileSizesStats($frequencies, $dates, (20 * $megaByte), (200 * $megaByte)),
									  $functions->fileSizesStats($frequencies, $dates, (200 * $megaByte), (2 * $gigaByte)),
									  $functions->fileSizesStats($frequencies, $dates, (2 * $gigaByte)));
	
	// jqPlot freaks out if there are missing values in a stacked line chart, so we need to fill those in using padArrays().
	padArrays($chartData['filesizes'][0], $chartData['filesizes'][1]);
	padArrays($chartData['filesizes'][0], $chartData['filesizes'][2]);
	padArrays($chartData['filesizes'][0], $chartData['filesizes'][3]);
	
	$chartData['vouchers'] = array($functions->vouchersStats($frequencies, $dates, false), 
								   $functions->vouchersStats($frequencies, $dates, true));
	
	$chartData['totalupdown'] = array($functions->totalUpDownStats($frequencies, $dates, 'Uploaded'), 
									  $functions->totalUpDownStats($frequencies, $dates, 'Download'));
	
	$chartData['activefiles'] = array($functions->activeFilesStats($frequencies));
	$chartData['activeusers'] = array($functions->activeUsersStats($frequencies));
	
	$chartData['totalfiles'] = array($functions->totalFilesStats($frequencies, $dates));
	$chartData['totalusers'] = array($functions->totalUsersStats($frequencies, $dates));
	
	
	// Also create merged versions of the arrays that we can create HTML tables from later.
	$tableData = array();
	foreach ($frequencies as $frequency) {
		$tableData['updown'][$frequency] = mergeArrays($chartData['updown'][0][$frequency], $chartData['updown'][1][$frequency]);
		
		$first = $chartData['filesizes'][0][$frequency];
		$second = $chartData['filesizes'][1][$frequency];
		$third = $chartData['filesizes'][2][$frequency];
		$fourth = $chartData['filesizes'][3][$frequency];
		
		$tableData['filesizes'][$frequency] = mergeArrays(mergeArrays(mergeArrays($first, $second), $third), $fourth);
		
		$tableData['vouchers'][$frequency] = mergeArrays($chartData['vouchers'][0][$frequency], $chartData['vouchers'][1][$frequency]);
		
		$tableData['totalupdown'][$frequency] = mergeArrays($chartData['totalupdown'][0][$frequency], $chartData['totalupdown'][1][$frequency]);
		
		$tableData['activefiles'][$frequency] = $chartData['activefiles'][0][$frequency];
		$tableData['activeusers'][$frequency] = $chartData['activeusers'][0][$frequency];
		
		$tableData['totalfiles'][$frequency] = $chartData['totalfiles'][0][$frequency];
		$tableData['totalusers'][$frequency] = $chartData['totalusers'][0][$frequency];
	}
	
	// Save the data to a file that can be retrieved on-demand.
	file_put_contents("../www/stats/data/data.json", json_encode(array("charts" => $chartData, "tables" => $tableData), JSON_NUMERIC_CHECK));
	logProcess("CRON", "Statistics data have been updated");
	return true;
}

// ---------------------------
// Checks for missing rows in 2D arrays and fills them with the value from the same cell in the previous row.
// This assumes the first column is always a date string and never empty, and the second column is an integer which may be empty.
// ---------------------------
function padArrays(&$first, &$second) {
	global $config;
	$frequencies = $config['stats_available_frequencies'];
	
	foreach ($frequencies as $frequency) {
		foreach ($first[$frequency] as $row) {
			$result = arrayFind($row[0], $second[$frequency]);
			
			if ($result === false) {
				// The missing cell is in the last row of the table, get the replace value from the end of the array.
				$last = end($second[$frequency]);
				$second[$frequency][] = array($row[0], $last[1]);
			} else if ($result !== true) {
				// The missing cell is somewhere before the last row, get the replace value from the previous row.
				$replaceValue = ($result == 0) ? 0 : $second[$frequency][$result-1][1];
				array_splice($second[$frequency], $result, 0, array(array($row[0], $replaceValue)));
			}
		}
	}
}

// ---------------------------
// Support function that checks if a date value exists in the first column of an array.
// If not, returns the position where the value should have been, or false if it should be at the end of the array.
// ---------------------------
function arrayFind($needle, $array) {
	for ($i = 0; $i < count($array); $i++) {
		if ($needle === $array[$i][0]) {
			return true;
		} else if (strtotime($needle) < strtotime($array[$i][0])) {
			return $i;
		}
	}
	return false;
}

function mergeArrays($first, $second) {
	$merged = array(); 
	
	foreach ($first as $firstRow) {
		foreach ($second as $secondRow) {
			if ($firstRow[0] == $secondRow[0]) {
				$firstRow[] = $secondRow[1];
			}
		}
		$merged[] = $firstRow;
	}
	return $merged;
}
?>