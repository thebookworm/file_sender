<?php
        // Statistics module
        $config['stats_access_level'] = 'public'; // Who can access stats page. Possible settings are 'admin', 'user', 'public' or 'disabled'. A more restrictive setting here would override the access settings below.
        $config['stats_access_level_updown'] = 'public'; // Availability of uploads/downloads graph. 'admin', 'user', 'public' or 'disabled'.
        $config['stats_access_level_filesizes'] = 'public'; // Availability of file sizes graph. 'admin', 'user', 'public' or 'disabled'.
        $config['stats_access_level_vouchers'] = 'public'; // Availability of invite vouchers graph. 'admin', 'user', 'public' or 'disabled'.
        $config['stats_access_level_totalupdown'] = 'public'; // Availability of total uploads/downloads graph. 'admin', 'user', 'public' or 'disabled'.
        $config['stats_access_level_activefiles'] = 'public'; // Availability of active files graph. 'admin', 'user', 'public' or 'disabled'.
        $config['stats_access_level_activeusers'] = 'public'; // Availability of active users graph. 'admin', 'user', 'public' or 'disabled'.
        $config['stats_access_level_totalfiles'] = 'public'; // Availability of total files graph. 'admin', 'user', 'public' or 'disabled'.
        $config['stats_access_level_totalusers'] = 'public'; // Availability of total users graph. 'admin', 'user', 'public' or 'disabled'.

        $config['stats_display_documentation'] = true; // Display information about the current graph on each page (true/false).
        $config['stats_documentation_position'] = 'top'; // Where on the page should the information be displayed ('top' or 'bottom').
        $config['excluded_domains'] = array(); // FROM domains that should not be included in the statistics.  Example: array('uninett.no', 'aarnet.edu.au'). Use array() to include everything.;

        $config['stats_available_frequencies'] = array('year', 'month', 'week', 'day'); // Which interval frequencies are enabled. 'year', 'month', 'week' and 'day' are possible options.
        $config['stats_default_frequency'] = 'week'; // The interval frequency that is shown on the stats page by default. Must be defined as enabled above.

        // The following describe how far back each graph frequency should display, e.g. a graph with aggregated yearly stats may go 5 years back in time.
        // NOTE: Changes here will not take effect until next time cron.php is run.
        $config['stats_offset_year'] = array('year' => 5, 'mon' => 0, 'mday' => 0); // Default 5, 0, 0 (5 years)
        $config['stats_offset_month'] = array('year' => 1, 'mon' => 0, 'mday' => 0); // Default 1, 0, 0 (1 year)
        $config['stats_offset_week'] = array('year' => 0, 'mon' => 6, 'mday' => 0); // Default 0, 6, 0 (6 months)
        $config['stats_offset_day'] = array('year' => 0, 'mon' => 0, 'mday' => 7); // Default 0, 0, 7 (7 days)
?>
