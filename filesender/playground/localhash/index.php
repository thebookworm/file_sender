<!DOCTYPE html>
<html lang="en">
  <head>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="sha256.js"></script>
    <script>
        $(document).ready(function () {
            if (window.File && window.FileReader && window.FileList && window.Blob) {
                $('#file').bind('change', function (evt) {
                    var files = evt.target.files; // FileList object
                    var chunkSize = 5000000;

                    // loop through all files specified, typically 1
                    for (var i = 0, f; f = files[i]; i++) {
                        var fileSize = f.size;
                        var sha256 = CryptoJS.algo.SHA256.create();

                        //for (var start = 0; start < fileSize; start += chunkSize) {
                        var readFile = function (file, start) {
                            if (start >= fileSize) {
                                var hash = sha256.finalize();
                                hashstring = hash.toString(CryptoJS.enc.Hex);
                                $('#result').html(hashstring);
                                return;
                            }

                            var end = chunkSize + start;
                            end = end > fileSize ? fileSize : end;

                            var reader = new FileReader();
                            reader.onload = function (e) {
                                sha256.update(CryptoJS.enc.Latin1.parse(e.target.result));
                                $('#progress').html(end / fileSize * 100);
                                readFile(file, end);
                            };

                            var chunk = file.slice(start, end);
                            reader.readAsBinaryString(chunk);
                        };
                        readFile(f, 0);
                        //}

                        // we're done now, finish.
                    }
                });
            } else {
                alert('The File APIs are not fully supported in this browser.');
            }
        });
    </script>
  </head>
  <body>
    <h1>File Hashing Demo</h1>
<label>File</label>
    <input type="file" id="file" name="file" />
    <pre id="progress"></pre>
    <pre id="result"></pre>
  </body>
</html>
