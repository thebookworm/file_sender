Updated: 21 Feb 2013

Installation instructions
=========================

FileSender installation instructions for version 1.5 are available at:
   https://www.assembla.com/spaces/file_sender/wiki/Installation_-_Linux_Source

FileSender depends on a number of common software packages.  Be sure to read
the installation guide sections regarding configuring PostgreSQL, PHP, Apache,
SimpleSAMLphp and cron to get up and running.


Upgrading from version 1.0.1, 1.1 or 1.1.1
==========================================

When upgrading from previous production releases to version 1.5 there are a
number of important issues to pay attention to:

   - You MUST update your database schema.

   - You are encouraged to start with a fresh configuration file.  Use the
     config-dist.php as a template.  Many configuration directives have been
     added, removed and changed.  Remember to make a copy of your current
     config.php!

   - Version 1.5 supports multiple languages and most text customisations of
     the web UI have been moved to language files.  Email templates are still
     found in config.php and are offered only in English.

   - The date format is configured in 3 places, 1 directive in the config.php
     and 2 in the language file(s) in use on your site.

   - The semantics for the "help" and "about" directives in config.php have
     changed, and the actual texts are now in the language files.  Check the
     online upgrade notes for details.

It is recommended to make a backup of your current database, config.php and
filesender installation before upgrading, especially if you have made local
modifications to the installed version.

Details about upgrade considerations are available at:
   https://www.assembla.com/spaces/file_sender/wiki/Upgrade_notes/


Upgrading from 1.5-beta versions
================================

Consult the detailed development notes if you're upgrading to 1.5-release from
one of the 1.5 betas:

https://www.assembla.com/spaces/file_sender/wiki/Installation_notes_for_1-5_development_code


Configuration
=============

The main configuration of FileSender is done in:
  - config/config.php

The .tar/.zip distribution file contains a fresh configuration file template
with all configurable parameters with sane defaults in:
  - config/config-dist.php

You'll have to create a working config/config.php from this template.

In the various beta cycles several new and required settings have been added to
the config.php that might not be present in your current configuration.  In
addition some settings have changed semantics or syntax.

IMPORTANT: When upgrading please start with a fresh configuration file and take
the time to go through all configuration directives to make sure all is as it
should be. Merging new entries into your old config.php is discouraged.


Multi-language support
======================

As of version 1.5 the FileSender supports multi-language for the web user
interface, using language files and the standard browser language preference
mechanism.  If you select for example "Norwegian [no]" as the preferred
language to show web sites in, FileSender will take its language from the
language/no_NO.php file.

FileSender comes with a number of language files.  You can add your own
language file and override text labels from standard langauge files.

Please consult the "Customising and configuring multi-language support" section
of the administrator reference guide for details:

https://www.assembla.com/spaces/file_sender/wiki/Administrator_reference_manual#customising_and_configuring_multi_language_support

**Please note the email texts are still in the config.php configuration file! **


Customisation
=============

For instructions on customisation of your FileSender site please check the
section on "Customising and configuring multi language support" in the
Administrator reference guide:

https://www.assembla.com/spaces/file_sender/wiki/Administrator_reference_manual#customising_and_configuring_multi_language_support

