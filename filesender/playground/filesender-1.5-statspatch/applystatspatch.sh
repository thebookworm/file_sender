#!/bin/bash

set -x 

YOUR_FILESENDER_INSTALLATION_BASEDIR='/var/www/filesender-1.5-beta4'
PATCH_DIRECTORY='.'
PATCHNAME='filesender-1.5-beta4-statspatch-v1'

PATCHBASE=${PATCH_DIRECTORY}

#begin 
echo "copying new files from ${PATCHBASE} to ${YOUR_FILESENDER_INSTALLATION_BASEDIR}"

# copy stats module related config to config directory
cp ${PATCHBASE}/config-templates/config.statspatch-dist.php ${YOUR_FILESENDER_INSTALLATION_BASEDIR}/config-templates/

# copy stats cronjob in place
cp ${PATCHBASE}/cron/cron_stats.php ${YOUR_FILESENDER_INSTALLATION_BASEDIR}/cron/

# copy new pages to pages directory
cp ${PATCHBASE}/pages/customchart.php ${YOUR_FILESENDER_INSTALLATION_BASEDIR}/pages/
cp ${PATCHBASE}/pages/stats.php ${YOUR_FILESENDER_INSTALLATION_BASEDIR}/pages/

# copy rest of stats module to www/stats
cp -r ${PATCHBASE}/www/stats ${YOUR_FILESENDER_INSTALLATION_BASEDIR}/www/

echo "patching www/index.php, classes/Functions.php, www/css/default.css and language/en_AU.php"

/usr/bin/patch -i ${PATCHBASE}/patched_files/Functions.php_${PATCHNAME}.patch ${YOUR_FILESENDER_INSTALLATION_BASEDIR}/classes/Functions.php
/usr/bin/patch -i ${PATCHBASE}/patched_files/en_AU.php_${PATCHNAME}.patch ${YOUR_FILESENDER_INSTALLATION_BASEDIR}/language/en_AU.php
/usr/bin/patch -i ${PATCHBASE}/patched_files/index.php_${PATCHNAME}.patch ${YOUR_FILESENDER_INSTALLATION_BASEDIR}/www/index.php
/usr/bin/patch -i ${PATCHBASE}/patched_files/default.css_${PATCHNAME}.patch ${YOUR_FILESENDER_INSTALLATION_BASEDIR}/www/css/default.css

chgrp -R www-data ${YOUR_FILESENDER_INSTALLATION_BASEDIR}/www/stats/data

echo "done!"
