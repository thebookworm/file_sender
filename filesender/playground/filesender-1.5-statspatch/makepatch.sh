#!/bin/bash

set -x 

#
# CHANGE THESE 4 LINES TO REFLECT CURRENT VERSIONS, PATHS ETC.

PATCHNAME='filesender-1.5-beta4-statspatch-v1'
PATCHWORKDIR='/root/statspatch'
DEVCODEBASE=${PATCHWORKDIR}'/originals/filesender-1.5-stats/'
FILESENDERBASE=${PATCHWORKDIR}'/originals/filesender-1.5-beta4/'


#
PATCHBASE=${PATCHWORKDIR}/${PATCHNAME} 
NOW=`/bin/date +"%d-%b-%y.%Hh%M"`

#
# start with setting up the code we need

# create directories where stats code and filesender code will be svn-ed into
mkdir -p ${DEVCODEBASE}
mkdir ${FILESENDERBASE}


# prepare stats module development code.  Checkout the code from SVN and throw it through dos2unix to get rid
# of any remaining Windows line endings.

/usr/bin/svn checkout https://subversion.assembla.com/svn/file_sender/filesender/playground/filesender-1.5-stats ${DEVCODEBASE}
find ${DEVCODEBASE} -name "*" -type f -exec dos2unix -o {} \;

# prepare the FileSender code we'll be making the patch for.  Check it out of SVN.
/usr/bin/svn checkout https://subversion.assembla.com/svn/file_sender/filesender/tags/filesender-1.5-beta4 ${FILESENDERBASE}

#
# start making the patch.

#create patch base directory
mkdir ${PATCHBASE}

# download applypatch.sh and standard config file template to patch workdir for later copying
/usr/bin/svn checkout https://subversion.assembla.com/svn/file_sender/filesender/playground/filesender-1.5-statspatch/ ${PATCHWORKDIR}/originals

#create directories for files that need patching or new files:
mkdir ${PATCHBASE}/cron
mkdir ${PATCHBASE}/pages
mkdir ${PATCHBASE}/patched_files
mkdir ${PATCHBASE}/scripts
mkdir ${PATCHBASE}/config-templates
mkdir ${PATCHBASE}/www

# generate patch for classes/Functions.php: stats code is added at the end
/usr/bin/diff -c ${FILESENDERBASE}/classes/Functions.php ${DEVCODEBASE}/classes/Functions.php > ${PATCHBASE}/patched_files/Functions.php_${PATCHNAME}.patch

# generate patch for default language file
/usr/bin/diff -c ${FILESENDERBASE}/language/en_AU.php ${DEVCODEBASE}/language/en_AU.php > ${PATCHBASE}/patched_files/en_AU.php_${PATCHNAME}.patch

# generate patch for index.php
/usr/bin/diff -c ${FILESENDERBASE}/www/index.php ${DEVCODEBASE}/www/index.php > ${PATCHBASE}/patched_files/index.php_${PATCHNAME}.patch

# generate patch for default.css, stats code is added at the end
/usr/bin/diff -c ${FILESENDERBASE}/www/css/default.css ${DEVCODEBASE}/www/css/default.css > ${PATCHBASE}/patched_files/default.css_${PATCHNAME}.patch

# copy applypatch.sh script into patch directory
cp ${PATCHWORKDIR}/originals/applystatspatch.sh ${PATCHBASE}
#/usr/bin/svn checkout https://subversion.assembla.com/svn/file_sender/filesender/playground/filesender-1.5-statspatch/applystatspatch.sh ${PATCHBASE}/
#/usr/bin/wget -P $PATCHBASE/ http://www.assembla.com/code/file_sender/subversion/nodes/filesender/playground/filesender-1.5-statspatch/applystatspatch.sh
#/usr/bin/wget -P $PATCHBASE/ https://www.assembla.com/code/file_sender/subversion/nodes/filesender/playground/filesender-1.5-statspatch/applystatspatch.sh

# copy config file template into config-templates directory
#/usr/bin/wget -P $PATCHBASE/config-templates/ https://www.assembla.com/code/file_sender/subversion/nodes/filesender/playground/filesender-1.5-statspatch/config.stats-patch
cp ${DEVCODEBASE}/config/config.stats-patch.php ${PATCHBASE}/config-templates/config.statspatch-dist.php

# copy stats cronjob
cp ${DEVCODEBASE}/cron/cron_stats.php ${PATCHBASE}/cron

# copy new pages to pages directory
cp ${DEVCODEBASE}/pages/customchart.php ${PATCHBASE}/pages
cp ${DEVCODEBASE}/pages/stats.php ${PATCHBASE}/pages

# copy sql to create stats table in postgres
cp ${DEVCODEBASE}/scripts/filesender_db_statsmodule.sql ${PATCHBASE}/scripts

# copy rest of stats module to its home in www/stats
cp -r ${DEVCODEBASE}/www/stats ${PATCHBASE}/www

#remove .svn dirs. 
find ${PATCHBASE}/www/stats -depth -name '.svn' -exec rm -r {} \;

# www/stats/data/data.json is used to gather data points to be plotted.  Remove this in the patch.
rm ${PATCHBASE}/www/stats/data/*

cd ${PATCHWORKDIR}
tar cvzf ${PATCHNAME}.${NOW}.tar.gz ${PATCHNAME}

#cleanup
#rm -rf ${PATCHBASE}
rm -rf ${PATCHWORKDIR}/originals
