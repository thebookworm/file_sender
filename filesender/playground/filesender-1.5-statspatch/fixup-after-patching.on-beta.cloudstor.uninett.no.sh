#!/bin/bash

FILESENDER=/var/www/filesender-1.5-beta4
TARBALL=/var/www/filesender-1.5-beta4.tar.gz
FILESENDERLINK=/var/www/filesender

NOW==`/bin/date +"%d-%b-%y.%Hh%M"`

cp $FILESENDER/config-templates/config.statspatch-dist.php $FILESENDER/config/config.stats-patch.php
chgrp www-data $FILESENDER/config/config.stats-patch.php
php -q /var/www/filesender/cron/cron_stats.php

