#!/bin/bash

FILESENDER=/var/www/filesender-1.5-beta4
TARBALL=/var/www/filesender-1.5-beta4.tar.gz
FILESENDERLINK=/var/www/filesender

NOW=`/bin/date +"%d-%b-%y.%Hh%M"`

rm ${FILESENDERLINK}
mv ${FILESENDER} ${FILESENDER}.prepatch.${NOW}
tar xvzf ${TARBALL} -C /var/www
ln -s $FILESENDER $FILESENDERLINK
cp -p ${FILESENDER}.prepatch.${NOW}/config/* $FILESENDER/config/
