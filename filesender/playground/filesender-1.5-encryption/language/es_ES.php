<?php

/*
 * FileSender www.filesender.org
 * 
 * Copyright (c) 2009-2012, AARNet, Belnet, HEAnet, SURFnet, UNINETT
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * *	Redistributions of source code must retain the above copyright
 * 	notice, this list of conditions and the following disclaimer.
 * *	Redistributions in binary form must reproduce the above copyright
 * 	notice, this list of conditions and the following disclaimer in the
 * 	documentation and/or other materials provided with the distribution.
 * *	Neither the name of AARNet, Belnet, HEAnet, SURFnet and UNINETT nor the
 * 	names of its contributors may be used to endorse or promote products
 * 	derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
** Spanish translation/version by:
**  fco.sanchez@unia.es and vjhergom@cic.upo.es
**  March 2012
*/

/* ---------------------------------
 * es_ES Language File
 * ---------------------------------
 * 
*/
// main menu items
$lang["_ADMIN"] = "Administraci&oacute;n";
$lang["_NEW_UPLOAD"] = "Enviar archivo";
$lang["_VOUCHERS"] = "Invitaciones";
$lang["_LOGON"] = "Entrar";
$lang["_LOG_OFF"] = "Salir";
$lang["_MY_FILES"] = "Mis archivos";

// page titles
$lang["_HOME"] = "Inicio";
$lang["_ABOUT"] = "Cr&eacute;ditos";
$lang["_HELP"] = "Ayuda";
$lang["_DELETE_VOUCHER"] = "Invitaci&oacute;n de env&iacute;o eliminada";
$lang["_UPLOAD_COMPLETE"] = "El archivo ha sido subido y el mensaje enviado";
$lang["_UPLOAD_PROGRESS"] = "Progreso de subida";
$lang["_DOWNLOAD"] = "Descarga";
$lang["_CANCEL_UPLOAD"] = "Cancelar subida";

// admin
$lang["_PAGE"] = "P&aacute;gina";
$lang["_UP"] = "Arriba";
$lang["_DOWN"] = "Abajo";
$lang["_FILES"] = "Archivos";
$lang["_DRIVE"] = "Directorio";
$lang["_TOTAL"] = "Total";
$lang["_USED"] = "En uso";
$lang["_AVAILABLE"] = "Disponible";
$lang["_TEMP"] = "Temp"; // as in Temporary files

$lang["_WELCOME"] = "Bienvenido"; 
$lang["_WELCOMEGUEST"] = "Bienvenido invitado"; 

// admin tab names
$lang["_GENERAL"] = "General";
$lang["_UPLOADS"] = "Subidos";
$lang["_DOWNLOADS"] = "Descargados";
$lang["_ERRORS"] = "Errores";
$lang["_FILES_AVAILABLE"] = "Archivos disponibles";
$lang["_ACTIVE_VOUCHERS"] = "Invitaciones de env&iacute;o activas";
$lang["_COMPLETE_LOG"] = "Completados";


// Form Fields
$lang["_TO"] = "Para";
$lang["_FROM"] = "De";
$lang["_SIZE"] = "Tama&ntilde;o";
$lang["_CREATED"] = "Fecha de creaci&oacute;n";
$lang["_FILE_NAME"] = "Nombre del archivo";
$lang["_SUBJECT"] = "Asunto";
$lang["_EXPIRY"] = "Fecha de expiraci&oacute;n";
$lang["_MESSAGE"] = "Mensaje";
$lang["_TYPE"] = "Tipo";

$lang["_TERMS_OF_AGREEMENT"] = "Condiciones de servicio";
$lang["_SHOW_TERMS"] = "Mostrar los t&eacute;rminos";
$lang["_SHOWHIDE"] = "Mostrar/Ocultar";
$lang["_UPLOADING_WAIT"] = "Subiendo el archivo: espere por favor...";

// Flash button menu
$lang["_UPLOAD"] = "Enviar un archivo";
$lang["_BROWSE"] = "Navegar";
$lang["_CANCEL"] = "Cancelar";
$lang["_OPEN"] = "Abrir";
$lang["_CLOSE"] = "Cerrar";
$lang["_OK"] = "Ok";
$lang["_SEND"] = "Enviar";
$lang["_DELETE"] = "Eliminar";
$lang["_YES"] = "Si";
$lang["_NO"] = "No";

$lang["_INVALID_MISSING_EMAIL"] = "Falta la direcci&oacute;n o no es v&aacute;lida";	
$lang["_INVALID_EXPIRY_DATE"] = "Fecha de expiraci&oacute;n no v&aacute;lida";	
$lang["_INVALID_FILE"] = "Archivo no v&aacute;lido";	
$lang["_INVALID_FILEVOUCHERID"] = "Archivo o Invitaci&oacute;n de Env&iacute;o no v&aacute;lidos";	
$lang["_INVALID_FILESIZE_ZERO"] = "Archivo de tama&ntilde;o 0. Seleccione otro archivo";
$lang["_INVALID_FILE_EXT"] = "Extensi&oacute;n de archivo no permitida";
$lang["_INVALID_TOO_LARGE_1"] = "El tama&ntilde;o del archivo debe ser inferior a";
$lang["_AUTH_ERROR"] = "Ya no est&aacute; autenticado. <br />Su sesi&oacute;n debe haber expirado o ha habido un error en el servidor /><br />Entre de nuevo y vuelva a intentarlo";
$lang["_SELECT_ANOTHER_FILE"] = "Seleccione otro archivo";
$lang["_INVALID_VOUCHER"] = "Esta Invitaci&oacute;n de env&iacute;o ha caducado. <br />Contacte con el emisor del mismo";
$lang["_SELECT_FILE"] = "Seleccione el archivo";
$lang["_INVALID_FILE_NAME"] = "Nombre de archivo no v&aacute;lido. C&aacute;mbiele el nombre y vuelva a intentarlo";
$lang["_INVALID_SIZE_USEHTML5"] = "Seleccione otro archivo o use un navegador compatible con HTLM5 para subir archivos mayores";
$lang["_ACCEPTTOC"] = "Acepto la Pol&iacute;tica y Condiciones de servicio";
$lang["_AGREETOC"] = "DEBE aceptar la Pol&iacute;tica y Condiciones de servicio";
$lang["_FILE_TO_BE_RESENT"] = "Archivo a compartir";
$lang["_ERROR_UPLOADING_FILE"] = "Error al subir el archivo";
$lang["_MAXEMAILS"] = "El n&uacute;mero m&aaacute;ximo de destinatarios del correo son";
$lang["_INVALID_DATE_FORMAT"] = "Formato de fecha err&oacute;o";
$lang["_DISK_SPACE_ERROR"] = "Espacio de disco insuficiente. Contacte con el administrador del servicio o suba un archivo menor";

$lang["_LOGOUT_COMPLETE"] = "Logout completado";

// vouchers
$lang["_SEND_NEW_VOUCHER"] = "Con las \"Invitaciones de env&iacute;o\", cualquiera puede enviale un archivo.<br />
Para generar una \"Invitaci&oacute;n de env&iacute;o\", escriba una direcci&oacute;n de correo y pulse en \"Enviar la invitaci&oacute;n\".<br />
El destinatario recibir&aacute; un correo con un enlace a la invitaci&oacute;n.<p>";
$lang["_EMAIL_SEPARATOR_MSG"] = "M&uacute;ltiples direcciones de correo separadas por ',' o ';'";

$lang["_NO_FILES"] = "No hay archivos disponbles";
$lang["_ARE_YOU_SURE"] = "&iquest;Est&aacute; seguro?";
$lang["_DELETE_FILE"] = "Eliminar archivo";
$lang["_EMAIL_SENT"] = "Mensaje enviado";
$lang["_EXPIRY_DATE"] = "Fecha de expiraci&oacute;n";
$lang["_FILE_SIZE"] = "Tama&ntilde;o del archivo";
$lang["_FILE_RESENT"] = "Archivo reenviado";
$lang["_MESSAGE_RESENT"] = "Mensaje reenviado";
$lang["_ME"] = "Yo";
$lang["_SEND_VOUCHER"] = "Enviar la invitaci&oacute;n";
$lang["_RE_SEND_EMAIL"] = "Renviar eMail";
$lang["_NEW_RECIPIENT"] = "A&ntilde;adir nuevo destinatario";
$lang["_SEND_VOUCHER_TO"] = "Invitar a";
$lang["_START_DOWNLOAD"] = "Comenzar la descarga";
$lang["_VOUCHER_SENT"] = "Invitaci&oacute;n enviada";
$lang["_VOUCHER_DELETED"] = "Invitaci&oacute;n eliminada";
$lang["_VOUCHER_CANCELLED"] = "Esta invitaci&oacute;n ha sido cancelada";
$lang["_STARTED_DOWNLOADING"] = "Su archivo debe comenzar a descargarse";

// files
$lang["_FILE_DELETED"] = "Archivo eliminado";
// steps
$lang["_STEP1"] = "Escriba las direcciones de destino";
$lang["_STEP2"] = "Selecione la fecha de expiraci&oacute;n";
$lang["_STEP3"] = "Busque y seleccione el archivo";
$lang["_STEP4"] = "Pulse en \"Enviar\"";
$lang["_HTML5Supported"] = "¡Permite archivos mayores de 2GB!";
$lang["_HTML5NotSupported"] = "Archivos menores de 2GB";

$lang["_OPTIONAL"] = "opcional";

// confirmation
$lang["_CONFIRM_DELETE_FILE"] = "&iquest;Est&aacute; seguro de que quiere eliminar este archivo?";
$lang["_CONFIRM_DELETE_VOUCHER"] = "&iquest;Est&aacute; seguro de que quiere eliminar esta Invitaci&oacute;n de env&iacute;o?";

// standard date display format
$lang['datedisplayformat'] = "d/m/Y"; // Format for displaying date/time, use PHP date() format string syntax 

// datepicker localization
$lang["_DP_closeText"] = 'Hecho'; // Done
$lang["_DP_prevText"] = 'Anterior'; //Prev
$lang["_DP_nextText"] = 'Siguiente'; // Next
$lang["_DP_currentText"] = 'Hoy'; // Today
$lang["_DP_monthNames"] = "['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']";
$lang["_DP_monthNamesShort"] = "['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun','Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']";
$lang["_DP_dayNames"] = "['Domingo', 'Lunes','Martes','Mi&ecute;rcoles','Jueves','Viernes','S&aacute;bado']";
$lang["_DP_dayNamesShort"] = "['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab']";
$lang["_DP_dayNamesMin"] = "['Do','Lu','Ma','Mi','Ju','Vi','Sa']";
$lang["_DP_weekHeader"] = 'Sem';
$lang["_DP_dateFormat"] = 'dd/mm/yy';
$lang["_DP_firstDay"] = '1';
$lang["_DP_isRTL"] = 'false';
$lang["_DP_showMonthAfterYear"] = 'false';
$lang["_DP_yearSuffix"] = '';

// Login Splash text
$lang["_SITE_SPLASHTEXT"] = htmlspecialchars($config['site_name']) ." es un medio seguro para compartir archivos grandes.<br />Entre para subir y enviar archivos o para generar \"Invitaciones de env&iacute;o\".<p>";

// site help
$lang["_HELP_TEXT"] = '

<div>

<div align="left" style="padding:5px">

<h4>Login</h4> 
<ul>
    <li>Si su instituci&oacute;n no aparece en la lista de Proveedores de Identidad (IdPs), o si falla la autenticaci&oacute;n institucional, pongase en contacto con su Centro de Atenci&oacute;n al Usuario</li> 
</ul>

<h4>Subir archivos de <i>hasta</i> 2 Gigabytes (2GB) con el cliente Adobe Flash</h4>
<ul>
	<li>Si visualiza videos de YouTube, esta opci&oacute;n deber&iacute;a funcionarle</li>
	<li>Precisa de un navegador con el complemento <a target="_blank" href="http://www.adobe.com/software/flash/about/">Adobe Flash</a> actualizado la versi&oacute;n 10 (y superiores)</li>
	<li>FileSender le advertir&aacute; de cualquier intento de subir un archivo que exceda el l&iacute;mite establecido</li>
</ul>

<h4>Subir archivos de <i>cualquier tama&ntilde;o</i> con HTML5</h4>
<ul>
        <li>Si le aparecece el icono <img src="images/html5_installed.png" alt="HTML5 OK" class="textmiddle" style="display:inline" /> en la esquina inferior derecha de la ventana de FileSender, esta es su opci&oacute;n </li>
	<li>Precisa de un navegador que soporte HTML5, la &uacute;ltima versi&oacute;n del "lenguaje de Internet"</li>
	<li>Firefox4 (y superiores) y Chrome, tanto en Windows, en Mac OSX como en Linux, "hablan" HTML5</li>
	<li>Consulte la web <a href="http://caniuse.com/#feat=fileapi" target="_blank">"Cu&aacute;ndo podr&eacute; usar..."</a> para seguir los progresos de la "FileAPI" HTML5 en la mayor&iacute;a de los navegadores. En particular, el soporte para <a href="http://caniuse.com/#feat=filereader" target="_blank">FileReader API</a> and <a href="http://caniuse.com/#feat=bloburls" target="_blank">Blob URLs</a> ha de aparececer en "verde" (=soportado) para el navegador, indicando que con &eacute;ste podr&aacute; subir archivos mayores de 2GB</li>
</ul>

<h4>Descargas de cualquier tama&ntilde;o</h4>
<ul>
        <li>Precisa de un navegador "moderno" para las descargas, que soporte Adobe Flash o HTML5</li>
</ul>


<h4>L&iacute;mites de esta instalaci&oacute;n de FileSender</h4>
<ul>
    <li><strong>N&uacute;mero m&aacute;ximo de destinatarios por env&iacute;o: </strong>'. $config["max_email_recipients"].' direcciones de email (separadas por comas o puntos y coma)</li>
    <li><strong>N&uacute;mero m&aacute;ximo de archivos por env&iacute;o: </strong>1, para envio de varios a la vez compr&iacute;malos en un &uacute;nico archivo</li>
    <li><strong>Tama&ntilde;o m&aacute;ximo del archivo por env&iacute;o (Adobe Flash): </strong>'. formatBytes($config["max_flash_upload_size"]).' </li>
    <li><strong>Tama&ntilde;o m&aacute;ximo del archivo por env&iacute;o (HTML5): </strong>'. formatBytes($config["max_html5_upload_size"]).'</li>
    <li><strong>D&iacute;as hasta la expiraci&oacute;n de un archivo/invitaci&oacute;n de env&iacute;o (m&aacute;ximo): </strong>'. $config["default_daysvalid"].' </li>
</ul>
<p></p>
<p>Para m&aacute;s informaci&oacute;n sobre FileSender, visite <a href="http://www.filesender.org/" target="_blank">www.filesender.org</a></p>
</div>
</div>';

// site about
$lang["_ABOUT_TEXT"] = ' <div align="left" style="padding:5px">'. '"'. htmlspecialchars($config['site_name']) .'" es una instalaci&oacute;n de FileSender (<a rel="nofollow" href="http://www.filesender.org/" target="_blank">www.filesender.org</a>), desarrollado siguiendo los requerimientos de la comunidad de investigaci&oacute;n y educaci&oacute;n superior.</div>';

// site AUP terms
$lang["_AUPTERMS"] = "Pol&iacute;tica de Uso Aceptable - Conciciones de Servicio";

?>
