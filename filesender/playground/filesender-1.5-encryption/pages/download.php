<?php

/*
 * FileSender www.filesender.org
 * 
 * Copyright (c) 2009-2012, AARNet, Belnet, HEAnet, SURFnet, UNINETT
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * *	Redistributions of source code must retain the above copyright
 * 	notice, this list of conditions and the following disclaimer.
 * *	Redistributions in binary form must reproduce the above copyright
 * 	notice, this list of conditions and the following disclaimer in the
 * 	documentation and/or other materials provided with the distribution.
 * *	Neither the name of AARNet, Belnet, HEAnet, SURFnet and UNINETT nor the
 * 	names of its contributors may be used to endorse or promote products
 * 	derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* ---------------------------------
 * My Downloads Page
 * ---------------------------------
 * 
 */


// get file data
if (isset($_REQUEST['vid'])) {
$vid = $_REQUEST['vid'];
$filedata = $functions->getVoucherData($vid);
}
?>
<script src="js/sjcl.js" type="text/javascript"></script>
<script src="js/FileSaver.js" type="text/javascript"></script>
<script src="js/FileStorage.js" type="text/javascript"></script>
<script src="js/shared.js" type="text/javascript"></script>
<script src="js/downloader.js" type="text/javascript"></script>
<!-- <script src="js/swfobject.js" type="text/javascript"></script> -->
<!-- <script src="js/downloadify.min.js" type="text/javascript"></script> -->

<script type="text/javascript">
var chunksize =  2000000;
$(document).ready(function() { 
	$("#message").hide();

	// set dialog cancel upload
	$("#dialog-cancel").dialog({ autoOpen: false, height: 140, width: 350, modal: true,
	buttons: {
			'downloadconfirmyesBTN': function() {
			location.reload(true);

			},
			'downloadconfirmnoBTN': function() { 
			$( this ).dialog( "close" );
			}
	}
	});
	$('.ui-dialog-buttonpane button:contains(downloadconfirmnoBTN)').attr("id","btn_downloadconfirmno");            
	$('#btn_downloadconfirmno').html('<?php echo lang("_NO") ?>') 
	$('.ui-dialog-buttonpane button:contains(downloadconfirmyesBTN)').attr("id","btn_downloadconfirmyes");            
	$('#btn_downloadconfirmyes').html('<?php echo lang("_YES") ?>') 
	
	$("#dialog-downloadprogress").dialog({ 		
	    open: function() {
      //Hide closing "X" for this dialog only.
      $(this).parent().children().children("a.ui-dialog-titlebar-close").remove();
	},
	autoOpen: false, height: 180,width: 400, modal: true,title: "<?php echo lang("_DOWNLOAD_PROGRESS") ?>:",		
	buttons: {
		'downloadcancelBTN': function() {
			// are you sure?
			$("#dialog-cancel").dialog('open');
			$('.ui-dialog-buttonpane > button:last').focus();
			}	
		}
	})
	$('.ui-dialog-buttonpane button:contains(downloadcancelBTN)').attr("id","btn_downloadcancel");            
	$('#btn_downloadcancel').html('<?php echo lang("_CANCEL") ?>') 
});
function startDownload()
{
	var encrypted = <?php echo $filedata["fileencryption"]?'true':'false';?>;

	if(encrypted) {
		$("#dialog-downloadprogress").dialog("open");
	    downloader.start('download.php?vid=<?php echo urlencode($filedata["filevoucheruid"]);?>', '<?php echo utf8tohtml($filedata["fileoriginalname"],TRUE);?>', total_cryptlen(<?php echo $filedata["filesize"];?>));
	    return false
	} else {
	    $("#message").show();
	    return true;
	}
}
</script>
<div id="dialog-cancel" style="display:none" title="<?php echo lang("_CANCEL_UPLOAD"); ?>"><?php echo lang("_ARE_YOU_SURE"); ?></div>
<div id="dialog-downloadprogress" title="" style="display:none">
<img id="progress_image" name="progress_image" src="images/ajax-loader-sm.gif" width="16" height="16" alt="Uploading" align="left" /> 
	<div id="progress_container">
   		<div id="progress_bar" style="display:none">
		<div id="progress_completed"></div>
	</div>
	</div> 
</div>

<div id='message'><?php echo lang("_STARTED_DOWNLOADING") ?></div>
<div id="box">
<?php echo '<div id="pageheading">'.lang("_DOWNLOAD").'</div>'; ?> 
  <div id="tablediv">
  <table>
  <tr><td id="download_to"><?php echo lang("_TO"); ?>:</td><td id="to"><?php echo htmlentities($filedata["fileto"]);?></td></tr>
  <tr><td id="download_from"><?php echo lang("_FROM"); ?>:</td><td id="from"><?php echo htmlentities($filedata["filefrom"]);?></td></tr>
  <tr><td id="download_subject"><?php echo lang("_SUBJECT"); ?>:</td><td id="subject"><?php echo utf8tohtml($filedata["filesubject"],TRUE);?></td></tr>
  <tr><td id="download_message"><?php echo lang("_MESSAGE"); ?>:</td><td id="filemessage"><?php echo nl2br(utf8tohtml($filedata["filemessage"],TRUE));?></td></tr>
  <tr><td id="download_filename"><?php echo lang("_FILE_NAME"); ?>:</td><td id="filename"><?php echo utf8tohtml($filedata["fileoriginalname"],TRUE);?></td></tr>
  <tr><td id="download_filesize"><?php echo lang("_FILE_SIZE"); ?>:</td><td id="filesize"><?php echo formatBytes($filedata["filesize"]);?></td></tr>
  <tr><td id="download_expiry"><?php echo lang("_EXPIRY_DATE"); ?>:</td><td id="expiry"><?php echo date($lang['datedisplayformat'],strtotime($filedata["fileexpirydate"]));?></td></tr>
  <tr><td id="download_encryption">Versleuteling:</td><td id="fileencryption"><?php echo $filedata["fileencryption"]?"encryption (v" .$filedata["fileencryption"]. ")":'geen';?></td></tr>
  </table>
  </div>
  <div class="menu" id="downloadbutton" ><p><a id="download" href="download.php?vid=<?php echo urlencode($filedata["filevoucheruid"]);?>" onclick="return startDownload();"><?php echo lang("_START_DOWNLOAD"); ?></a></p></div>
</div>
