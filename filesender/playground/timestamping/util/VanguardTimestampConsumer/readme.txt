Vanguard Timestamp Consumer
---------------------------

Usage:
  java -jar VanguardTimestampConsumer.jar [-id requestid] -dh 0123456789ABCDEF0123456789ABCDEF [-dh ..] [-rfc3161] [-xml|-json]

-h        Prints this help.
-id       Optional request ID. Not processed, but returned by the webservice as a reference.
-dh       32 byte (64 chars long) hexadecimal SHA-256 hash of the document to be timestamped.
-rfc3616  Request timestamp as a RFC3616 TSR.
-xml      Specifies that output is in xml (default).
-json     Specifies that output is serialised as JSON.

Notes:
If the keystore (at the time of writing is "AARNET - 3PT.pfx") is changed, src/META-INF/service.xml
(specifically the <sc:KeyStore/> element) needs to be updated to have the correct filename, keystore
type, store password, key password, and alias.

If the hexadecimal string after -dh isn't exactly 64 characters long (which converts to 32 bytes), the
Vanguard will respond with a general error. If the hexadecimal string is an odd number of characters,
the java client will complain indicate that the string is not an even number of characters.