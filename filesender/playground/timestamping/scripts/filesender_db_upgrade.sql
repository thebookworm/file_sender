	--
-- Notes: pgSQL upgrade from 1.1.x DB to 1.5
-- Version: 1.5

--
-- Notes: The fileauthuserid column in the files table now has a size of 500, consistent with the corresponding column in the logs table and to allow for SAML attributes with very large values.
-- Required: Upgrading from 1.0.x/1.1.x

ALTER TABLE files ALTER fileauthuseruid TYPE character varying(500);
ALTER TABLE files ALTER fileip6address TYPE character varying(45);

--
-- Notes: Due to a change in the back-end workflow the database type of the fileto and corresponding logto columns needs to be changed from varchar(250) to text. Not doing so will break uploads to multiple recipients exceeding the 250 character limit when combined.
-- Required: Upgrading from 1.5-beta1

ALTER TABLE files ALTER fileto TYPE text;
ALTER TABLE logs ALTER logto TYPE text;

-- Notes: add stats table
-- Required: For all upgrades from 1.x
DROP FUNCTION IF EXISTS add_stats();
CREATE FUNCTION add_stats() RETURNS VOID AS
$$
BEGIN
        BEGIN
            CREATE TABLE stats
		(
			statid integer NOT NULL DEFAULT nextval('log_id_seq'::regclass),
			statfileuid character varying(60),
			statlogtype character varying(60),
			statfromdomain character varying(250),
			stattodomain text,
			statdate timestamp without time zone,
			statfilesize bigint,
			statsessionid character varying(60),
			statvoucheruid character varying(60),
			statauthuseruid character varying(500),
			CONSTRAINT stats_pkey PRIMARY KEY (statid)
		);
            RETURN;
        EXCEPTION
        WHEN duplicate_table THEN 
        RAISE NOTICE 'stats table already exists.';
            -- do nothing
        END;
END;
$$
LANGUAGE plpgsql;
SELECT add_stats();

-- Notes: Add filetimestampcertificate to files table
-- Required: For all upgrades from 1.x
DROP FUNCTION IF EXISTS add_filetimestampcertificate();
CREATE FUNCTION add_filetimestampcertificate() RETURNS VOID AS
$$
BEGIN
        BEGIN
            ALTER TABLE files ADD COLUMN filetimestampcertificate text;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column filetimestampcertificate already exists in files.';
        END;
END;
$$
LANGUAGE plpgsql;
SELECT add_filetimestampcertificate();

-- Notes: Add fileoptions to files table
-- Required: For all upgrades from 1.x
DROP FUNCTION IF EXISTS add_fileoptions();
CREATE FUNCTION add_fileoptions() RETURNS VOID AS
$$
BEGIN
        BEGIN
            ALTER TABLE files ADD COLUMN fileoptions text;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column fileoptions already exists in files.';
        END;
END;
$$
LANGUAGE plpgsql;
SELECT add_fileoptions();

-- Notes: Config table added to store dbversion number
-- Required: Upgrading from 1.x
DROP FUNCTION IF EXISTS addtable_config();
CREATE FUNCTION addtable_config() RETURNS VOID AS
$$
BEGIN
        BEGIN
            CREATE TABLE config
		(
		configID integer NOT NULL DEFAULT nextval('log_id_seq'::regclass),
		configField varchar(45) NOT NULL,
		configValue text NOT NULL,
		CONSTRAINT config_pkey PRIMARY KEY (configID)
		);
        EXCEPTION
            WHEN duplicate_table THEN RAISE NOTICE 'table config already exists.';
        END;
END;
$$
LANGUAGE plpgsql;
SELECT addtable_config();

-- Notes: Set Config dbversion
-- Required: For all upgrades from 1.x

DELETE FROM config WHERE configField = 'DBVersion';
INSERT INTO config ( configField,configValue)  VALUES ('DBVersion','1.5');
