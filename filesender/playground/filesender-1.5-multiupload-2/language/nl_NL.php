<?php

/*
 * FileSender www.filesender.org
 * 
 * Copyright (c) 2009-2012, AARNet, Belnet, HEAnet, SURFnet, UNINETT
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * *	Redistributions of source code must retain the above copyright
 * 	notice, this list of conditions and the following disclaimer.
 * *	Redistributions in binary form must reproduce the above copyright
 * 	notice, this list of conditions and the following disclaimer in the
 * 	documentation and/or other materials provided with the distribution.
 * *	Neither the name of AARNet, Belnet, HEAnet, SURFnet and UNINETT nor the
 * 	names of its contributors may be used to endorse or promote products
 * 	derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* ---------------------------------
 * nl_NL Language File
 * ---------------------------------
 * 
 */
// Hoofdmenu items
$lang["_ADMIN"] = "Beheer";
$lang["_NEW_UPLOAD"] = "Nieuwe Upload";
$lang["_VOUCHERS"] = "Uitnodiging";
$lang["_LOGON"] = "Aanmelden";
$lang["_LOG_OFF"] = "Afmelden";
$lang["_MY_FILES"] = "Mijn bestanden";

// Pagina titels
$lang["_HOME"] = "Home";
$lang["_ABOUT"] = "Over";
$lang["_HELP"] = "Help";
$lang["_DELETE_VOUCHER"] = "Trek uitnodiging in";
$lang["_UPLOAD_COMPLETE"] = "Uw bestand is geüpload en e-mail is verzonden.";
$lang["_UPLOAD_PROGRESS"] = "Voortgang Upload";
$lang["_DOWNLOAD"] = "Download";
$lang["_CANCEL_UPLOAD"] = "Annuleer Upload";

// Admin categorie namen

$lang["_PAGE"] = "Pagina";
$lang["_UP"] = "Terug";
$lang["_DOWN"] = "Verder";                        
$lang["_FILES"] = "Bestanden";
$lang["_DRIVE"] = "Disk";                        
$lang["_TOTAL"] = "Totaal";
$lang["_USED"] = "In gebruik";
$lang["_AVAILABLE"] = "Beschikbaar";
$lang["_TEMP"] = "Tijdelijk"; // as in Temporary files

// Admin tabblad namen
$lang["_GENERAL"] = "Algemeen";
$lang["_UPLOADS"] = "Uploads";
$lang["_DOWNLOADS"] = "Downloads";
$lang["_ERRORS"] = "Fouten";
$lang["_FILES_AVAILABLE"] = "Beschikbare Bestanden";
$lang["_ACTIVE_VOUCHERS"] = "Geldige uitnodigingen";
$lang["_COMPLETE_LOG"] = "Volledig logboek";

// Upload page
$lang["_WELCOME"] = "Welkom";
$lang["_WELCOMEGUEST"] = "Welkom Gast";

// Upload pagina: Formuliervelden
$lang["_TO"] = "Aan";
$lang["_FROM"] = "Van";
$lang["_SIZE"] = "Grootte";
$lang["_CREATED"] = "Aangemaakt";
$lang["_FILE_NAME"] = "Bestandsnaam";
$lang["_SUBJECT"] = "Onderwerp";
$lang["_MESSAGE"] = "Bericht";
$lang["_OPTIONAL"] = "Optioneel";
$lang["_EXPIRY"] = "Vervaldatum";
$lang["_EXPIRY_DATE"] = "Vervaldatum";
$lang["_TYPE"] = "Type";

$lang["_TERMS_OF_AGREEMENT"] = "Algemene voorwaarden";
$lang["_SHOW_TERMS"] = "Toon Voorwaarden";
$lang["_SHOWHIDE"] = "Toon/Verberg";
$lang["_SELECT_FILE"] = "Selecteer een bestand om te uploaden";
$lang["_UPLOADING_WAIT"] = "Uploaden bestand - even geduld a.u.b. ...";
$lang["_EMAIL_SEPARATOR_MSG"] = "Meerdere e-mailadressen gescheiden door, of ;";
$lang["_NO_FILES"] = "Er zijn momenteel geen bestanden beschikbaar";

$lang["_AUPTERMS"] = "Algemene voorwaarden";
$lang["_ACCEPTTOC"] = "Ik ga akkoord met de bepalingen en voorwaarden van deze dienst.";	
$lang["_AGREETOC"] = "U moet akkoord gaan met de voorwaarden.";

$lang["_ERROR_INCORRECT_FILE_SIZE"] = "Er is een probleem opgetreden bij het uploaden van uw bestand. <br />De bestandsgrootte op de server wijkt af van de grootte van uw eigen kopie.<br /><br />Neem contact op met de beheerder.";
$lang["_MAXEMAILS"] = "Het maximum aantal toegestane mailadressen is ";
$lang["_INVALID_DATE_FORMAT"] = "De datum is in een ongeldig formaat.";
$lang["_DISK_SPACE_ERROR"] = "Er is niet genoeg vrije ruimte op deze service. Neemt u s.v.p. contact op met de beheerder of upload een kleiner bestand.";


// Vouchers
$lang["_SEND_NEW_VOUCHER"] = "Met een Uitnodiging kunt u iemand een bestand laten sturen.<br />
Om een Uitnodiging te maken, voer een e-mailadres in en klik op Stuur Uitnodiging.<br />
Er wordt dan een e-mail verstuurd naar de ontvanger met daarin een link om de uitnodiging te gebruiken.";
$lang["_SEND_VOUCHER_TO"] = "Stuur uitnodiging naar";
$lang["_SEND_VOUCHER"] = "Stuur uitnodiging";
$lang["_VOUCHER_USED"] = "Deze uitnodiging is reeds gebruikt.";

// Upload pagina: button menu
$lang["_UPLOAD"] = "Upload";
$lang["_BROWSE"] = "Blader";
$lang["_CANCEL"] = "Annuleer";
$lang["_OPEN"] = "Open";
$lang["_CLOSE"] = "Sluit";
$lang["_OK"] = "OK";
$lang["_SEND"] = "Verzend";
$lang["_DELETE"] = "Verwijder";
$lang["_YES"] = "Ja";
$lang["_NO"] = "Nee";

// steps
$lang["_STEP1"] = "Voer één of meer e-mailadressen in";
$lang["_STEP2"] = "Stel een vervaldatum in";
$lang["_STEP3"] = "Selecteer een bestand";
$lang["_STEP4"] = "Selecteer Verzend";
$lang["_HTML5Supported"] = "Uploads groter dan 2GB mogelijk!";
$lang["_HTML5NotSupported"] = "Uploads groter dan 2GB niet mogelijk!";

// Upload page: error messages, displayed on-input
$lang["_INVALID_MISSING_EMAIL"] = "Ongeldig of ontbrekend e-mailadres";
$lang["_INVALID_EXPIRY_DATE"] = "Ongeldige vervaldatum";
$lang["_INVALID_FILE"] = "Ongeldig bestand";
$lang["_INVALID_FILEVOUCHERID"] = "Ongeldig bestands- of uitnodigings-ID";	
$lang["_INVALID_FILESIZE_ZERO"] = "Ongeldige bestandsgrootte van 0 bytes. Kies een ander bestand.";
$lang["_INVALID_FILE_EXT"] = "Ongeldig bestandstype.";
$lang["_INVALID_TOO_LARGE_1"] = "Bestand kan niet groter zijn dan";
$lang["_AUTH_ERROR"] = "U bent niet meer aangemeld.<br />Wellicht is uw sessie verlopen of was er een probleem op de server.<br /><br />Logt u s.v.p. opnieuw in en probeer het nogmaals.";
$lang["_SELECT_ANOTHER_FILE"] = "Kies een ander bestand.";
$lang["_INVALID_VOUCHER"] = "Deze uitnodiging is niet meer geldig.<br />Neem contact op met degene die u uitnodigde.";
$lang["_INVALID_FILE_NAME"] = "Ongeldige bestandsnaam. Hernoem het bestand en probeer het opnieuw.";
$lang["_INVALID_SIZE_USEHTML5"] = "Selecteer een ander bestand of gebruik een geschikte HTML5-browser voor grotere bestanden.";
$lang["_FILE_TO_BE_RESENT"] = "Bestand om opnieuw te versturen";
$lang["_ERROR_UPLOADING_FILE"] = "Fout bij het uploaden van het bestand";
$lang["_LOGOUT_COMPLETE"] = "U bent afgemeld";

$lang["_ARE_YOU_SURE"] = "Weet u dit zeker?";
$lang["_DELETE_FILE"] = "Verwijder bestand";
$lang["_EMAIL_SENT"] = "E-mail verstuurd";
$lang["_FILE_SIZE"] = "Bestandsgrootte";
$lang["_FILE_RESENT"] = "Bestand opnieuw verstuurd";
$lang["_MESSAGE_RESENT"] = "Bericht opnieuw verstuurd";
$lang["_ME"] = "Mij";
$lang["_RE_SEND_EMAIL"] = "E-mail opnieuw versturen";
$lang["_NEW_RECIPIENT"] = "Ontvanger toevoegen";
$lang["_START_DOWNLOAD"] = "Start Download";
$lang["_VOUCHER_SENT"] = "Uitnodiging verstuurd";
$lang["_VOUCHER_DELETED"] = "Uitnodiging ingetrokken";
$lang["_VOUCHER_CANCELLED"] = "Deze uitnodiging is ingetrokken.";
$lang["_STARTED_DOWNLOADING"] = "De download van het bestand zal beginnen.";
$lang["_FILE_DELETED"] = "Dit bestand is verwijderd.";

// confirmation
$lang["_CONFIRM_DELETE_FILE"] = "Wilt U dit bestand echt verwijderen?";
$lang["_CONFIRM_DELETE_VOUCHER"] = "Wilt U deze uitnodiging echt intrekken?";

// standard date display format
$lang['datedisplayformat'] = "d-m-Y"; // Format for displaying date/time, use PHP date() format string syntax 

// datepicker localization
$lang["_DP_closeText"] = 'Sluiten'; // Done
$lang["_DP_prevText"] = '←'; //Prev
$lang["_DP_nextText"] = '→'; // Next
$lang["_DP_currentText"] = 'Vandaag'; // Today
$lang["_DP_monthNames"] = "['januari', 'februari', 'maart', 'april', 'mei', 'juni','juli', 'augustus', 'september', 'oktober', 'november', 'december']";
$lang["_DP_monthNamesShort"] = "['jan', 'feb', 'mrt', 'apr', 'mei', 'jun','jul', 'aug', 'sep', 'okt', 'nov', 'dec']";
$lang["_DP_dayNames"] = "['zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag']";
$lang["_DP_dayNamesShort"] = "['zon', 'maa', 'din', 'woe', 'don', 'vri', 'zat']";
$lang["_DP_dayNamesMin"] = "['zo', 'ma', 'di', 'wo', 'do', 'vr', 'za']";
$lang["_DP_weekHeader"] = 'Wk';
$lang["_DP_dateFormat"] = 'dd-mm-yy';
$lang["_DP_firstDay"] = '1';
$lang["_DP_isRTL"] = 'false';
$lang["_DP_showMonthAfterYear"] = 'false';
$lang["_DP_yearSuffix"] = '';

// Login Splash text
$lang["_SITE_SPLASHTEXT"] = "FileSender is een veilige manier om bestanden te delen met iedereen! Meld U aan om een bestand te versturen of om iemand uit te nodigen om een bestand te sturen."; 

// site help
$lang["_HELP_TEXT"] = '
<div>
<div align="left" style="padding:5px">
<h4>Aanmelden</h4> 
<ul>
    <li>Als uw instelling niet in de lijst met beschkbare identity providers staat, of u heeft problemen met inloggen, neemt u dan contact op met uw lokale IT-afdeling.</li>
</ul>

<h4>Uploads van <i>willekeurig welke grootte</i> via HTML5</h4>
<ul>
        <li>Als u het <img src="images/html5_installed.png" alt="green HTML5 tick" class="textmiddle" style="display:inline" />-symbool ziet dan is deze methode voor u beschikbaar.</li>
	<li>Hiervoor is een relatief nieuwe browserversie nodig die HTML5 ondersteunt, de nieuwste editie van de "taal van het web".</li>
	<li>Momenteel geldt dit in ieder geval voor Firefox 4 (en hoger) en Chrome op Windows, Mac OS X en Linux.</li>
	<li>Gebruik de <a href="http://caniuse.com/#feat=fileapi" target="_blank">"When can I use..."</A>-website om de voortgang van de implementatie van HTML5 FileAPI in de belangrijkste browsers te zien. Met name ondersteuning voor de <a href="http://caniuse.com/#feat=filereader" target="_blank">FileReader API</A> en voor <A href="http://caniuse.com/#feat=bloburls" target="_blank">Blob URLs</A> moet groen licht (=ondersteund) krijgen, wil een browser uploads groter dan 2GB kunnen doen.</li>
</ul>

<h4>Downloads van willekeurig welke grootte</h4>
<ul>
        <li>Hiervoor is slechts een moderne browser noodzakelijk; Adobe Flash of HTML5 zijn <b>niet</b> nodig voor downloads.</li>
</ul>

<h4>Uploads kleinder dan 2 Gigabyte (2GB) via Adobe Flash</h4>
<ul>
	<li>Als u YouTube-video\'s kunt bekijken dan zou deze methode ook moeten werken.</li>
	<li>U heeft een moderne browser nodig met minimaal versie 10 van <a target="_blank" href="http://www.adobe.com/software/flash/about/">Adobe Flash.</a></li>
	<li>FileSender waarschuwt u als u een bestand wilt uploaden dat te groot is voor deze methode.</li>
</ul>



<h4>Beperkingen van deze FileSender-instantie</h4>
<ul>
    <li><strong>
      Maximum aantal e-mail-ontvangers:</strong> Tot '. $config["max_email_recipients"].' e-mailadressen gescheiden door een komma of puntkomma</li>
    <li><strong>Maximum aantal bestanden per upload:</strong> &eacute;&eacute;n - om meerdere bestanden ineens te versturen, kunt u ze eerst samenpakken in een archiefbestand zoals zip</li>
    <li><strong>Maximum bestandsgrootte per upload, alleen gebruikmakend van Adobe Flash:</strong> '. formatBytes($config["max_flash_upload_size"]).'</li>
    <li><strong>Maximum bestandsgrootte per upload, via HTML5:</strong> '. formatBytes($config["max_html5_upload_size"]).'</li>
    <li><strong>Maximum geldigheidsduur van bestanden en uitnodigingen:</strong> '. $config["default_daysvalid"].' dagen</li>
</ul>
<p>Voor meer informatie, bezoek <a href="http://www.filesender.org/" target="_blank">www.filesender.org</a>.</p>
</div>
</div>';


// site about
$lang["_ABOUT_TEXT"] = ' <div align="left" style="padding:5px">'. htmlentities($config['site_name']) .' is een instantie van FileSender (<a rel="nofollow" href="http://www.filesender.org/" target="_blank">www.filesender.org</a>), ontwikkeld om te voldoen aan de eisen van de hoger onderwijs en onderzoeksgemeenschap.</div>';


?>
