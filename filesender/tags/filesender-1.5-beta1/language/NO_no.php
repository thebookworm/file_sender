<?php

/*
 * FileSender www.filesender.org
 * 
 * Copyright (c) 2009-2012, AARNet, Belnet, HEAnet, SURFnet, UNINETT
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * *	Redistributions of source code must retain the above copyright
 * 	notice, this list of conditions and the following disclaimer.
 * *	Redistributions in binary form must reproduce the above copyright
 * 	notice, this list of conditions and the following disclaimer in the
 * 	documentation and/or other materials provided with the distribution.
 * *	Neither the name of AARNet, Belnet, HEAnet, SURFnet and UNINETT nor the
 * 	names of its contributors may be used to endorse or promote products
 * 	derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* ---------------------------------
 * NO_no Language File
 * ---------------------------------
 * 
 */
// Main menyelementene
$lang["_ADMIN"] = "Administrasjon";
$lang["_NEW_UPLOAD"] = "Ny filsending";
$lang["_VOUCHERS"] = "Send invitasjon til filsending";
$lang["_LOGON"] = "Logg på";
$lang["_LOG_OFF"] = "Logg av";
$lang["_MY_FILES"] = "Mine filer";

// Siden titler
$lang["_HOME"] = "Hjem";
$lang["_ABOUT"] = "Om";
$lang["_HELP"] = "Hjelp";
$lang["_VOUCHER_CANCELLED"] = "Filsendingsinvitsjon kansellert";
$lang["_DELETE_VOUCHER"] = "Kanseller invitasjon";
$lang["_UPLOAD_COMPLETE"] = "Opplasting ferdig";
$lang["_UPLOAD_PROGRESS"] = "Opplastingsframgang";
$lang["_DOWNLOAD"] = "Last ned";
$lang["_CANCEL_UPLOAD"] = "Avbryt opplasting";

// site about
$lang["_ABOUT_TEXT"] = ""; // overide in config/EN_AU.php
// site help
$lang["_HELP_TEXT"] = ""; // overide in config/EN_AU.php

// Admin meny


// Admin kategorien navn

$lang["_PAGE"] = "Side";
$lang["_UP"] = "Opp";
$lang["_DOWN"] = "Ned";                        
$lang["_FILES"] = "Filer";
$lang["_DRIVE"] = "Disk";                        
$lang["_TOTAL"] = "Totall";
$lang["_USED"] = "I bruk";
$lang["_AVAILABLE"] = "Tilgjengelig";
$lang["_TEMP"] = "Temp"; // as in Temporary files

// Admin interface: tab names
$lang["_GENERAL"] = "Generelt";
$lang["_UPLOADS"] = "Opplastninger";
$lang["_DOWNLOADS"] = "Nedlastninger";
$lang["_ERRORS"] = "Feil";
$lang["_FILES_AVAILABLE"] = "Tilgjengelige filer";
$lang["_ACTIVE_VOUCHERS"] = "Aktive invitasjoner";
$lang["_COMPLETE_LOG"] = "Alle logdata";

// Upload page
$lang["_WELCOME"] = "Welkommen";
$lang["_WELCOMEGUEST"] = "Welkommen Gjest";

// Upload page: Form Fields
$lang["_TO"] = "Til";
$lang["_FROM"] = "Fra";
$lang["_SIZE"] = "Størrelse";
$lang["_CREATED"] = "Opprettet";
$lang["_FILE_NAME"] = "Filnavn";
$lang["_SUBJECT"] = "Emne";
$lang["_MESSAGE"] = "Melding";
$lang["_OPTIONAL"] = "opsjonell";
$lang["_EXPIRY"] = "Utløpsdato";
$lang["_EXPIRY_DATE"] = "Utløpsdato";
$lang["_TYPE"] = "Type";

$lang["_TERMS_OF_AGREEMENT"] = "Brukervilkår";
$lang["_SHOW_TERMS"] = "Vis vilkår";
$lang["_SELECT_FILE"] = "Velg en fil til å laste opp";
$lang["_UPLOADING_WAIT"] = "Laster opp fil - vennligst vent ...";
$lang["_EMAIL_SEPARATOR_MSG"] = "Flere epost-adresser avskilles med , eller ;";

$lang["_AUPTERMS"] = "Vilkår og betingelser";
$lang["_ACCEPTTOC"] = "Jeg aksepterer vilkårene for denne tjenesten.";	
$lang["_AGREETOC"] = "Du må akseptere vilkårene.";
$lang["_SHOWHIDE"] = "Vis/gjem";

// Upload page: Flash button menu
$lang["_UPLOAD"] = "Last opp";
$lang["_BROWSE"] = "Velg fil";
$lang["_CANCEL"] = "Avbryt";
$lang["_OPEN"] = "Open";
$lang["_CLOSE"] = "Lukk";
$lang["_OK"] = "Ok";
$lang["_SEND"] = "Send";
$lang["_DELETE"] = "Slett";
$lang["_YES"] = "Ja";
$lang["_NO"] = "No";

// Upload page: error messages, displayed on-input
$lang["_INVALID_MISSING_EMAIL"] = "Feilaktig eller manglende epostadresse";
$lang["_INVALID_EXPIRY_DATE"] = "Feil i utløpsdato";
$lang["_INVALID_FILE"] = "Noe er galt med filen som ble valgt";
$lang["_INVALID_FILEVOUCHERID"] = "Invalid File or Voucher ID";	
$lang["_INVALID_FILESIZE_ZERO"] = "Filer med størrelse 0 kan ikke velges.  Velg en annen fil";
$lang["_INVALID_FILE_EXT"] = "Feiltypen ikke tillat.";
$lang["_INVALID_TOO_LARGE_1"] = "Filstørrelse kan ikke være mer enn";
$lang["_SELECT_ANOTHER_FILE"] = "Velg en annen fil.";
$lang["_INVALID_FILE_NAME"] = "Feilaktig filnavn, filen kan ikke lastes opp.  Gi filen et annet navn og prøv på nytt.";
$lang["_INVALID_SIZE_USEHTML5"] = "Velg en annen fil or use a HTML5 enabled browser to upload larger files.";

$lang["_ERROR_UPLOADING_FILE"] = "Feil under filopplasting";
$lang["_LOGOUT_COMPLETE"] = "Avlogging utført";

$lang["_ARE_YOU_SURE"] = "Er du sikker?";
$lang["_EMAIL_SENT"] = "Epost sendt";

$lang["_FILE_SIZE"] = "Filstørrelse";
$lang["_FILE_RESENT"] = "Sendt fil på nytt";
$lang["_ME"] = "Meg";

// MYFILES

$lang["_NO_FILES"] = "Ingen filer funnet";

// Myfiles: re-send email: tooltips, dialogue box and on-screen message 
$lang["_RE_SEND_EMAIL"] = "Send epost på nytt";
$lang["_FILE_TO_BE_RESENT"] = "Fil:";
$lang["_MESSAGE_RESENT"] = "Epost med nedlastningslenken sendt på nytt"; 

$lang["_NEW_RECIPIENT"] = "En mottaker til";

// Myfiles: delete file: tooltips, dialogue box and on-screen message 
$lang["_DELETE_FILE"] = "Slett fil";
$lang["_FILE_DELETED"] = "Fil slettet";



$lang["_START_DOWNLOAD"] = "Begyn nedlastning";
$lang["_VOUCHER_SENT"] = "Invitasjon sendt";
$lang["_VOUCHER_DELETED"] = "Invitasjon slettet";
$lang["_VOUCHER_CANCELLED"] = "Denne filen eller invitasjonen har blitt utilgjengeliggjort.";
$lang["_STARTED_DOWNLOADING"] = "Filnedlastning burde starte nå";

// Upload page: information on steps user needs to perform
$lang["_STEP1"] = "Fyll ut epost-adress(ene)";
$lang["_STEP2"] = "Set utløpsdato";
$lang["_STEP3"] = "Velg en fil";
$lang["_STEP4"] = "Klikk Send";
$lang["_HTML5Supported"] = "Filopplasting over 2GB støttet!";
$lang["_HTML5NotSupported"] = "Filopplasting over 2GB ikke støttet!";	
//$lang["_site_help_text"] = "Hjelp tekst";
//$lang["_Help"] = "Hjelp";



// Voucher page
$lang["_SEND_NEW_VOUCHER"] = "Send en ny invitasjon";
$lang["_SEND_VOUCHER_TO"] = "Send en invitasjon til";
$lang["_SEND_VOUCHER"] = "Send invitasjon";

// confirmation
$lang["_CONFIRM_DELETE_FILE"] = "Er du sikker at du vil slette filen?";
$lang["_CONFIRM_DELETE_VOUCHER"] = "Er du sikker at du ønsker å tilbaketrekke invitasjonen?";

// standard date display format
$lang['datedisplayformat'] = "Y-m-d"; // Format for displaying date/time, use PHP date() format string syntax 

// datepicker localization
$lang["_DP_closeText"] = 'Lukk'; // Done
$lang["_DP_prevText"] = '&laquo;Forrige'; //Prev
$lang["_DP_nextText"] = 'Neste&raquo;'; // Next
$lang["_DP_currentText"] = 'I dag'; // Today
$lang["_DP_monthNames"] = "['Januar','Februar','Mars','April','Mai','Juni','Juli','August','September','Oktober','November','Desember']";
$lang["_DP_monthNamesShort"] = "['Søn','Man','Tir','Ons','Tor','Fre','Lør']";
$lang["_DP_dayNames"] = "['Søndag','Mandag','Tirsdag','Onsdag','Torsdag','Fredag','Lørdag']";
$lang["_DP_dayNamesShort"] = "['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']";
$lang["_DP_dayNamesMin"] = "['Sø','Ma','Ti','On','To','Fr','Lø']";
$lang["_DP_weekHeader"] = 'Uke';
$lang["_DP_dateFormat"] = 'yy-mm-dd'; // uses datepicker specific format http://docs.jquery.com/UI/Datepicker/formatDate
$lang["_DP_firstDay"] = '1';
$lang["_DP_isRTL"] = 'false';
$lang["_DP_showMonthAfterYear"] = 'false';
$lang["_DP_yearSuffix"] = '';


?>
