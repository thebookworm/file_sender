Updated: 1 November 2011

FileSender is a software package that implements a web-based application that
allows authenticated users to securely and easily send arbitrarily large files
to other users. Authentication of users is provided through SAML2, LDAP and
RADIUS. Users without an account can be sent an upload voucher by an
authenticated user. FileSender is developed to the requirements of the higher
education and research community.

The purpose of the software is to send a large file to someone, have that file
available for download for a certain amount of time, and after that
automatically delete the file. The software is not intended as a permanent
file publishing platform.

All major workflow actions (such as files or vouchers becoming available, or
being deleted) are signalled to both sender as well as recipient by means of
html-templated emails

FileSender project home:
   http://www.filesender.org/

FileSender installation and configuration documentation is available at:
   https://www.assembla.com/wiki/show/file_sender/Documentation_for_v1-x


Support is available on a best-effort basis through the FileSender development
mailinglist filesender-dev@filesender.org. For information on how to subscribe
and the mailinglist archives:
   http://www.assembla.com/wiki/show/file_sender/Support_and_Mailinglists


Acknowledgements
================
The 1.0/1.1 release of the FileSender software was developed by an international
core team of contributors. The FileSender 1.0 core team members were
Chris Richter, Gijs Molenaar, Guido Aben, Jan Meijer, Xander Jansen, Wendy Mason

Many others have contributed to make FileSender 1.0/1.1 possible:
   http://www.assembla.com/wiki/show/file_sender/Acknowledgements

FileSender 1.0 was funded by a consortium of the national research and
educational networks (NREN) AARNet (Australia), HEAnet (Ireland), SURFnet
(Netherlands) and UNINETT (Norway) and FileSender 1.1 contains contributions
also funded by Belnet (Belgium). 

The FileSender project started in April 2009 and was initiated by AARNet,
HEAnet and UNINETT.
