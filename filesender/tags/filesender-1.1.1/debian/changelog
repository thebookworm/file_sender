filesender (1.1.1) stable; urgency=high
   * Release 1.1.1

   * Important changes since 1.1
     - Support for changed HTML5 semantics in Firefox 13+ (#730).
     - Fixed potential privilege elevation issue which under certain
       conditions could give a normal user access to the admin
       interface (#750).
     - Change in database column type for fileto/logto (#739).
       When upgrading from previous versions you should manually alter
       the relevant column type in the database with:

       #sudo -u postgres psql filesender
         ALTER TABLE files ALTER fileto TYPE text;
         ALTER TABLE logs ALTER logto TYPE text;
         \q

   * Minor fixes and changes since 1.1
     - Additional email validation in Mail.php (#746).
     - Fix for multi-value SAML-attribute logging (#747).
     - Fix for upload with a backslash or double quote in the filename (#601).
     - Workaround for download of files with a double quote in the
       filename (#602).

 -- Filesender Development <filesender-dev@filesender.org>  Wed, 31 May 2011 16:00:00 +0200

filesender (1.1) stable; urgency=medium
   * Release 1.1
     
   * Major change since 1.0.1
     - Dropped Gears dependency, upload of large (>2G) files now requires
       a browser that supports the HTML5 FileAPI, other browsers are
       limited to 2G uploads using the Flash upload mechanism. This change
       also obsoletes the following Gears-related issues in 1.0:

       #176 >2GB upload on MacOS hangs at ~2GB (Gears enabled)
       #290 File select fails in Linux/Gears for files of 2G or more.
       #358 Filename accents not displayed in GUI when using Gears

       This means that FileSender 1.1 also allows large uploads from
       the Mac OSX and Linux platforms, provided a supported HTML5 browser
       is used.

     - Note that only user visible references to Gears have been changed.
       In the code and config.php references to 'gears' are still used. No
       changes are required in config.php except for the URL pointing to
       more information on HTML5:

          $config['gearsURL'] = 'http://html5test.com/';

   * Minor fixes and changes
     r993 filesender_db.sql: make fileauthuseruid character varying(500)
          When upgrading from previous versions you should fix the relevant
          column size in the database with:
              #sudo -u postgres psql filesender
              ALTER TABLE files ALTER fileauthuseruid TYPE character varying(500);
              \q
     #563 Compare of UID attribute with admin uid broken when attribute is
          not in an array
     #569 Case-sensitive references to Files in cron.php
     #582 Inconsistent file size limit warnings
     #583 Email addresses rejected at maximum for uploads & voucher issue

   * Packaging
     - The Debian package has been adapted to use the default baseurl
       of the simplesamlphp 1.7+ debian packages which changed from
      'simplesaml' to 'simplesamlphp' to adhere to standard Debian policy.

 -- Filesender Development <filesender-dev@filesender.org>  Sat, 05 Nov 2011 14:59:08 +0100

filesender (1.0.1) stable; urgency=medium
   * Release 1.0.1

   * Fixes and changes
     #271 compatibility fix for SimpleSAMLphp 1.7+
     #379 Log IPv6 address of connecting client. When upgrading from
          previous versions you should fix the relevant column size in the
          database with:
           #sudo -u postgres psql filesender
           ALTER TABLE files ALTER fileip6address TYPE character varying(45);
           \q
     #380, #389, #390  fix the max_gears_upload_size check.
     #381 allow multiple email addresses to be separated by both a
          comma and a semi-colon
     #383 trim whitespace from email-addresses
     #394 "File too large, use Gears" warnings fixups
     #396 Confirmation when cancelling Flash upload
     #397 Wrapping of Subject field in My Files table

   * Experimental feature
     - Mail bounce handling (for details see
       http://www.assembla.com/wiki/show/file_sender/Mail_Bounce_Handling )

   * Packaging
     - Added filesender-php.ini with recommended PHP settings

 -- Filesender Development <filesender-dev@filesender.org>  Fri, 29 Apr 2011 23:42:31 +0200

filesender (1.0) stable; urgency=medium
   * Release 1.0

   * Documentation
     - Updated documentation on http://www.filesender.org/
     - Updated config.php with documentation and grouping of related settings
     - Updated help.php and about.php (#363, #367)

   * Packaging
     - Improvements in the packaging (tar, RPM, debian)

   * Fixes and changes
     #360 Low diskspace warning administrator email message
     #368 Minor HTML fixes
     #369 Gears CHUNK_BYTES increased to 2M (was 200KB)
     #371 Change default pg_username from postgres to filesender
     #375 Add 'start again' link to default logout page

 -- Filesender Development <filesender-dev@filesender.org>  Sun, 30 Jan 2011 10:07:31 +0100

filesender (0.1.19+1.0rc1) stable; urgency=low
  * 1.0 Release Candidate 1
    - re-release of beta 0.1.19

 -- Filesender Development <filesender-dev@filesender.org>  Sun, 16 Jan 2011 13:09:35 +0100

filesender (0.1.19) stable; urgency=low

  * New features
    - #340 email address validation in flex regex added to config
      Please update your config.php
  * Small bug fixes and cleanup

 -- Filesender Development <filesender-dev@filesender.org>  Fri, 24 Dec 2010 14:08:00 +0100

filesender (0.1.17) stable; urgency=low

  * New features
    - FileSender now can handle filenames, subjects and personal messages in non-US-ASCII charsets
    - IMPORTANT: a new naming scheme to store files on the server was introduced. Please
                 run "php <filesenderbase>/scripts/convert-filenames.php" after upgrading
                 to Beta 0.1.17. If you need to downgrade to a previous version run
                 "php <filesenderbase>/scripts/convert-filenames.php revert" *before* downgrading.
    -  Many bug Fixes

 -- Filesender Development <filesender-dev@filesender.org>  Thu, 18 Nov 2010 14:08:00 +0100

filesender (0.1.16.1) stable; urgency=low

  * patch release 0.1.16.1

 -- Filesender Development <filesender-dev@filesender.org>  Fri, 08 Oct 2010 14:27:45 +0200

filesender (0.1.16) stable; urgency=low
  
  * New upstream release

 -- Filesender Development <filesender-dev@filesender.org>  Fri, 08 Oct 2010 11:48:53 +0200
