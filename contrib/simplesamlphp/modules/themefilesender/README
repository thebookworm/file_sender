Custom theme for simplesamlphp
==============================

The files in this directory are assumed to reside in the 'themefilesender'
directory in the simplesamlphp modules tree. The theme is based on
templates for simplesamlphp version 1.6.x but can be used with 1.7.

The files in the 'themefilesender' module can be used to:

1. get the simplesaml theming identical to the theme/style used by FileSender
2. to add some site specific text to the IdP selection page.

Both functions can be used independently, just remove the files for the
function you don't need.

Before activating the module change the sample support link in the supplied
dictionary files (see below at 2.) to your own support page.

Activate the theme by setting theme.use in config.php:

       'theme.use'             => 'themefilesender:filesender',

Also the following (empty) file should be in the top level directory
of the 'themefilesender' module:

  default-enable

1. Use the FileSender theme
===========================

To use the FileSender look&feel with simplesamlphp the default
header.php and footer.php need to be modified. In the files

  themes/filesender/default/includes/header.php
  themes/filesender/default/includes/footer.php

this is done by changing the href to the default.css stylesheet, by adding
a pointer to the FileSender displayimage.php script and by removing all
text in the default footer. The provided files assume that FileSender
is running on the same virtual webserver and is reachable at the
"/filesender/" base URL (the default install of FileSender). Please modify
these files if you have FileSender running on a different base URL.

The included header.php checks the current SimpleSAMLphp version and uses
a different list of language names for versions >= 1.7.0. 'Fixed' versions
to be used with either 1.6.x or 1.7.x are included.

2. Add additional info to the IdP selection page
================================================

If you want to add some instructions to the default IdP selection page
to help people when their IdP is not listed the following files can be used:

In:

  themes/filesender/default/selectidp-dropdown.php
  themes/filesender/default/selectidp-links.php

a short block of code is added at the end to display the extra text.
The important part is:

   echo $this->t('{themefilesender:filesender:idp_notlisted}'); 

This will display the text defined in the 'idp_notlisted' dictionary entry.
Two (dutch and english) sample definitions are provided in:

  dictionaries/filesender.definition.json
  dictionaries/filesender.translation.json

These definitions contain a sample URL pointing to the Federation pages of
SURFnet, please change to point to a location more relevant to your own
audience.

If you want to restrict the available languages to only the defined entries in
the above dictionaries change the language.available setting in config.php:

      'language.available'    => array('en', 'nl'),

More information
================

See http://simplesamlphp.org/docs/1.6/simplesamlphp-theming
and http://www.filesender.org/

Created by Xander Jansen @ SURFnet
